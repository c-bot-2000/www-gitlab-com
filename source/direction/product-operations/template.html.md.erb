---
layout: markdown_page
title: Product Direction - Product Operations
description: "Product Operations supports product management and cross-functional teams across GitLab"
canonical_path: "/direction/product-operations/"
---
- TOC
{:toc}

## Product Operations Overview

### What is Product Operations?

Product operations is a critical function of scaled, product-led organizations. The role and focus of product operations can vary depending on business maturity, customer needs and the product itself. A **good** product operations function will define, communicate, and improve practices that can be standardized, such as tools, planning, team gatherings, and training. A **great** product operations function will sit at the intersection of product management, UX/design, and engineering to enable cross-functional teams to consistently, flexibly and creatively deliver outcomes  to customers/users via strong  feedback loops and process automation. An **excellent** product operations function will create and implement systems to continually drive partnership between product management and teams across the business, inclusive of marketing, sales and customer success, resulting in accelerated feedback loops, collection/exposure of critical data and improvement of feature adoption. 

### What is Product Operations at GitLab?

At GitLab, Product Operations strives for the good, the great, and the excellent! We are in a unique position to define and lead what this newly emerging function can do for software companies.

We are continualy working to apply the following strategies and tactics: 
- iterative design 
- asynchronous, inclusive communication 
- Lean product development practices 

And we do this all through an open core product with an all-remote, global team. This means that our definition of Product Operations is a [community contribution](https://about.gitlab.com/community/contribute/) product system that drives [efficiency](https://about.gitlab.com/handbook/values/#efficiency) and [results](https://about.gitlab.com/handbook/values/#results) without the traditional boundaries that restrain most product development methodologies. 

**Vision**:  A world class product system, that GitLab and all of GitLab’s customers and users can benefit from. 

**Mission**: Empower GitLab to be truly product-led and customer-centered, by iterating on Agile/Lean best practices with a remote-first mindset, one MR at a time.

#### Principles

- **We embody all [GitLab values](https://about.gitlab.com/handbook/values/), with a deep appreciation for [collaboration](https://about.gitlab.com/handbook/values/#collaboration).** Product Operations is only as good as the sum of its _contributions_ from the whole product development team and its stable counterparts. Teams that share accountability for (product) ownership invent better solutions and gain traction with less internal friction. These teams bond and rally around achieving a shared definition of success. 

- **We prioritize [results](https://about.gitlab.com/handbook/values/#results) and [efficiency](https://about.gitlab.com/handbook/values/#efficiency) by valuing outcomes over outputs.** This mindset is propelled by our core [product development flow](https://about.gitlab.com/handbook/product-development-flow/), which rallies us around the goal of delivering business impact to GitLab's customers and users with every iteration. Anything that doesn’t contribute to this overarching goal is waste and will be removed from our product system.

- **We continually align around [our product principles](https://about.gitlab.com/handbook/product/product-principles/#our-product-principles), with a focus on understanding the customer problem rather than assuming we know the solution.** A single problem will have multiple solutions with varying impact potential. Customer-focused teams  maximize value by prioritizing one problem to generate multiple solutions, rather than assuming a singular solution to solve multiple problems.

- **We build feedback bridges between Product and other teams, such as Marketing, Sales, Customer Success, UX/Design, and Engineering** The most important success metric for any (product) team is the time it takes to move through the Think-Make-Check (or Build-Measure-Learn) cycle. Teams that [iterate](https://about.gitlab.com/handbook/engineering/ux/how-we-work/#iteration) successfully and quickly through this cycle have continual access to qualitative and quantitative data through every phase of development and across every function in the business.

- **We grow high-functioning, cross-functional teams by creating flexible frameworks that support confident decision making along with the permission to fail, which in turn fuels velocity.** When teams have [psychological safety](https://about.gitlab.com/handbook/leadership/emotional-intelligence/psychological-safety/), they are more likely to take risks and move faster, which ultimately leads to [bigger ideas](https://about.gitlab.com/handbook/engineering/ux/thinkbig/) and better [MVCs](https://about.gitlab.com/handbook/product/product-principles/#the-minimal-viable-change-mvc) toward those ideas. These teams lose judgment in favor of trust. 

#### Challenges 

**Finding the right balance of synchronous versus asynchronous collaboration as an all-remote team.** GitLab has a [unique culture ](https://about.gitlab.com/company/culture/all-remote/values/) of combined independence and ownership as a fully remote organization. We've carved the path for successful remote work with our [transparent](https://about.gitlab.com/company/culture/all-remote/values/#transparency) handbook first approach, which has also propelled a highly productive asynchronous team with an [everyone can contribute](https://about.gitlab.com/company/strategy/#contribute-with-gitlab) mindset. However, as GitLab has gone from fewer than 350 to over 1200 employees, we've seen some of our asynchronous processes fail serve us at scale.

At scale, leaning too much toward asynchronous results in opportunity gaps to collaborate on optimal outcomes within and across teams, and even operational chaos. Too much synchronous communication creates the overly burdened "red tape" operational inefficiencies that traditional companies suffer from across large teams and various time zones. So as we grow our team and our userbase, we have to continue adjusting the levers to establish the right convergence and divergence points, for product development teams and across the business.

**Creating and maintaining a psychologically safe space as we grow our headcount.** To date, GitLab has succeeded with a culture that is reasonably flat and [values](ttps://about.gitlab.com/company/culture/all-remote/values/) that promote level agnostic collaboration and contributions across the organization regardless of functional role. We've embraced a [bias toward action](https://about.gitlab.com/handbook/values/#bias-for-action), raising MRs to make micro improvements that can be quickly improved, revised or even removed, minimizing the fear of failure.

As we grow larger teams to support bigger enterprise customers, especially as an all remote team, it becomes more challenging to build human to human relationships within in and across teams to building trust and safety. And because every change we introduce into our system or into our product can have significant positive and negative impacts on thousands of our users, with real revenue ramification for larger customers, functional roles get more defined and the stakes become higher. This opens the door to hierarchy driven systems, competitively motivated behavior and fear based decision making, which don't promote risk-taking, voicing alternative opinions and creativity, which are the types of behavior that lead to innovative breakthroughs.

There is no one formula for this challenge, which all maturing organizations face. We'll have to continually lean into opportunities to build empathy amongst our teams by building frameworks to provide resources that foster positive emotions of curiosity, confidence, and inspiration.

**Providing access to actionable product usage data for GitLab product development teams** As GitLab's userbase grows, collecting quantitative data to identify patterns and trends while leveraging qualitative data to better understand the reason behind those patterns and trends will be key to delivering meaningful impact to customers. However, GitLab is committed to [user data privacy](https://about.gitlab.com/privacy/) and respects user-requested boundaries on data collection. Alternatives to product usage data, such as user surveys and testing can help fill some of the gaps but are not as scalable or unbiased. And while leveraging product usage data from SaaS users can yield meaningful insights for Self-managed users as well, it's not always an apples to apples comparison. We need to continually work on finding the balance between user privacy and providing the product teams access to the business intelligence they need to deliver meaningful impact to customers. Concurrently, we need to minimize the complexity and manual nature of effort needed by product development teams to access the most meaningful data available to them in the system by prioritizing instrumentation and providing standardized dashboards for teams to leverage and customize. 

#### Opportunities

**Continuing GitLab's leadership in the "all remote" work revolution.** While the Covid pandemic has given remote working an unexpected relevance and urgency for organizations worldwide, even prior to the pandemic remote working had begun taking precedence due proof around the core, repeatable benefits*:

- _Productivity — Teleworkers are an average of 35-40% more productive than their office counterparts, and have measured an output increase of at least 4.4%_
- _Performance — With stronger autonomy via location independence, workers produce results with 40% fewer quality defects_
- _Engagement — Higher productivity and performance combine to create stronger engagement, or in other words, 41% lower absenteeism_ 
- _Retention — 54% of employees say they would change jobs for one that offered them more flexibility, which results in an average of 12% turnover reduction after  remote work agreement is offered_
- _Profitability — Organizations save an average of $11,000 per year per part-time telecommuter, or 21% higher profitability_

As a successful all-remote organization, GitLab is already familiar with the benefits of workplace flexibility. We know that remote teams have the ability to move fast, in both parallel and non-parallel workflows, achieving optimal efficiency. And by dogfooding our own [all-remote guide](https://about.gitlab.com/company/culture/all-remote/guide/) and [all-remote cultural tips](https://about.gitlab.com/company/culture/all-remote/tips/), we've demonstrated remote teams are  not just more productive, but happier and more innovative when autonomy and ownership are combined, allowing a focus on goals/outcomes rather than hours/outputs to measure success. With remote-first working practices more at the forefront than ever, GitLab as an opportunity to continue leading by example, helping organizations across various industries, not just technology. We can help teams lose the disadvantages of in-office work, such as office politics. And we can help teams translate the benefits of in-office culture, such the empathy that comes from human connection, to an all-remote environment. Product Operations will work cross-functionally to ensure GitLab's product team best practices are continually [handbook first](https://about.gitlab.com/company/culture/all-remote/guide/#handbook-first-documentation) such  GitLab and non-Gitlab teams can benefit from our learnings, in alignment with our value of [transparency](https://about.gitlab.com/company/culture/all-remote/values/#transparency).

*Statistics reference via [Forbes](https://www.forbes.com/sites/laurelfarrer/2020/02/12/top-5-benefits-of-remote-work-for-companies/?sh=51d1271716c8)

**Productizing our product development successes for the benefit of our users as well as the benefit of GitLab as a business.** As an open core product, we have the unique advantage that what we build for ourselves at GitLab can be productized for our customers and improved upon by our users. This means that every strategy/tactic we leverage to optimize our internal processes, be it an automation such as [release post item generator](https://about.gitlab.com/handbook/marketing/blog/release-posts/#release-post-item-generator) or workflow optimization such as [scaling our release post](https://docs.google.com/presentation/d/1_Osx3FrDxT4aqjl-Kc9QXgp30z0Pl1k4tBCv0DlZYkk/edit), can and should be built into our product fo our users. Building it into our product can be as simple as an update to our transparent and highly referenced [handbook](https://about.gitlab.com/handbook/) or [iterating on our release feature](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8641) over multiple cycles to optimize our release notes process. Product operations will treat all of Gitlab's product development practices, such as a [product development flow](https://about.gitlab.com/handbook/product-development-flow/product-development-flow-draft.html), as a product to be optimized, dogfooding with the intention of improving existing or introducing new GitLab features to operationalize successful outcomes for the benefit of GitLab and non-Gitlab teams. 

## Product operations resource library

Product Operations will collect and populate helpful content here on a regular basis. Any team member is welcome to contribute to the library by raising an MR and assigning it to the [Product Operations PM](https://gitlab.com/fseifoddini).

### Team health

[High performing teams need psychological safety](https://hbr.org/2017/08/high-performing-teams-need-psychological-safety-heres-how-to-create-it)

[The five keys to a successful (Google) team](https://rework.withgoogle.com/blog/five-keys-to-a-successful-google-team/)

### Product management skills

[Communication](https://miro.com/blog/communication-product-managers-product-school/)

## Product operations survey results

### Paid NPS

[GitLab Paid NPS: Q3 2020](https://docs.google.com/presentation/d/1bXM7hGlV_INTJrzObO_4VlIj1zkfYn6JIqvOCjHJHZo/edit#slide=id.g59bfc474c5_2_145)

[GitLab Paid NPS: Q2 2020](https://docs.google.com/presentation/d/1hi0opmahxsv_FLVtSj6m4zMFmXWK0YfJxHq7wUqO-Zs/edit#slide=id.g59bfc474c5_2_145)

[GitLab Paid NPS: Q1 2020](https://docs.google.com/presentation/d/1AqlpFl_3jUPyGHqCp9ZRy2NFOf-AyT-WhF-8JoIzgCk/edit#slide=id.g59bfc474c5_2_145)

### Post purchase

[GitLab PPS: Q3 2020](https://docs.google.com/presentation/d/1NVb3ecscPhcQMC_wpznbXsDqKkK-09--6OhSj3bclXs/edit)

[GitLab PPS: Q2 2020](https://docs.google.com/presentation/d/1zKuJ1TMcdX8PaLi-qF1NeVM3pQ21wnr7ERRkZRliMJo/edit)

### Product development flow

[Product develoment flow: Q2 2020](https://docs.google.com/presentation/d/1BxUVcoPyjYkR7MOHn1aZGllMCqlH5eb27JKM_bb_uDA/edit)

## Product Operations Areas of Focus

### Operationalizing Outcome Driven Products
We aim to minimize manual processes in favor of flexible automations that compliment intentional and precisely timed human touch points. The outcome is repeatable, customizable and [efficient](https://about.gitlab.com/handbook/values/#efficiency) workflows that GitLab's product teams can leverage to build and release best in class features for Gitlab's users. 

#### Recently completed

<%= direction["prodops:direction,Product Operations,prodops:outcomes"]["recent"] %>

#### In Progress and Upcoming

<%= direction["prodops:direction,Product Operations,prodops:outcomes"]["all"] %>

### Building Qualitative & Quantitative Feedback Loops

We lean into opportunities to grow internal and external communication channels for GitLab's product development teams and their stable counterparts. The outcome is the collection and exposure of actionable user feedback and data that can be utilized across the business to deliver impactful [results](https://about.gitlab.com/handbook/values/#results) to GitLab's users. 

#### Recently completed

<%= direction["prodops:direction,Product Operations,prodops:feedbackloops"]["recent"] %>

#### In Progress and Upcoming

<%= direction["prodops:direction,Product Operations,prodops:feedbackloops"]["all"] %>

### Scaling Product Knowledge

We collaborate to identify best practices for product development and create [transparency](https://about.gitlab.com/handbook/values/#transparency) of knowledge across teams in an ongoing and remote-friendly way. The outcome is a collection of written, video and interactive content in the GitLab [handbook](https://about.gitlab.com/handbook/#product) that is easy to consume and adopt for GitLab's product managers as well as product managers throughout the tech community.

#### Recently completed

<%= direction["prodops:direction,Product Operations,prodops:knowledge"]["recent"] %>

#### In Progress and Upcoming

<%= direction["prodops:direction,Product Operations,prodops:knowledge"]["all"] %>

### Suggesting edits to this page

Product operations welcomes contributions to this page! Please feel free to raise an MR and assign to the [Product Operations PM](https://gitlab.com/fseifoddini) for collaboration. 
