---
layout: markdown_page
title: "Category Direction - Auto Devops"
description: "GiitLab's direction for “Auto DevOps” is to leverage our single application to assist users in every phase of the development and delivery process. Learn more!"
canonical_path: "/direction/configure/auto_devops/"
---

- TOC
{:toc}

## Auto DevOps

Our direction for “Auto DevOps” is to leverage our [single application](https://about.gitlab.com/handbook/product/single-application/) to assist users in every phase of the development and delivery process, implementing automatic tasks that can be customized and refined to get the best fit for their needs.

With the dramatic increase in the number of projects being managed by software teams (especially with the rise of micro-services), it's no longer enough to just craft your code. In addition, you must consider all of the other aspects that will make your project successful, such as tests, quality, security, deployment, logging, monitoring, etc. It's no longer acceptable to add these things only when they are needed, or when the project becomes popular, or when there's a problem to address; on the contrary, all of these things should be available at inception.

[Watch this video of our CEO Sid explaining the importance of Auto DevOps](https://www.youtube.com/watch?v=i7qL1dS5x8g) and follow along with this issue where we are organizing to increase [Auto DevOps adoption](https://gitlab.com/gitlab-com/Product/-/issues/1801). 

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AAuto%20DevOps)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/480) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## Vision

DevOps Adoption is [a known pain point](https://about.gitlab.com/direction/ops/#opportunities) that GitLab, as a complete DevOps platform delivered as a single application, can help alleviate. That adoption isn't just hard technically, it's also a challenge organizationally.  The vision for Auto DevOps is to ease that adoption pain. We will serve as a central mechanism for enabling users to continue to further their DevOps adoption journey by automatically implementing best practices, and recommending iterative improvements where appropriate. 

There is no current tool in the market that does what Auto DevOps is capable of doing. The best analogy we've found is this - consider that you are trying to navigate from New York to California by car in the year 1992. To get there it would be painful, even with a map because it is a multi-variate problem - one that requires lots of inputs and more than just instruction manuals and best practices. Now imagine you are doing that today with Google Maps on your phone? Google Maps removes all of the pain of those best practices, employs data to intelligently move beyond them and recommends pit stops if you ask. 

Auto DevOps will be something similar for DevOps practices. Today it can provide encoded best practices. In the future, Auto DevOps will become the navigation assistant for your DevOps journey - a guide for the journey of continuous improvement. 

This vision offers enormous benefit to users confronted with the pain of adopting DevOps, and it serves our business. As a recommendation engine for our single platform - Auto DevOps can improve the discoverability of valuable capabilities to our users, increasing their complete platform adoption.

### Target persona

The primary users of Auto DevOps are the developers who want to focus exclusively on the business code they have to write. We want to provide them all the other requirements necessaary to run their devops workflows.

Our secondary persona is the platform engineer who - in large organisations - provides the templates and tools necessary for compliant processes and deployments. We want to serve their needs, so they can enable their developers to use Auto DevOps, and reduce the pain of DevOps adoption.

## What's Next & Why

We're pursuing ways to increase [Auto DevOps adoption](https://gitlab.com/gitlab-com/Product/-/issues/1801) through providing a more integrated setup. 

We are currently looking into customer insights to learn more about the required use cases, major pain points and current limitations of the existing Auto DevOps offering.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and our next maturity target is [Viable](/direction/maturity/). See the [Auto DevOps viable](https://gitlab.com/groups/gitlab-org/-/epics/1333) epic for more info.

### Group Ownership

Auto Devops ties together several feature from across GitLab [product categories](/handbook/product/categories/). Each individual feature will have its own maturity classification. 

| Feature | Responsible GitLab Group  |
| ------ | ------ |
| Auto Build | [Configure](/handbook/engineering/development/ops/configure/) |
| Auto [Test](/direction/verify/code_testing/)  | [Testing](/handbook/engineering/development/ops/verify/testing/) |
| Auto [Code Quality](/direction/verify/code_quality/)  | [Testing](/handbook/engineering/development/ops/verify/testing/) |
| Auto [SAST](/direction/secure/static-analysis/sast/)  | [Static Analysis](/handbook/engineering/development/secure/static-analysis/) |
| Auto [Secret Detection](/direction/secure/static-analysis/secret-detection/)  | [Static Analysis](/handbook/engineering/development/secure/static-analysis/) |
| Auto [Dependency Scanning](/direction/secure/composition-analysis/dependency-scanning/) | Composition |
| Auto [License Compliance](/direction/secure/composition-analysis/license-compliance/)  | Composition |
| Auto [Container Scanning](/direction/protect/container-scanning/)  | Container Security |
| Auto [Review Apps](/direction/release/review_apps/) | [Progressive Delivery](/handbook/engineering/development/ops/release/progressive-delivery/) |
| Auto [DAST](/direction/secure/dynamic-analysis/dast/)  | Dynamic Analysis |
| Auto [Browser Performance Testing](/direction/verify/web_performance/)  | [Testing](/handbook/engineering/development/ops/verify/testing/) |
| Auto [Load Performance testing](/direction/verify/load_testing/)  | [Testing](/handbook/engineering/development/ops/verify/testing/) |
| Auto [Deploy](/direction/release/continuous_delivery/)  | [Progressive Delivery](/handbook/engineering/development/ops/release/progressive-delivery/) |
| Auto [Monitoring](/direction/monitor/)  | [APM](/handbook/engineering/development/ops/monitor/apm/) |

## Competitive Landscape

While there are "piece-meal" solutions that offer to automate a particular stage, there are no comprehensive tools that offer to address the entire devops lifecycle.

### DeployHQ

DeployHQ offers to "Automatically build and deploy code from your repositories", however, its UX is complex that its deployment targets limited.

[deployhq.com](https://www.deployhq.com)

## Analyst Landscape

There is currently no analyst category that aligns with Auto DevOps.

## Top Customer Success/Sales Issue(s)

[Use Auto DevOps for design.gitlab.com](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/96)

## Top Customer Issue(s)

[Add support for AWS ECS deployments to Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ce/issues/38430)

## Top Internal Customer Issue(s)

[Use Auto DevOps for design.gitlab.com](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/96)

#### Top Vision Item(s) 

- [Disable Auto DevOps at the Group level for gitlab.com](https://gitlab.com/gitlab-org/gitlab-ce/issues/52447) 

- [Composable Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ce/issues/47234)

- [Don't run Auto DevOps when no dockerfile or matching buildpack exists](https://gitlab.com/gitlab-org/gitlab-ce/issues/57483)
