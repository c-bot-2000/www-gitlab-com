---
layout: markdown_page
title: "Product Direction - Monitor"
description: "The Monitor stage comes after you've configured your production infrastructure and deployed your application to it. Learn more here!"
canonical_path: "/direction/monitor/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product direction for Monitor. If you'd like to discuss this direction
directly with the product managers for Monitor, feel free to reach out to Sarah Waldner (PM of the [Health Group](/handbook/engineering/development/ops/monitor/health/)) ([GitLab](https://gitlab.com/sarahwaldner), [Email](mailto:swaldner@gitlab.com) [Zoom call](https://calendly.com/swaldner-gitlab/30min)) or Kevin Chu (Group PM of Monitor) ([GitLab](https://gitlab.com/kbychu), [Email](mailto:kchu@gitlab.com) [Zoom call](https://calendly.com/kchu-gitlab/30min)).

## Overview

The Monitor stage comes after you've configured your production infrastructure and
deployed your application to it. 

The monitor stage helps you understand the health and performance of your system. When incidents occur, we enable you to streamline your incident response. 

## Mission
The mission of the GitLab Monitor stage is to provide feedback so that you can decreases the frequency and severity of incidents and improves operational and product performance.

## Landscape
The Monitor stage directly competes in several markets, including Application Performance Monitoring (APM), Log Management, Infrastructure Monitoring, IT Service Management (ITSM), Digital Experience Management (DEM) and Product Analytics. The total addressable market for the Monitor stage is projected to be [$2.7 billion](https://about.gitlab.com/handbook/product/investment/#total-and-service-addressable-markets) by 2024. 

All of these markets are well-established and crowded, with winning companies achieving spectacular growth as businesses continue to shift online.

Successful vendors, such as market leader [Datadog](https://www.datadoghq.com/) are leveraging a [platform strategy](https://mitsloan.mit.edu/ideas-made-to-matter/platform-strategy-explained) to expand their markets (see DataDog's [acquisition of Undefined Labs to expand beyond production applications to provide code insights during development](https://drive.google.com/file/d/1hq74bZvBv5nD6Krmi-iaSsdqA9ChOf1r/view), or their expansion to [incident management in 2020](https://www.datadoghq.com/blog/dash-2020-new-feature-roundup/)). Competition among market leaders today is also geared toward making the whole stack observable for enterprises. [New Relic's updated business model](https://blog.newrelic.com/product-news/new-relic-one-observability-made-simple/) reflects the need for vendors to capture increasing footprint (and spend) of enterprises while enabling future growth by making a significant part of their business free.

## Vision
The vision of the Monitor stage is to enable DevOps team to operate their application by enabling monitoring, observability, incident response, and feedback all within GitLab. This vision is part of the overall [GitLab vision](https://about.gitlab.com/direction/#vision) and enables teams to complete the DevOps loop within a single application.

## Strategy
To achieve our vision, our strategy is to:

1. Focus first on user adoption and dogfooding of Incident Management
1. Strengthen bi-directional product tie-in to other GitLab stage capabilities
1. Build a [boring](https://about.gitlab.com/handbook/values/#boring-solutions) Monitor/Observability solution that enbales customers to start using GitLab and move away from expensive Monitoring vendors

### Opportunities

1. Instrumentation is commoditized. GitLab will not need to invest in agents since [OpenTelemetry](https://opentelemetry.io) and most vendor agents are all open source.
1. With development shifting cloud-native and the massive community driven investment in tools and patterns, the opportunity to build boring solutions on top of the cloud-native solutions plays right to GitLab's strength.
1. Out-of-the-box monitoring capabilities saves time and money and lowers the bar on the expertise required for enterprises and start-ups. The ease by which most users can start monitoring their service, using established vendors, such as DataDog or New Relic, and newer competitors like Honeycomb, is something we should strive to emulate.
1. Monitoring is traditionally for production, there are opportunities to shift monitoring tools and techniques left.
1. Monitoring vendors are considered, in general, to be expensive. We can compete on cost when monitoring is part of the GitLab single application.

### Challenges

1. Monitoring vendors offer generous free tiers for smaller companies and complete solutions for enterprises.
1. Huge investments are made by market leaders. Market leaders are also expanding the scope of their solution. This makes them more sticky with their customers.


### Pricing
<%= partial("direction/ops/tiering/monitor") %>

## What's next

The Monitor surface area is large. Rather than continue to pursue bringing multiple products within the monitor purview to market concurrently, GitLab has consolidated the majority of its focus to Incident Management. This allows us to complete the [smart feedback loop](/direction/ops/#smart-feedback-loop) within a single DevOps platform as a first priority. With GitLab Incident Management's development timeline, our users will benefit from the advantage of enabling collaboration for incident response within the same tool as their source code management, CI/CD, plan, and release workflows - all within the same tool. This most effectively positions GitLab to gain market traction and user adoption. 

We still intend to invest in the Observability categories which include Metrics, Logging, and Tracing and this is top of mind. We need to be strategic as the Monitor team has limited bandwidth. In FY21Q1, we are working on a strategy that will allow us to start investing in the Observabitlity categories without slowing down on Incident Management. Check out more details on this [epic](https://gitlab.com/groups/gitlab-org/-/epics/5132).

The Monitor stage's goals from 2021-01 through 2021-04 are the following:

1. Mature the Incident Management category so that the GitLab SRE team can dogfood it
 * Key Result 1: [plan, execute, and collect feedback on GitLab's third game-day](https://gitlab.com/gitlab-org/gitlab/-/issues/285284)
 * Key Result 2: SRE team is using Incidents in place of Issues
 * Key Result 3: Release minimal version of [on-call schedule management](https://gitlab.com/groups/gitlab-org/-/epics/3960)

2. Grow estimated SMAU for Monitor to 15,000 users (target for FY20Q4 was 12,000 users and was achieved)


You can see our entire public backlog for Monitor at this
[link](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Monitoring);
filtering by labels or milestones will allow you to explore. If you find
something you're interested in, you're encouraged to jump into the conversation
and participate. At GitLab, everyone can contribute!

## Performance Indicators (PIs)

Our [Key Performance Indicator](https://about.gitlab.com/handbook/ceo/kpis/) for the Monitor stage is the [Monitor
SMAU](https://app.periscopedata.com/app/gitlab/758607/Centralized-SMAU-GMAU-Dashboard?widget=10039330&udv=0) ([stage monthly active users](https://about.gitlab.com/handbook/product/metrics/#monthly-active-users-mau)).

As of December 2020, the Monitor SMAU is based on the [Incident Management category](https://about.gitlab.com/direction/monitor/debugging_and_health/incident_management/). It is the count of unique users that interact with alerts and incidents. This PI will inform us if we are on the right path to provide meaningful incident response tools.

<embed width="100%" height="100%" style="min-height:300px;" src= "<%= signed_periscope_url(chart: 10132068, dashboard: 769975, embed: 'v2') %>">

<%= partial("direction/workflows", :locals => { :stageKey => "monitor" }) %>

<%= partial("direction/categories", :locals => { :stageKey => "monitor" }) %>

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "monitor" }) %>
