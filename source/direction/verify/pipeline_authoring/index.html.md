---
layout: markdown_page
title: "Category Direction - Pipeline Authoring"
description: "Defining your pipelines is an important part of Continuous Integration, and we want to make it as easy as possible."
canonical_path: "/direction/verify/pipeline_authoring/"
---

- TOC
{:toc}

## Our Mission

Defining your pipelines is an essential step to getting started with Continuous Integration. In the Ops Section [direction](/direction/ops/#operations-for-all) this is the gateway to enable "Operations for All", where our goal is to make the authoring experience as easy as possible for both novice and advanced users alike. We believe that creating a green pipeline quickly and easily will help development teams leverage our CI to increase their efficiency. One of the ways we measure success is by improving the [time to green pipeline](https://gitlab.com/gitlab-org/gitlab/-/issues/232814).

## Our vision

<iframe width="560" height="315" src="https://www.youtube.com/embed/hInM7JUEH4Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

As a practitioner of [Speedy, Reliable Pipelines](/direction/ops/#speedy-reliable-pipelines), GitLab wants your time to a green pipeline to be the fastest on any platform.

When you're setting up something new, and especially when learning a new CI platform, there can be a lot to take in, and it can be hard to even know what you don't know, and what kinds of options and strategies are available to you. This is why our focus over the next three years is to create an amazing authoring experience in a way that leads to getting your first green pipeline as quickly as possible while leveraging all the available features and functionalities GitLab CI can offer.

### Our top vision items are:

  - We started our work for a [dedicated pipeline authoring area](https://gitlab.com/groups/gitlab-org/-/epics/4540), so we can facilitate features that would make the authoring experience as easy as possible 
  - Adding a [pipeline visual builder](https://gitlab.com/gitlab-org/gitlab/-/issues/15754) to make it easy to see the options that are available to you what the pipeline will actually look like when it runs. 
  - In addition to the templates for entire pipelines that we have today. We would like to add [Job templates](https://gitlab.com/gitlab-org/gitlab/-/issues/280802). This will make it easier to set up simple pipelines and get to that first green pipeline.
  - Improving the linter and integrating it better with the editor to give smart feedback on not just errors, but provide warnings and advice when editing your pipeline. This will accelerate the learning curve more information can be found in the  [improvements to the CI Linter](https://gitlab.com/groups/gitlab-org/-/epics/3517) and [Make Editor Lite more aware of .gitlab-ci.yml MVC](https://gitlab.com/groups/gitlab-org/-/epics/4067) 
  - [Templates](https://gitlab.com/groups/gitlab-org/-/epics/4858) make it easier to get started with more kinds of pipelines, for example ML data pipelines, mobile app development builds, and so on. These tend to be possible with GitLab today, but we can improve the documentation, templates, and other resources for getting up and running quickly for more kinds of users who are looking to begin adopting GitLab CI.

## Who we are focusing on? 

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Pipeline Authoring, our "What's Next & Why" are targeting the following personas, as ranked by priority for support: 

1. [Sasha - Software Developer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. [Devon - DevOps Engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)

### Everyone can contribute 

If you have any feedback on our 3 year vision which you would like to share please do so in the [Pipeline authoring 3 year vision](https://gitlab.com/groups/gitlab-org/-/epics/4534)


There are some exciting opportunities we've yet to begun to explore:

- Automatic generation of pipeline config based on what's in the repository or what's already configured. We have this with Auto Build (part of [Auto DevOps](https://docs.gitlab.com/ee//topics/autodevops/index.html)) to some extent, but this can continue to be evolved. You could imagine features in the pipeline builder that use ML models to generate build, test, and deploy jobs automatically based on detecting the language and tech stack. One path to unify all this could be to bring Auto Build features directly into CI and make them more composable rather than turning Auto DevOps completely on or off.
- Analytical models for how pipelines are performing based on their configuration, and automatically suggesting (or even applying) performance improvements to the configuration in order to achieve better results. Our Testing team for example is looking at similar ideas for automatically testing everything as efficiently as possible.
- A robust library with more granular templates that will allow to quickly build your pipeline with easy to search and customize building blocks.

### Additional Resources

For more general information on CI direction see also the general [Continuous Integration category direction](/direction/verify/continuous_integration). You may also be looking for one of the following related product direction pages: [GitLab Runner](/direction/verify/runner/), [Continuous Delivery](/direction/release/continuous_delivery/), [Release stage](/direction/ops#release), or [Jenkins Importer](/direction/verify/jenkins_importer).

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3APipeline%20Authoring)
- [Topic Labels](https://gitlab.com/groups/gitlab-org/-/labels?utf8=%E2%9C%93&subscribed=&search=ci%3A%3A) (because Pipeline Authoring is such a large category, it is broken out into topic areas)
- [Overall Vision](/direction/ops/#verify)

## What's Next & Why

* The work on 13.9 scoped, and you can see the assigned issues on our [pipeline authoring 13.9 planning issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/4) and the [kickoff video](https://www.youtube.com/watch?v=xZAg9-RFaFQ&t=4s). In this milestone, we work toward creating a first-class authoring experience as described in our 3y vision. The scope of work for 13.9 will be focused on incremental improvements to the pipeline editor and popular requests such as [Reference YAML block from another file](https://gitlab.com/gitlab-org/gitlab/-/issues/266173) and [Support multiple cache in a single job](https://gitlab.com/gitlab-org/gitlab/-/issues/32814).

The planning for version 13.10 is undergoing, you can view and contribute directly to the [planning issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/5)

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our [definitions of maturity levels](/direction/maturity/)), it is important to us that we defend that position in the market. As such, we are balancing prioritization of [important P2 issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Apipeline%20authoring&label_name[]=priority%3A%3A2) and [items from our backlog of popular smaller feature requests](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Apipeline+authoring&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93) in addition to delivering new features that move the vision forward. If you feel there are any gaps or items that would risk making authoring pipelines in GitLab no longer lovable for you, please let us know.

## Competitive Landscape

Our main competitor doing innovative things in terms of pipeline authoring is GitHub, who have evolved Actions into more and more of a standalone CI/CD solution. GitLab remains far ahead in a lot of areas of CI/CD that they are going to have to catch up on, but Microsoft and GitHub have a lot of resources and have a large user base excited to use their product, especially when given away as part of a package. Actions has a more event-driven and community plugin-based approach than GitLab, whereas we take community contributions directly into the product where they can be maintained.

GitHub actions are a seemingly powerful toolkit with a high potential for low maintainability with community contributions as we have seen with Jenkins. We are monitoring to swiftly incorporate the best of their innovation into our product. We've already had some successes [running GitHub Actions directly in GitLab CI](https://gitlab.com/snippets/1988376) and will continue to explore that. We are also working on a [migration guide](https://gitlab.com/gitlab-org/gitlab/-/issues/228937) to help teams we're hearing from who are looking to move over to GitLab have an easier time. Features like [making the script section in containers optional](https://gitlab.com/gitlab-org/gitlab/-/issues/223203) would make it easier to build reusable plugins within GitLab that would behave more like Actions and [Allow `needs:` (DAG) to refer to a job in the same stage](https://gitlab.com/gitlab-org/gitlab/-/issues/30632) to enable users to run an entire pipline without defining stages. 

 Watch this walkthrough video of Github actions 
 
 <figure class="video_container">
  <iframe src="https://www.youtube.com/embed/OlgXHnCPZZs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Top Customer Issue(s)

Our top customer issues ([search](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3APipeline+Authoring&label_name%5B%5D=customer&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93)) include the following:

- [Make it possible to control build status using exit codes](https://gitlab.com/gitlab-org/gitlab/-/issues/16733)
- [Allow `needs:` (DAG) to refer to a job in the same stage](https://gitlab.com/gitlab-org/gitlab/-/issues/30632)
- [Make it possible to identify a new/empty branch with workflow:rules](https://gitlab.com/gitlab-org/gitlab/-/issues/15170)

## Top Internal Customer Issue(s)

Our top internal customer issues ([search](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3APipeline%20Authoring&label_name[]=internal%20customer)) include the following:

- [Pipeline doesn't succeed when manual jobs using new DAG dependency "needs:" are waiting for other "when: manual" jobs to succeed](https://gitlab.com/gitlab-org/gitlab/-/issues/31264)
- [Add support for `when:manual` within triggered pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/201938)

Our top dogfooding issues ([search](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3APipeline%20Authoring&label_name[]=Dogfooding)) are:

- [Use artifact relative URLs to fetch Knapsack and Flaky tests metadata](https://gitlab.com/gitlab-org/gitlab/-/issues/32222)
- [Add syntax for importing a job that is "virtual" so it is not run by default](https://gitlab.com/gitlab-org/gitlab/-/issues/31304)

## Analyst Landscape

Pipeline Authoring is not independently analyzed as an analyst category. See our [Continuous Integration Direction](/direction/verify/continuous_integration) for this content.



- [Allow `needs:` to refer to a job](https://gitlab.com/gitlab-org/gitlab/-/issues/30632)
