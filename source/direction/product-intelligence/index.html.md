---
layout: markdown_page
title: Product Direction - Product Intelligence
description: "Product Intelligence manages a variety of technologies that are important for GitLab's understanding of how our users use our products. Learn more here!"
canonical_path: "/direction/product-intelligence/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

As part of our overall [3 year Strategy](https://about.gitlab.com/company/strategy/#three-year-strategy) GitLab is striving to build a [Customer Centric](https://about.gitlab.com/company/strategy/#3-customer-centricity) DevOps platform through Strong Data Insights. In order to empower our customers to achieve their goals, and ultimately enable GitLab to ship a world class DevOps product, we must provide the necessary data and reporting so all teams within the business can identify opportunities, mitigate risks, and make the right decisions. By providing robust and accurate reporting we can reduce the cycle time from when we release a change, to when we know its impact to overall product usage, and customer experience, helping all of GitLab reach our goals through operational efficiencies, R&D allocation/investment, development priority, etc. 
 
To accomplish this, we aim to leverage our deep expertise, tied with investments in a strong technical foundation to enable all teams within GitLab to produce, analyze, and report on product data by FY23-Q4. Like many top software/SaaS companies we are moving to be a product-led and data-driven organization. Through a partnership with our Data Team and collaboration with Product and Engineering we are cultivating a strong data focused culture within GitLab, driving to support our 30 year [BHAG company goal](https://about.gitlab.com/company/strategy/#big-hairy-audacious-goal) of "becoming the most popular collaboration tool for knowledge workers in any industry". 

## Guiding Principles

In order to build the best DevOps product we can, and provide the most value for our customers, we need to collect and analyze usage data across the entire platform so we can investigate trends, patterns, and opportunities. Insights generated from our Product Intelligence program enable GitLab to identify the best place to invest people and resources, what categories to push to maturity faster, where our UI experience can be improved and how product changes effect the business. 

We understand that usage tracking is a sensitive subject, and we respect and acknowledge our customers' concerns around what we track, how we track it, and what we use it for. To protect and empower our users the Product Intelligence team follows these guiding principles when developing solutions and picking technologies:

**Transparency**

We will always be transparent about what we track and how we track it. In line with our company's value of [Transparency](https://about.gitlab.com/handbook/values/#transparency), our tracking source code and documentation will always be public.

**User Access and Control**

We will provide a simple and easily accessible Privacy Control Center, where users can view what they have opted-in to, what is being tracked, and update privacy settings. 

## Challenges we face in Product Intelligence

- GitLab's [single application approach to DevOps](/handbook/product/single-application/) creates a product that is both wide and deep, encompassing a large collection of features used by many teams within an organization, which are composed of different types of users.

- That depth/breadth makes it exceedingly complex to properly map out and understand how our diverse customer set is using the product and gaining value. 

- We currently are unable to provide GitLab the required data to identify opportunities and make the right decisions against them. 

- GitLab's MVC approach to product development introduces frequent changes to the product stages and what data is available, making historical trend analysis difficult

## How We Work

For more information on Product Intelligence, you can checkout our [Product Intelligence Guide](https://about.gitlab.com/handbook/product/product-intelligence-guide/) which details a [high-level overview of how we make data usable](https://about.gitlab.com/handbook/product/product-intelligence-guide/#product-intelligence-overview), the [Collection Frameworks](https://about.gitlab.com/handbook/product/product-intelligence-guide/#collection-framework) we leverage, our [Event Dictonary](https://about.gitlab.com/handbook/product/product-intelligence-guide/#event-dictionary), and much more!

## Top Priorities and Deliverables

Product Intelligence provides the necessary frameworks, tooling, and expertise to help us build a better GitLab. Naturally we sit in the middle of many projects, initiatives and OKRs at GitLab. In order to provide clarity and realistic expectations to our stakeholders and customers we practice ruthless prioritization([per Product Principle #6](https://about.gitlab.com/handbook/product/product-principles/)), identifying what is above the line, what is below, and what is unfunded and not possible for us to action on in a given timeline. 

### Product Performance Indicators

Product Intelligence top priority is to build out the infrastructure and analytics frameworks for our Product and Engineering Teams to instrument analytics throughout the GitLab platform, provide guidance on best practices and enforce standardization. 

To learn how we set these at the Product level checkout [Product Performance Indicators](https://about.gitlab.com/handbook/product/performance-indicators/).

### Support Sales/Customer Success

Teams Involved: Sales, Customer Success, Product, Product Intelligence, Data

We are working with Sales and Customer Success to [document the metrics they need](https://docs.google.com/spreadsheets/d/1ZR7duYmjQ8x86iAJ1dCix88GTtPlOyNwiMgeG_85NiA/edit#gid=0) so we can map out a path to delivering them over time.

## Our Roadmap (As of 12/1/2020)

### Resolve Data Integrity Concerns

Teams Involved: Product, Product Intelligence, Data, Infrastructure, Sales, Customer Success

**Product Intelligence Responsibilities:**

- Collaborate with Data and Customer Success to identify data integrity concerns
- Map and prioritize identified issues to establish a path to resolution
- Work with Product and Engineering to resolve any instrumentation problems
- Work with Product, Engineering, CS, Infrastructure, etc to resolve any integration problems
- Establish a set of standards for new database tables and records
- Establish necessary auditing processes to ensure we do not recreate these issues in the future

**Why This Is Important:**

As GitLab has grown, and our analytics tracking has expanded we are finding areas where the integrity and accuracy of our data needs to be reviewed. Product Intelligence is working with internal stakeholders and teams to map out and identify [these areas](https://docs.google.com/spreadsheets/d/1bqgEweYgSI99uK5CoViH_U0NAKOuaQtR4xPXLFBspoE/edit#gid=0) so we can formulate resolutions.

**Recent Progress:**
 
 - Kicked Off investigation and documentation 
 - Working through high level summary of data integrity concerns

 **What's Next:**

 - Link and Create issues for each integrity issue to a global epic for Product Intelligence to champion
 - Prepare summary for stakeholders and leadership
 - Present summary and recommended path forward
 - Prioritize!

### Privacy Policy and Settings - Rollout Updated Privacy Policy

Teams Involved: Legal, Product Intelligence, Data, Security

**Product Intelligence Responsibilities:**

- Privacy Policy updates
- Internal and External Communication 
- Consolidate Privacy Settings into a single location in the Product
- Expand user control over analytics settings

**Why This Is Important:**

- Our [privacy policy](https://about.gitlab.com/privacy/) has not been updated since the [October 29, 2019 Update on free software and telemetry](https://about.gitlab.com/blog/2019/10/10/update-free-software-and-telemetry/). This policy needs to be updated to align with GitLab's strategy and our Product Intelligence direction.
- Our customers and users provided significant feedback on this change and we are working to respect and action against it while supporting our business needs.

**Recent Progress:**

- Draft privacy policy is in place and being reviewed
- We're working on resourcing and designing a Privacy Control Center

**What's Next:**

- [Privacy Policy](https://gitlab.com/groups/gitlab-com/-/epics/907) - work through draft privacy policies and product intelligence policies, gather internal and community feedback, create a communications and rollout plan.
- Privacy Control Center - we're building a centralized privacy settings page in GitLab for users to easily understand and control their privacy settings.

### Data Collection - Maintain and Scale Usage Ping

Teams Involved: Product Intelligence, Data, Product Managers

**Product Intelligence Responsibilities:**

- Expand Collection Framework
- Maintain Event Dictionary
- Expand and Guide Instrumentation
- Optimize and Maintain Usage Ping Generation
- Maintain and Scale Usage Ping Collector	

**Why This Is Important:**

- Product Metrics are currently instrumented by all product teams to track key metrics of usage. We're close to 3x the number of metrics tracked since the beginning of this 2020, as such, we need to ensure metrics are added in a structured and standardized way. Usage Ping on GitLab.com takes over 32 hours to generate at this time, which is 4x longer than at the beginning of this year. In order for all this instrumentation to be as valuable as possible we need to reduce the amount of time it takes to generate and be accessible by the business.

**Recent Progress:**

- Over 800 metrics tracked in Usage Ping
- Product Intelligence review process in place
- Usage Ping and Snowplow documentation
- Client and server-side counts can now be tracked on SaaS and Self-Managed.
- Client and server-side events are a work in progress on Self-Managed.
- Plan-level reporting using SaaS Usage Ping is not possible as SaaS is multi-tenant and Usage Ping reports at an Instance level.
- We've implemented an [Event Dictionary for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/4174)

**What's Next:**

- Usage Ping Standardizations and Data Integrity - create a standardized naming convention, define usage ping metrics in a centralized YAML file, generate the event dictionary automatically from this file.
- Usage Ping Parallelization - compute usage ping metrics in parallel instead of serially. 
- Usage Ping Collector - build out a versioned Usage Ping collector endpoint to collect restructured Usage Ping data.

### Data Collection - Enable Snowplow Event Tracking

Teams Involved: Product Intelligence, Data, Infrastructure, Product

**Product Intelligence Responsibilities:** 

- Expand Collection Framework
- Maintain Event Dictionary
- Expand and Guide Instrumentation
- Support Product Usage Analysis
- Maintain and Expand Snowplow Collector
- Maintain and Scale Snowplow Enricher

**Why This Is Important:**

Snowplow allows us to collect a richer set of usage data, build more complex data modeling and usage funnels, and gain a deeper understanding of user actions and paths through the product. 

**Recent Progress:**

- Proof on concept for Product Analytics Snowplow and Usage Ping built.

**What's Next:**

- Snowplow Tooling, Standardizations, and Linking with Usage Ping - Standardize Snowplow schemas and instrumentation methods, create an Event Dictionary for Snowplow, build out development and testing tools along with documentation. Finish tying Usage Ping and Product Intelligence's Snowplow together specifically merging tracking classes into a single class with multiple destinations.
- Scale Product Intelligence - Take ownership of our Snowplow Infrastructure and maintain the snowplow collector and enricher. Scale Product Intelligence's internal database and collector while working towards supporting an external database and collector

### Processing Pipeline - Decrease Cycle Times for Product Intelligence

Teams Involved: Product Intelligence, Data, Infrastructure, Product

**Product Intelligence Responsibilities:** 

- Release Cycle
- Product Usage
- Usage Ping Generation
- Usage Ping Collector
- Extractors
- Loaders
- Snowflake Data Warehouse

**Why This Is Important:**

The Data Availability cycle times are currently a 51 day cycle and our exports of the Versions DB are currently done manually every 14 days according to [this schedule](https://gitlab.com/groups/gitlab-data/-/epics/162)  In order for all this instrumentation to be as valuable as possible we need to reduce the amount of time it takes to generate and be accessible by the business. 

**Recent Progress:**

- Proof of concept built for exporting data from Versions application using CI pipelines.

**What's Next:**

- [Automate Versions DB Exports](https://gitlab.com/gitlab-org/product-intelligence/-/issues/398) - currently Usage Pings are stored in the Versions DB and this is manually exported into the data warehouse every 14 days. This is a manual process by both the infrastructure and data engineering teams. This will allow us reduce import times from 14 days to 1 day.	
- Generate Usage Ping Daily - currently Usage Pings are generated every 7 days, this change will make generating and sending Usage Ping every day. This allows us to reduce generation times from 7 days to 1 day.	

### Processing Pipeline - Plan and Group-level Reporting for SaaS

Teams Involved: Product Intelligence, Data 

**Product Intelligence Responsibilities:** 

- Collection Framework
- Usage Ping Generation
- Collectors
- Processors
- Snowflake Data Warehouse
- dbt Data Model

**Why This Is Important:**

Currently Usage Ping is not segmented by pricing tier, which means for any SaaS free / paid account segmentation cannot be done using Usage Ping. Instead, as a work around, the data team is using the Postgres database import and manually recreating all Usage Ping queries in Sisense. A single usage ping on SaaS takes 32+ hours to generate, for group level metrics, we need to run this 1 million times which is only feasible if it's done in the data warehouse.

**Recent Progress:**

- [Usage Ping SQL queries exported](https://gitlab.com/gitlab-org/product-intelligence/-/issues/423)

**What's Next:**

- Export Usage Ping Queries 
- export SQL queries for all Postgres based metrics, find a way to export non-SQL queries for Usage Ping metrics in Redis, integrations, settings.
- Data Modelling and Scheduling - setup the Snowflake EDW to run Usage Ping query workload, build dbt Base Data Models and Product Data Models, setup transformation pipeline schedule.

### Product Performance Indicators

**Why This Is Important:**

Supporting Product Perfomance Indicators empowers our R&D Teams to be data driven and to demonstrate what value they are adding to the platform. These indicators inform the business on where to invest and what customers are leveraging. 

**Product Intelligence Responsibilities:**  
- Instrumentation 
- Sisense Dashboard
- Performance Indicators
- Metrics Reviews
- Product Improvements

Teams Involved: Product Managers, Product Intelligence, Data

**Why This Is Important:**

- Product performance indicators help guide each of our product sections, stages, and groups to be metrics driven.

**Recent Progress:**

- FY21-KR1 Delivered: (EVP, Product) [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
- FY21-KR2 Delivered: (EVP, Product) [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343) 
- All [stage and group performance indicators](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/) have been added to the handbook. 
- Process defined for [implementing product performance indicators](https://about.gitlab.com/handbook/product/product-intelligence-guide/#implementing-product-performance-indicators)

**What's Next:**

In order to support [100% of DevOps groups have Predicted GMAU (or Paid GMAU)](https://gitlab.com/gitlab-com/Product/-/issues/1610) we are working with the PMs of each group that still needs to finalize their GMAU metrics so the Data Team can apply the Predicted GMAU formula to their dashboards. 

- [Create : Static Site Editor GMAU Instrumentation](https://gitlab.com/gitlab-org/gitlab/-/issues/233994)
- [Create : Ecosystem GMAU Instrumentation](https://gitlab.com/gitlab-org/gitlab/-/issues/225678)
- [Verify : Testing GMAU Instrumentation](https://gitlab.com/groups/gitlab-org/-/epics/2991)
- Secure : Fuzz Testing - Confirming instrumentation is functioning properly
- Secure : Threat Insights - Confirming instrumentation is functioning properly
- Geo : Enablement GMAU Instrumentation - Confirming instrumentation is functioning properly
- [Intersect and union across PIs](https://gitlab.com/groups/gitlab-org/-/epics/4210) - allow the ability to combine multiple metrics and PIs together.
- Define metrics review process - align sections on monthly review process.

### Project Compass

**Why This Is Important:**

- Supporting Project Compass is critical to GitLab as it increases our sales & marketing efficiencies andempowers us to react to market and customer trends faster.

**Product Intelligence Responsibilities:**  
- dbt Base Data Models Product Data Models 
- Customer Success Data Models
- Enterprise Dimensional Models 
- Snowflake EDW to Salesforce Data Pump 
- Salesforce Dashboards 
- Salesforce to Gainsight Data Feed
- Gainsight Dashboards 
- Customer Conversations

Teams Involved: Sales, Customer Success, Product Intelligence, Data

**Why This Is Important:**

- Project Compass is important as it increases our sales & marketing efficiencies.

**Recent Progress:**

- [Versions App Reporting has been replicated in Sisense Dashboards](https://gitlab.com/gitlab-data/analytics/-/issues/4488/)
- A datafeed has been setup from Sisense to Salesforce
- License data has been added to this data feed

**What's Next:**

- Customer Success Data Models - create SCM, CI, and DevSecOps use cases.
- Tie Product Usage Data to Fulfillment Data - tie together our Usage Pings with license and customer information.
- SaaS Group/Account Level Product Usage + SaaS Product Data Models	- feed SaaS account level data into Salesforce and Gainsight

### Below the Line

As Product Ananlytics sits in the center of many of GitLab's initiatives and OKRs, there are items we want to work on, but are unable to prioritize with current resources and team. THese items are still important and will provide value, but are below the priority line for us at this time:

- Product Intelligence UI for Customers - We aim to provide the data we collect through our Product Ananlytics process to our users so they can understand their usage of GitLab. Currently we are focusing our available resources in building out the long term infrastructure and frameworks to support this in the future, either by building it or collaborating with other Groups at GitLab overtime.
- We need to roll Product Intelligence out to our other internal applications to expand our available data sets and coverage reporting (ex: versions app, licsense.gitlab.com, etc)

## Quick Links

| Resource                                                                                                                          | Description                                               |
|-----------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| [Product Intelligence Guide](/handbook/product/product-intelligence-guide)                                                                              | A guide to Product Intelligence                   |
| [Usage Ping Guide](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html)                                              | An implementation guide for Usage Ping                    |
| [Snowplow Guide](https://docs.gitlab.com/ee/development/product_analytics/snowplow.html)                                                  | An implementation guide for Snowplow                      |
| [Event Dictionary](/handbook/product/product-intelligence-guide#event-dictionary)                                        | A SSoT for all collected metrics and events               |
| [Privacy Policy](/privacy/)                                                                                                       | Our privacy policy outlining what data we collect and how we handle it     |
| [Implementing Product Performance Indicators](/handbook/product/product-intelligence-guide#implementing-product-performance-indicators)                                   | The workflow for putting product performance indicators in place   |
| [Product Intelligence Direction](/direction/product-intelligence/)                                                                              | The roadmap for Product Intelligence at GitLab                       |
| [Product Intelligence Development Process](/handbook/engineering/development/growth/product-intelligence/) | The development process for the Product Intelligence groups         |
