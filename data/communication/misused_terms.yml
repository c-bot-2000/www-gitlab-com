# Example:
#
# - avoid: Term to be avoided
#   avoid_description: An optional field to expand on the context of the term itself.
#   use_instead:
#    - Terms to be used
#    - Another, as many as applicable
#   reason: |
#     Explain and provide some links if needed. You can use markdown.
#
#
# If the terms to avoid are not literal or are too common, feel free to add this
# to not include the entry in the exports (see "They" entry):
#
#   exclude_from_exports: true
#

- avoid:
  - GitLabber
  - International team member
  use_instead: GitLab team member
  reason: The wider community shouldn't be excluded from being a GitLabber, we [use team member instead](/company/team/structure/#team-and-team-members). By definition, we are all international.

- avoid: IPO
  use_instead: Becoming a public company
  reason: We might do a direct listing instead of an offering.

- avoid:
    - Enterprise Edition
    - EE
  use_instead: One of our [paid tiers](/pricing/). If you are talking about users of the Enterprise Edition, use "subscribers" or "paid users"
  reason: |
    [EE is a distribution](/handbook/marketing/strategic-marketing/tiers/#history-of-ce-and-ee-distributions), and is not a product. EE should not be used to describe a tier or where a feature goes as per our [messaging do's and don'ts](/handbook/marketing/strategic-marketing/tiers/#messaging-dos-and-donts). [Read more about GitLab's distinct distributions here](/install/ce-or-ee/). Not all people who use EE pay us, for example those on the [Free tier](/pricing/) or members of our [community programs](/community/), so we should not refer to paying customers as "EE users".

- avoid:
    - Community Edition
    - CE
  use_instead: Open source distribution, open source codebase, or open source project
  reason: |
    [CE is a distribution](/handbook/marketing/strategic-marketing/tiers/#history-of-ce-and-ee-distributions), and is not a product. It should not be used to describe a tier or where a feature goes as per our [messaging do's and don'ts](/handbook/marketing/strategic-marketing/tiers/#messaging-dos-and-donts). [Read more about GitLab's distinct distributions here](/install/ce-or-ee/)

- avoid: Hi guys
  use_instead:
    - Hi people
    - Hi everyone
  reason: We want to use [inclusive language](/handbook/values/#inclusive-language--pronouns)

- avoid: Aggressive
  use_instead: Ambitious
  reason: We want to [attract a diverse set of people](https://www.huffpost.com/entry/textio-unitive-bias-software_n_7493624)

- avoid: Employees
  use_instead: Team members
  reason: We have team members who are [contractors](/handbook/total-rewards/compensation/compensation-calculator/#contract-factor)

- avoid: Resources
  use_instead: People
  reason: We are more than just our output.

- avoid: Community
  use_instead: Wider community to refer to people outside of the company
  reason: |
    [People at GitLab are part of the community too](/handbook/communication/#writing-style-guidelines).

- avoid: Radical transparency
  use_instead: Intentional transparency
  reason: Radical tends to be absolute and infers a lack of discretion. We are thoughtful and intentional about many things — [informal communication](/company/culture/all-remote/informal-communication/), [handbook-first documentation](/company/culture/all-remote/handbook-first-documentation/), [onboarding](/company/culture/all-remote/building-culture/#intentional-onboarding), etc. — and have [exceptions to transparency](/handbook/communication/#not-public).

- avoid: Sprint
  use_instead: Iteration
  reason: We are in it [for the long-term](/handbook/values/#under-construction) and [sprint implies fast, not quick](https://hackernoon.com/iterations-not-sprints-efab8032174c).

- avoid: Grooming
  use_instead: Refinement
  reason: The term has [negative connotations](https://pm.stackexchange.com/questions/24133/difference-between-grooming-and-refinement) and the word refinement is used by the [Scrum Guide](https://www.scrumguides.org/revisions.html).

- avoid: Obviously
  use_instead: Skip this word
  reason: Using things like "obvious/as we all know/clearly" discourages people who don't understand from asking for clarification.

- avoid: They
  avoid_footnote: Do not refer to other teams at GitLab externally as "they" (e.g., "Well, they haven't fixed that yet!").
  use_instead: The Product team (example)
  reason: |
    There is no "they" or "headquarters" - there is only us as team members, and it's one team.
    You could instead say something like "The Product team is looking at that issue...".
    When used in the context of a pronoun, they is as valid as he, she, or any other pronoun someone decides to use.
  exclude_from_exports: true

- avoid: Deprecate
  use_instead:
    - Delete
    - Remove
  reason: We usually mean we're removing a document or feature whereas deprecation is actually leaving the thing intact while discouraging its use. Unlike other terms on this list, there *are* times where "deprecate" is the right word, for example in formal [product process](/handbook/product/gitlab-the-product/#deprecating-and-removing-features).

- avoid:
    - Diverse candidate
    - Diversity candidate
    - Diverse team member
    - Diversity team member
  use_instead: Someone from an underrepresented group
  reason: |
    A single person cannot be "diverse." Using "diverse" as a noun is an "othering" term. Instead, someone may be from an underrepresented group. It is okay to describe a team or any collection of people as "diverse", however.

- avoid: Non-technical
  avoid_footnote: used to describe a person
  use_instead: |
    Usually people mean "non-engineering."
  reason: Everyone has a technical knowledge that they used to perform in their role.

- avoid:
    - Search
    - Elasticsearch
  use_instead: |
    - [Global Search](/direction/enablement/global-search/) with the features Basic Global Search and [Advanced Global Search](https://docs.gitlab.com/ee/user/search/advanced_global_search.html).
    - [Log Search](/direction/monitor/apm/logging/) for the monitor stage logging feature.
    - Issue Search and Merge Request search for filtering and searching in those content areas.
    - [Elastic log Stack](https://docs.gitlab.com/ee/user/clusters/applications.html#elastic-stack) when referring to Elasticsearch and Filebeat.
  reason: There are many kinds of search in the GitLab application; use complete feature names to describe areas.

- avoid: Telemetry
  use_instead: |
    - Seat link is number of seats on a license
    - [Version check](/handbook/sales/process/version-check/) is what version of GitLab a subscription is using
    - [Usage ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#usage-ping) is high level data of how GitLab is being used in a license
    - [Snowplow](/handbook/business-ops/data-team/platform/snowplow/) is a dynamic site for web event analytics
    - [Google Analytics](/handbook/tools-and-tips/#google-analytics) is a static site for receiving data from `about.gitlab.com` and `docs.gitlab.com`
    - Database events is using the database records of Gitlab.com to look at how people are using the application.
    - Product Usage Data is [data collected](https://about.gitlab.com/handbook/product/product-intelligence-guide/#collection-framework) on the customer's account to be used within GTM tools such as Salesforce, Marketo, or Gainsight to improve retention and expansion
  reason: Avoid this word since it has the connotation of third-party analytics. It also is too broad so use something more specific.

- avoid: Rule of thumb
  use_instead: Guideline or Heuristic
  reason: Rule of thumb is of [dubious historical origins](https://www.phrases.org.uk/meanings/rule-of-thumb.html) and can be considered a violent or non-inclusive term.

- avoid: Deadline
  use_instead: Due date
  reason: Deadline [may have violent origins](https://en.wikipedia.org/wiki/Time_limit#Origin_of_the_term) and we use due date in our user interface for issues.

- avoid: Grandfathered
  use_instead: Legacy
  reason: Grandfathered has [discriminatory origins](https://en.wikipedia.org/wiki/Grandfather_clause).

- avoid:
    - Whitelist
    - Blacklist
  use_instead:
    - Allowlist
    - Denylist
  reason: Whitelist/Blacklist have connotations about value and map to racial terms.

- avoid: GitLab monitoring dashboard
  use_instead: GitLab metrics dashboard
  reason: Monitoring dashboard is too generic and is often confused with the GitLab's self-monitoring features.

- avoid: Second-class citizen
  use_instead:
    - Different class of team member
    - Being treated like you don't belong/like an outsider
  reason: |
    When referencing people, "different class of team member" or "being treated like you don't belong/like an outsider" is preferred when discussing such environments, since citizen has political and socioeconomic connotations. In a [hybrid-remote](/company/culture/all-remote/hybrid-remote/#disadvantages-to-hybrid-remote) environment (e.g. not GitLab, which is [all-remote](/company/culture/all-remote/guide/#why-remote)), those outside of the office may feel inferior or deprioritized compared to coworkers who routinely work onsite.

- avoid:
    - Top of the hour
    - Bottom of the hour
  use_instead: Start of the meeting
  reason: While most timezones in the world are offset from UTC by a whole number of hours, a few are offset by [30 or 45 minutes](https://www.timeanddate.com/time/time-zones-interesting.html).

- avoid: |
    Middleman
  use_instead: |
    Go-between, intermediary
  reason: It is gender-specific and therefore exclusionary.

- avoid: |
    Poor man's solution
  use_instead: |
    [Boring solution](/handbook/values/#boring-solutions)
  reason: It is gender-specific and implies that someone with less money would come up with a substandard solution compared to someone who has more money.

- avoid: Senior moment
  use_instead: I forgot
  reason: We should not associate forgetfulness with senior citizens.

- avoid:
    - Biweekly
    - Bimonthly
  use_instead:
    - Twice weekly/monthly
    - Every two weeks/months
  reason: Biweekly and bimonthly are ambiguous terms.

- avoid: Functional group leader
  use_instead: |
    [E-group](/handbook/leadership/#e-group) leader
  reason: |
    The term "functional group leader" is a past term that has now been updated. See [Org Structure](/company/team/structure/#organizational-structure).

- avoid: Functional group
  use_instead: |
    Department or team (depending on the context)
  reason: |
    "Functional group" is a past term that is no longer used. See [Org Structure](/company/team/structure/#organizational-structure).

- avoid:
    - Learning
    - Playbook
    - FAQ
    - Standard Operating Procedure
    - Training
  use_instead: The entire Handbook is the our standard operating procedure.
  reason: |
    Using these terms can cause confusion and lead to duplicate content. For example: Call it Contract Negotiation Handbook instead of Contract Negotiation Playbook

- avoid:
    - White-collar workers
    - Blue-collar workers
  use_instead:
    - Knowledge worker
    - Trade worker
  reason: The original terms are outdated references and have socioeconomic connotations. There are also other [color classifications](https://en.wikipedia.org/wiki/Designation_of_workers_by_collar_color) that are less common.

- avoid:
    - Customer names mentioned in recorded or streamed meetings
  use_instead:
    - Refer to a Google doc agenda listing the customer name
    - Use a codename
  reason: We want to train ourselves to not mention customer names to allow us to be as [transparent](/handbook/values/#transparency) as possible.  Whenever possible, we want to live stream our meetings to YouTube and to do that we need to be in the habit of not mentioning customer names. So we should avoid mentioning them even in [privately streamed meetings](/handbook/marketing/marketing-operations/youtube/#visibility).
  exclude_from_exports: true

- avoid:
    - Portal
  use_instead:
    - |
      [CustomersDot](https://about.gitlab.com/handbook/engineering/development/fulfillment/architecture/#customersdot)
    - |
      [LicenseDot](https://about.gitlab.com/handbook/engineering/development/fulfillment/architecture/#licensedot)
    - |
      [Stripe](https://about.gitlab.com/handbook/engineering/development/fulfillment/architecture/#stripe)
    - |
      [Zuora](https://about.gitlab.com/handbook/engineering/development/fulfillment/architecture/#zuora)
  reason: The (Customer) Portal is an overloaded term which has come to represent multiple aspects in GitLab's Fulfillment process. To disambiguate the different components of the system we should use more specific language.

- avoid:
    - Black Mark
  use_instead:
    - Blemish
    - Blot
  reason: This phrase is meant to refer to a situation where someone remembers something you did and holds it against you unfairly in a later circumstance. However it can unintentionally reinforce the idea that black is associated with negative or undesirable traits.

- avoid:
    - Poaching
    - Raiding
  use_instead:
    - Voluntary Attrition
    - Recruiting
  reason: In the recruiting industry these are common terms to refer to companies targeting other company's team members to hire them away. But they are violent and warlike. So it's better to use the more literal, technical terms. While we want to keep voluntary attrition low as a company we should never forget that it is great for our team members if they get lots of great offers from other companies.

- avoid:
    - High Availability
  use_instead:
    - Reference Architecture
    - Scaled Architecture
  reason: All solutions come with a [trade-off between cost/complexity and uptime](https://about.gitlab.com/solutions/reference-architectures/). Each availability component comes with its own complexity and maintenance. Users should thoroughly analyze the benefits of a solution against its costs and the team's readiness.

- avoid:
    - Final Solution
  use_instead:
    - Completed Solution
  reason: While often natural to use this phrase when referring to a theoretical end-point of a developing feature there is [historical sensitivity](https://www.merriam-webster.com/dictionary/final%20solution) around the term and its use during World War II.

- avoid:
    - Homepage
  use_instead:
    - Marketing Homepage
    - Homepage Content 
  reason: Both about.gitlab.com and gitlab.com has a [homepage](https://www.merriam-webster.com/dictionary/home%20page). Use "marketing homepage" to reference the homepage of [about.gitlab.com](https://about.gitlab.com/) and "homepage content" to reference the homepage of [gitlab.com](https://gitlab.com).
