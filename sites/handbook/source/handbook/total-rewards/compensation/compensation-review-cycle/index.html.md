---
layout: handbook-page-toc
title: Compensation Review Cycle
description: "On this page, we explain how we review our Compensation Calculator and carry out the Compensation Review Cycle."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

On this page, we explain how we review our Compensation Calculator and carry out the Compensation Review Cycle.

## Contact Total Rewards
If you have any feedback or questions about the compensation review cycle, please contact [Total Rewards](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group).

## Compensation Review Cycle

GitLab goes through 2 compensation reviews:

1. **Annual Compensation Review** - Happens in the fourth quarter of each year. This is when Compensation Calculator inputs are reviewed and when majority of compensation is reviewed for team members. Any changes will be processed with an effective date of February 1st.
1. **Targeted Mid-Year Increase Review** - For team members hired in November to January. The process starts in August with an effective date of September 1st.

## Annual Compensation Review

The most up to date timeline of the Annual Compensation Review process can be found in the [Total Rewards Schedule](/handbook/total-rewards/#total-rewards-schedule).

During the fourth quarter of each year, the Total Rewards team will conduct a compensation review to ensure all team members are paid based on market rates in the [compensation calculator](/handbook/total-rewards/compensation/compensation-calculator/). This is not a Cost of Living Adjustment, but instead a review of market changes. Location Factor will continue to be a part of the compensation calculator equation.

Increases for Compensation Review Cycles are based on 1) assessing market changes in the compensation calculator inputs and 2) performance in the role.

The increase percentage may vary for each person. If a team member was recently adjusted, the annual adjustment might yield no additional salary during the annual compensation review. This review acts as a sweep for each team member’s compensation to be evaluated at least once per year. If there is an increase in Location Factor and/or Benchmark, team members affected will have the new location factors applied to their compensation, but would not receive an automatic percent increase. For example, if the location factor in a region increases by 10% the team member will not receive an automatic 10% adjustment. The team member would receive an increase aligned to their benchmark, location factor, and performance taking any budget constraints into account.

#### Process overview

```mermaid
graph LR
    start((Annual review<br/>process<br/>kicks off))
    step_manager_review(Manager determines<br/>provisional performance factor<br/>of team members)
    step_exec_review_1(Executive review)
    step_tr_calculate(Total Rewards<br/>team calculates<br/>proposed increases)
    step_manager_compaas(Manager review<br/>in Compaas)
    step_exec_review_2(Executive review)
    step_manager_final(Manager informs<br/>direct reports of<br/>adjustment to<br/> compensation)
    stop((New<br/>compensation<br/>effective 1<br/>Feb 2021))

    start-->step_manager_review
    step_manager_review-->step_exec_review_1
    step_exec_review_1-->step_tr_calculate
    step_tr_calculate-->step_manager_compaas
    step_manager_compaas-->step_exec_review_2
    step_exec_review_2-->step_manager_final
    step_manager_final-->stop
```

### Eligibility

All team members hired on or before October 31st will be eligible to be evaluated as part of the Annual Compensation Review cycle. Team members hired after this date will be reviewed to ensure their salary does not fall below the bottom of their compensation band based on increases to benchmark or location factor. If this does occur, the team member will be adjusted to the bottom of the band during the Annual Compensation Review cycle.

### Annual Compensation Review Timeline

##### November:
1. A member of the Total Rewards team will join the People Business Partner sync on a monthly basis at least one month before Annual Compensation Review begins.
1. Benchmarks and Location Factors
  * The Total Rewards team will review all benchmarks and location factors associated with the Compensation Calculator and propose revised inputs to the Compensation Group for approval/implementation.
1. Performance Factors
   * The Total Rewards team will reach out the managers and People Business Partner to obtain performance factors for each active team member with a hire date on or before October 31st.
   * Remember that this is also a good time to update your team’s position description if it does not reflect the role.
   * Performance Factors should be confirmed in the assessment tool/BambooHR no later than November 30th.

##### December:
1. Once all performance factors have been reviewed and approved the executive leaders of each group will review the breakdown with the Total Rewards team along with the People Business Partner for their group.
     * In this breakdown, leaders will be able to analyze all individual performance factors, the distribution of performance factors overall and by department, as well as an [underrepresented group audit](/handbook/total-rewards/compensation/compensation-review-cycle/#underrepresented-group-audit). Total Rewards will outline any concerns based on the aggregated data for the leader and people business partner to review.
1. The Total Rewards Team will schedule a sync with the Recruiting team to review proposed iterations.     
1. Using the revised inputs above, the Total Rewards team will calculate a minimum market increase for each team member to stay at the minimum of the FY22 range using the exchange rate as of 2020-12-01.
1. Anyone on a formal Performance plan (PEP, PIP) that is ongoing through December 15 will be excluded from the Annual Compensation review. December 15th is the due date for emailing Total Rewards with PBP in CC about team members on a formal performance plan.
1. Anyone who is departing from GitLab, with an end date in until February 1, will be excluded from the Annual review cycle. The People Business Partner will make sure to loop in Total Rewards on any terminations.
1. December 15 - December 18: People Business Partners bring any preliminary budgets concerns to Divisional leaders (or department leaders as applicable).

##### January:
1. January 7th - January 22nd: [Manager Review](/handbook/total-rewards/compensation/compensation-review-cycle/#manager-review) in Compaas
  * Each division leader is responsible for making sure their group stays within budget. The company has a [6% budget](/handbook/total-rewards/compensation/compensation-review-cycle/#fy-2022-annual-compensation-review-budget) for all Market (location factor and benchmark) and Performance increases.
  * Managers and indirect managers will review the minimum recommended increase from Total Rewards and then propose an increase based on the performance factor for their team members. The minimum recommended increase from Total Rewards is not a cap, but instead a minimum value to ensure alignment to the new ranges based on performance and market adjustments.
  * Depending on budget constraints, the increases for the individual team members may be adjusted up or down by management and reviewed by Total Rewards.
1. The People Business Partner team would advise managers to directly align proposed increases for team members with their manager (Senior Manager/Director) over 1-1s before Submitting.
  * Hereafter divisional leaders (or department leaders as applicable) can review and discuss tentative budgetary impact for the division (or department as applicable).
1. January 14th: People Business Partner check on the progress/Budget impact.
1. January 20th: Aim to have all Managers/Senior Managers/Directors reviewed their slates. Justifications must be included in the notes section for all increases exceptions (for example: any above 10%, out of band exceptions due to contract factor updates, etc.) for slates to be complete. These justifications will be reviewed and approved by Department and Division leaders. 
1. January 20 - January 22nd: Department Heads/People Business Partners review budget impact and deficiencies, before moving to E-Group.
1. January 22nd - January 27th: For E-Group to review and approve final compensation changes.
1. January 27th - February 1st: Total Rewards will present all compensation changes to the Compensation Group for final approval.

##### February:
1. February 1st - Februrary 4th: Total Rewards Analysts will update [BambooHR](https://www.bamboohr.com/d/), and notify all payroll providers to be effective February 1st. Total Rewards will be generating a "Total Rewards Statement" inclusive of Cash Compensation changes and current equity value for managers to use in communicating compensation changes in FY22.
1. Once the People Group gives the ok: Managers will inform the team members of the increase and adjustments to compensation compared to their performance. Team members will need to be notified no later than February 12th.

### Duties and Responsibilities

The Total Rewards team and People Business Partners both play a vital role in ensuring the success of Annual Compensation Review. The Total Rewards team handles more of the "big picture" company-wide in the set up and administration of Annual Compensation Review while the People Business Partners take on more of the "details" related to their division.

#### Total Rewards

The Total Rewards team is responsible for setting up Annual Compensation Review, analytics, audits, and general communication. This includes (but is not limited to):
* Ensuring all analyses (below) are created on time and all data inputs are reviewed and refreshed for the next Fiscal Year.
* Setting up Compaas with eligible team members, managers, and approvers. Determining minimum recommended increases and loading these into Compaas. Providing Compaas with each team member's benchmark % increase, location factor % increase, and suggested increase parameters according to their performance factor.
* Communicating timelines, what process is taking place, instructions, and deadlines to all relevant team members.

#### People Business Partners

People Business Partners work with their teams to address any problems, help the Total Rewards team with communications, and help their team/executive review Compaas and any analyses that are created. This can include (but not limited to):
* Reviewing any concerns highlighted from the Total Rewards team during audits with their executive and determining with them the best course of action.
* Reviewing and editing communications to be sent out in order to reduce anticipated questions or confusion. They also assist with following up with team members where action is needed; for example, if a manager hasn't gone into Compaas yet and the deadline is approaching.
* Working with their teams to stay in budget.
* Managing departmental review processes and timelines to meet the overall company timeline.

### Analyses created for Annual Compensation Review

#### Benchmarking Review

Benchmarking review is an internal process completed by the Total Rewards team to determine any adjustments to role benchmarks for the next FY.

##### Process

1. Create a copy of the `Benchmarking Analysis` sheet for the upcoming FY.
1. Ensure all job codes from the `Job Codes` sheet are in the `Benchmarking Analysis` and pull in updated Radford and Comptryx job codes.
1. Refresh the Radford and Comptryx survey tabs with the most recent data available.
1. Ensure the IC, Manager, Director, and Sales benchmark tabs have been updated with our currently used job codes.
1. Ensure formulas are in place for each row of the "Radford/Comptryx Job Codes" tab and that the median of the 50th and 75th are properly pulling into the benchmarks tabs.
1. Use the `Job Codes` sheet to refresh the current FY21 benchmarks. Compare our current benchmarks to the median of the 50th and 75th of the survey data (depending on the role) and make any recommendations based on market fluctuations.
1. Once the new benchmarks are reviewed and approved, these are updated in the [handbook](https://gitlab.com/gitlab-com/people-group/peopleops-eng/compensation-calculator/-/blob/master/data/job_families.yml).

##### Timing

The benchmark review is completed in November.

#### Location Factor Review

The location factor review is an internal process completed by the Total Rewards team to determinate any adjustments to location factors for the next FY.

##### Process

1. Create a copy of the `Location Factor Review` sheet for the upcoming FY.
1. Ensure all location factors, including ones added throughout the year and ones marked for further review, have been added to the analysis.
1. [Pull in refreshed data](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-review-cycle/#pulling-survey-data-and-matching) for all data sources: ERI, Robert Half, Dice, Radford, and Comptryx.
   1. Check these resources for new matches to our location factors and pull data for all available metros.
1. Using the refreshed data, compare the new data to our current location factors for fluctuations noting that we will not decrease a location factor.
    1. Analyze the average data for all locations in a geographical region to determine the location factor for that geographical region.
    1. Analyze all data for metro areas and countries separately to determine which metro areas and countries will need to be separated from the location factor determined for the geographical region.
    1. If the formula is overwritten, notate the reasoning why.
1. Once the new locations factors are reviewed and approved, these are updated in the [handbook](https://gitlab.com/gitlab-com/people-group/peopleops-eng/compensation-calculator/-/blob/master/data/location_factors.yml).

###### Pulling Survey Data and Matching

**ERI**

1. Log in to [ERI](https://online.erieri.com/account/login?returnurl=/). Login details are stored in 1Password.
1. Navigate to the Geographic Assessor.
1. Navigate to the Comparison List - Cost of Labor tab.
1. Set salary levels to $100,000 and $150,000.
1. Select all locations by clicking Add Location and adding all locations to your current list. This may need to be broken up into different lists to comply with ERI's list limits.
1. Download the excel file for all locations and add to the `Location Factor Review` sheet in the ERI tab.
1. Pull in the previously matched GitLab localities from the previous year's location factor review into the ERI tab.
1. On the Analysis tab, review locations without a match to ERI to see if there is now a match on the ERI tab.
    1. For cities, only add the exact match, not everything that would fall into the locality. For example, for San Francisco, California, I would match it to San Francisco in ERI's data set, but not Oakland. If two cities are listed in GitLab's locality, for example Berlin/Leipzig, Germany, match to both cities if available in the data.
    1. For All and Everywhere Else localities, for example All,  Nigeria or Everywhere else, Netherlands, match to the state/province/country average data. Everywhere Else data will be deflated in the analysis to account for the metro area being included in the state/province/country average.

**Robert Half**

1. Download the salary guide from each country Robert Half operates in. The list of countries Robert Half operates in and their respective websites where you can download the salary guide are located [here](https://www.roberthalf.net/).
1. Information from Robert Half has to be manually pulled out of their PDFs to our sheet.
    1. For the US and Canada, enter the location differential to the right of each location in the Robert Half tab. Create a new line for each location that isn't currently captured and match it to our data if we have the location as a locality.
    1. For locations in other countries, the salaries by role will need to be compared between the country being reviewed and the US. The San Francisco differential reported by Robert Half should be applied to the salaries for the US and used to calculate a differential to the salary guides in other countries.
1. Once differentials have been calculated for each global location, any new locations that do not currently have a match to one of GitLab's localities should be matched.

**Dice**

1. Download the most recently published [Dice Technology Salary Report](https://techhub.dice.com/Dice-2020-Tech-Salary-Report.html).
1. Information from Dice has to be manually pulled out of their PDF to our sheet.
1. Add the salaries for each city and state reported by Dice to the Dice tab, adding a new line for any new cities or states.
1. Match any Dice locations to the corresponding GitLab locality, if applicable.

**Radford**

1. Log in to [Radford](https://radford.aon.com/).
1. Navigate to the Data tab and then the Market Queries tab on the Data tab.
1. Select a location in the Query dropdown that corresponds to the tabs in the `Radford Data` sheet for both the Global Technology Survey and Global Sales Survey. In the Select Jobs dropdown, select All Jobs. In the Data Elements dropdown, select ACR. Click Save & Run. Navigate to the reports tab to download once the report has finished running. Note that more than one report can be run at a time.
1. Repeat these steps until all tabs have been refreshed.
1. Once all locations that were used last year have been refreshed, check for new locations by opening any query, select Add A Breakout, select a country, and use the Region/City dropdown to see if there are any new locations.
1. If there are new locations, they should be added as a separate query. To do this, in the Select a Query dropdown select Create New Query. Name the Query after the location. You'll want to complete these steps for both the Global Technology Survey and Global Sales Survey. Again, select Add A Breakout, select the country, and in the Region/City dropdown select the place you added as the Query name. Name the breakout after the location as well. Click save and follow the steps from step 3 to run the report.
1. Once the report is downloaded, add this to the corresponding tab in the `Radford Data` sheet, combining the Global Tech and Sales Surveys.
1. On the SF tab, scroll all of the way to the right and add two new columns for each new location. Copy the formulas from two other columns and update the vlookup to reference the correct location for these columns.
1. Add the new location(s) to the Location Factors tab and take the average of the location's factor column in the SF tab.
1. Match the new location(s) to a GitLab locality in this tab as well as any location that haven't previously been matched, if applicable.

**Comptryx**

1. Log in to [Comptryx](https://comptryx.mercer.com/).
1. Click Global Pay in the upper right.
1. In the Select Population box, select all individual levels (ie P3, not P14) in the Selected Levels dropdown. Select all 3-letter functions in the Select Functions dropdown. Select all Metros and Countries in the Selected Locations dropdown.
1. Select Data Extract at the top. Under Select Pay Elements, check off Total Cash - Tgt (NA). Under Select Statistics, check off 50th %'ile and 75th %'ile. Select to include only market data. Submit the report. If the report freezes your computer, break it up into smaller chunks such as by pulling data by continent.
1. Import the report into the Comptryx - Data tab of the `Comptryx Data` sheet.
1. Add any new locations as a new column in the Analysis tab and apply the same formula from previous columns. Also add any new locations to the Location Factor tab at the bottom and drag down the formula.
1. Match the new location(s) to a GitLab locality in this tab as well as any location that haven't previously been matched, if applicable.

##### Timing

The location factor review is completed in November. It can be completed once the October ERI data refresh has been updated.

#### Underrepresented Group Audit

The Total Rewards team will run an audit twice throughout the Compensation Review Process to review any statistically relevant bias for underrepresented groups: after the Performance Factor submissions (December) to review distribution of performance ratings and after Manager Review (January) to review the distribution of discretionary increase allocations.

This analysis will be conducted at the department level, for each department leader to review, as well as all other indirect leaders up to the e-group level. The executive of each division as well as their People Business Partner will address any concerns with the performance factors or discretionary increase allocations submitted across their division.

##### Process

The Total Rewards Team will audit with the following data breakdowns (as aligned in our [Identity Data Metrics](https://about.gitlab.com/company/culture/inclusion/identity-data/)):
* Ethnicity (only in the United States as we work on adding globally relevant [ethnicity categories to BambooHR](https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/issues/347) for self-identification)
* Tenure
* Gender (as self reported in BambooHR)
* Geographic Region
* [Grade](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

The Total Rewards team has created a template to automatically sync for each department leader, indirect manager, and overall e-group leader by adding the information submitted into a master file for increased efficiency in data analysis.

##### People Business Partner Action Required

The People Business Partner should familiarize themself with the output of this analysis and what has been flagged. They will review any issues that have been flagged with the leader of their division and advise Total Rewards of any changes.

#### Minimum Market Increase

This will calculate the minimum amount necessary to ensure the team member is in range and will be loaded into Compaas as the minimum recommended increase from the Total Rewards team.

##### Process

1. Utilizing the `Comp Data Analysis and Modeling` sheet template, create a new report with all Annual Compensation Review eligible team members.
1. Overwrite their Location Factor and Benchmark with the updated inputs.
1. Create a column that flags anyone with a compa ratio below 0.8 and another column that outputs how much they would need to be increased to fall into range.
1. Create a copy of this report for each division for People Business Partners to review and create a report for Compaas to upload using their template.

##### Timing

Once all data inputs have been finalized, before Annual Compensation Review opens.

##### People Business Partner Action Required

Review and flag any concerns or possible data errors to the Total Rewards team.

### Compensation Change Freeze

To ensure a smooth process, the Total Rewards team will freeze any compensation changes relating to promotions as part of the Annual Compensation Review process leading into the [Manager Review](/handbook/total-rewards/compensation/compensation-review-cycle/#manager-review) portion of the program. Any compensation or promotion change requests need to be [submitted in BambooHR](/handbook/people-group/promotions-transfers/#for-managers-requesting-a-promotion-or-compensation-change) by 2020-12-04 with last available effective date of 2020-12-01 or 2020-12-16. If an exception needs to be made and a compensation change needs to be processed during this time, such as relocations or other reasons, please send an email to Total Rewards.

For internal transfers, Total Rewards will need a list of active applicants from Recruiting on roles as of 2020-12-16. If a team member takes an internal transfer, they will be removed from the Annual Compensation Review process as their compensation will be reviewed per the hiring process.

Any compensation changes that result from Annual Compensation Review will have an effective date of 2021-02-01. If a manager anticipates that a promotion is more suitable to process for a team member, the manager has the option to remove the team member out of the Annual Compensation Review process. If this is the case, please send an email to Total Rewards no later than 2020-12-16.

Promotions and compensation change requests will reopen in BambooHR on 2021-02-01. The effective date for these requests will be processed in the next available payroll date from the time of approval.

## Performance Factor

Performance Factors are assessed twice per year as part of the [Performance/Potential Matrix](/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix). The average performance output (developing - performing - exceeding) will be used for annual compensation review.

For FY22 compensation (effective February 1, 2021) we will only have the ability to have one assessment (conducted in November 2020). If a team has already submitted a Performance/Potential Assessment in Q3 FY21, they will not be required to submit another assessment in Q4, but may update their assessment if desired.

The People Group is developing guidelines and the assessment tool for how the Performance Factor will be determined/processed. [Related Issue](https://gitlab.com/gitlab-com/people-group/peopleops-eng/people-operations-engineering/-/issues/188).

### Performance Factor and Annual Compensation Review

Recommendations from Total Rewards for discretionary increases based on performance factor:
* Developing in role = 0-3% increase
* Performing in role = 3-7% increase
* Exceeding in role = 6-10% increase

<span style="color:red">**Your compensation increase may vary (up or down) from these percentages.**</span> These guidelines are meant to help ensure company-wide consistency. Actual increases may vary based on:

* Changes to local market rates  
* Where you fall in the compensation range
* Previous increases
* Budget constraints

Do not expect that the final discretionary increases will fall into these ranges, as performance is only one factor out of many that goes into compensation adjustments.

Managers should take into account any increases the team member received in the last fiscal year (for example, due to promotion, transfer, catch-up compensation review, etc.) or if the team member was recently hired and already aligned to market/performance rates. After a manager determines the performance increase percent, he or she should be able to clearly communicate to the team member how it was derived.

Increases are not capped at 10%. If a manager would like to suggest an increase higher than the guidelines, they will want to add a justification for each indirect manager to review and approve.

If a manager has a team member on their team who is red circled (paid over the top end of the compensation range), in order for the person to receive an increase, they will need to submit a compensation exception request with their People Business Partner to Total Rewards which will then be reviewed and approved by the Compensation Group. The Total Rewards team will notify the People Business Partners of the team members red-circled once the new ranges are set for proactive review.

### Communicating Performance Factor

Once all Performance Factors have been reviewed and approved and the Performance Factor Executive Audit has been complete, managers will be communicated to that they can share the final performance factor with their direct report. This will occur before Annual Compensation Review takes place intentionally so that way it is more focused on the growth and development of the team member rather than how this ties into compensation.

## Manager Review

As part of the new [Annual Compensation Review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review-timeline), managers and indirect managers will review, approve, and enter proposed salary increases to ensure that we are paying each team member to market and prioritizing increases based on performance while staying aligned to budget. Please verify the compensation review inputs (performance, level, title) are accurate in Compaas.

It is very important that GitLab team-members understand their performance and how it impacts their salary.

While some GitLab team-members may not receive an increase due to already being at the right [competitive rate](/handbook/total-rewards/compensation/#competitive-rate) for their Level, Role, and Location, there are other circumstances where an increase should be avoided. If there are any reasons as to why the team member should not receive the proposed increase to be aligned with market rates in our calculator, please email total-rewards@ domain with the reasoning and cc your People Business Partner. This could be due to a current performance issue, pending termination, etc. If you would like to delay the increase, please outline a proposed plan of action and deadline. Team members who are currently struggling to perform at their current level should have that communicated clearly and the manager should consider delaying the increase until performance reaches a commendable level.

### Manager Review in Compaas

[Compaas](https://www.compa.as/) is GitLab's compensation platform where managers can login, review, change, and submit their proposed increases during Annual Compensation Review.

For any feedback about using Compaas during the FY22 Manager Review portion of Annual Compensation Review, please use the following [issue](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/240). 

**Process for slate owners:**

1. Navigate to [Compaas](https://app.compa.as/login) and select the option to sign in with Google.
* ![compaas login](/images/handbook/people-group/1_Compaas.png)
1. Select to sign in with your originally assigned GitLab email address.
* If you select to sign in with an alias, you will receive an error and not be able to sign in. You can confirm your original GitLab email address on the Personal tab of BambooHR.
* ![incorrect email](/images/handbook/people-group/2_Compaas.png)
1. If you have not used Compaas in the past, you will need to Select the Terms and Conditions box and click Submit. If a user has already visited Compaas Rewards they will automatically be taken to their slate view.
* ![terms and conditions](/images/handbook/people-group/T&C.png)
1. Once logged in you will be taken to your slate. The slate will list the team member's name, role, benchmark role, performance factor, last raise amount (USD), raise (USD) and raise percent, compensation before and after any adjustments, range with range penetration, and notes.
* ![slate owner](/images/handbook/people-group/3_Compaas.png)
* The role is located under the team member's name. This is the team member's job title including level, for example, Senior or Associate.
* Underneath the role is the [benchmark role](/handbook/total-rewards/compensation/compensation-calculator/#sf-benchmark). This is the job title that is used for determining the base compensation before other inputs such as level are evaluated. For example, the benchmark role for a Senior Backend Engineer is a Backend Engineer, Individual Contributor.
* The performance factor is uploaded into Compaas and taken from the [Performance/Potential](https://about.gitlab.com/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix) review completed in November.
* The Last Raise field outlines in USD the total amount of the last pay increase to base salary. 
* The Raise (and Raise as %) is where the manager or indirect manager can enter in the proposed increase for the team member for FY22 Compensation. Please note this is all in USD and will be converted to local currency once the cycle has closed.
* The team member's compensation before (light gray) and after (green) any adjustments is listed to the right of the percent increase.
  * If there is an "OTE" next to the compensation values, this indicates that part of cash compensation is a bonus component. Total Rewards works in OTE during Annual Compensation Review and then allocates to base/variable based on the desired split per role.
* The range penetration is the team member's compensation range for their role in their location. The bubble on the range will mark where they currently are and the arrow will show where they are being recommended to move to. This will prompt you if your raise brings the team member below or above the range for their role.
* The talk bubble at the right of the team member's card can be clicked on to leave a note on the team member's record. This can be used to record reasons for decisions, details about specific compensation change, or open questions for approvers or the People Group. The option to delete a note is also available.
* ![note](/images/handbook/people-group/6_Compaas.png)
* ![delete note](/images/handbook/people-group/7_Compaas.png)
1. Clicking the background of the person's card expands it to reveal their compensation timeline. The timeline shows any historical compensation data Compaas has available for each person. Clicking the card again closes the timeline.
* Please note this data pulls from the Compensation and Job Information table in BambooHR. Any changes in the Job Information table automatically are entered as "Promotion" in Compaas in the timeline, but may have been a manager change or other title change not related to a promotion. Pay Rate changes will show in local currency as this is how it is entered in BambooHR in the Compensation Table.
* ![timeline](/images/handbook/people-group/4_Compaas.png)
* Total Rewards has added notes to each person in Compaas to help managers assess increases based on changes to the calculator and performance ratings. There is a performance recommended increase which aligns to the [company wide recommendations](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor-and-annual-compensation-review), Location Factor increase from the previous to current fiscal year, Benchmark increase from the previous to current fiscal year, and the Total Rewards Recommended Minimum Increase. This final range is the minimum Total Rewards would advise a manager to increase a team member to ensure alignment to the compensation calculator range as well as their performance. This is not a capped value and managers can add additional discretionary budget to team members as appropriate while ensuring alignment to budget. For more information on how to use this information see the [Determining the Total Increase in Compaas](/handbook/total-rewards/compensation/compensation-review-cycle/#determining-the-total-increase-in-compaas) handbook section.
* ![Total Rewards Notes](/images/handbook/people-group/Recommended_Increase.png)
1. Your slate can be filtered by what part of the pay range your reports currently fall into: Below, Bottom, Lower-middle, Upper-middle, Upper, Above.
* ![filter range](/images/handbook/people-group/5_Compaas.png)
1. The slate can be saved as a draft at any time. When you choose to "Save and Submit", you will see a screen summarizing your proposed raises. You may choose to cancel, leaving the slate in a draft state, or "Submit and Lock" which automatically submits your recommendations for approval to the next level manager.
* ![slate owner submit](/images/handbook/people-group/Updated_Save&Submit.png)
* **Note: Even slates for 0% increases need to be submitted via Compaas so the slate does not appear as "incomplete" while it moves through the process.**
1. Once the slate has been submitted for approval, it will be locked. You will no longer be able to make changes, but while the annual compensation cycle is open you will be able to log in and review your team's compensation adjustments.
* If any changes are made by a slate approver, these will be visible in your review screen.
* ![slate owner locked](/images/handbook/people-group/Updated_Slate_Locked.png)

**Process for approvers:**

1. Approvers will log in using the same instructions as slate owners.
1. Once you have logged in, you will see your dashboard and all available slates. Your slate will have a card for each of the team members you will be recommending a raise for.
* ![dashboard](/images/handbook/people-group/Dashboard.png)
1. From the dashboard, approvers will be able to view their own slate in the "Slates" section and can follow the steps outlined above.
1. From the dashboard, the approver can click on the green “View My Approvals” button to be directed to this page. All of the slates the approver has responsibility for, including their own, will be displayed here. The approver is able to click in to all of the Slates located within the Slates tab. When a Slate changes to “Ready” Status, a green circle will appear in the Status column. This means that the approver can review, adjust, and approve the slates that roll up to them for approval. This page also shows the status of your budget (if applicable, budget is held by the executive of each group).
* ![approver review](/images/handbook/people-group/8_Compaas.png)
1. Clicking the slate name or the > arrow will take you to a slate. If you need to override a slate approval, please reach out to the Total Rewards team as only admins can submit on behalf of another account. When you view a slate on your list, you will be able to edit the slate owner's recommendations and save them. Once you have saved, please refresh the page to see the changes applied to your budget, if applicable. For more information on the details located on each team member's card, please review the process for slate owners above.
* ![approver review slate](/images/handbook/people-group/9_Compaas.png)
1. Once you are finished making edits to a slate you are an approver for, you can choose to "Save & Submit". You will be taken to a screen summarizing the proposed raises. You may choose to cancel, leaving the slate unapproved, or "Submit and Lock" which automatically submits your recommendations for approval to anyone higher up in the approval chain. It is recommended that you keep all slates in draft form until you are ready to approve all slates in your group. Once you submit you will no longer be able to make any changes.
* ![slate owner submit](/images/handbook/people-group/Updated_Save&Submit.png)
1. Approvers who have multiple slates to submit may wish to utilize the "Approve All" button to finalize all of the slates that roll into them at once. This button will become available to click once all slates are ready for your approval and is located in the upper right corner of the Slates view.
* ![bulk approve](/images/handbook/people-group/Approve_All.png)
* After the "Approve All" button is selected, a window will pop up asking you to confirm whether you would like to approve all slates. Clicking "Confirm" will submit and lock each of your slates and send them for further approval, if applicable. 
* ![confirm bulk approve](/images/handbook/people-group/Confirm_Approve_All.png)
1. After you have approved a slate, it will no longer be editable on your list of slates. After approving, you will still be able to visit the slate and view a current summary of any proposed compensation adjustments including any adjustments made by an approver higher up in the approval chain.
* ![slate owner locked](/images/handbook/people-group/Updated_Slate_Locked.png)

### Determining the Total Increase in Compaas

When you review your team member's card in Compaas, you will see the recommended % increase for [performance factor](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor-and-annual-compensation-review), the % increase in [benchmark](/handbook/total-rewards/compensation/compensation-calculator/#sf-benchmark) (if applicable), and the % increase in [location factor](/handbook/total-rewards/compensation/compensation-calculator/#location-factor) (if applicable). Also, if applicable, there will be a minimum recommended increase to ensure team members end up within their compensation band (Minimum increase by Total Rewards). All the factors should be reviewed and used to develop a holistic increase. We recommend the following steps and best practices:  

1. Managers to take a holistic view: The recommendation is not to add the percentage compensation factors together to determine the increase, rather to use them as an overall factor in developing what the increase should be.
1. Managers should use the minimum increase of Total Rewards to make sure the team member is within their band. As a manager you can review the Compensation band in the [Compensation calculator](/handbook/total-rewards/compensation/compensation-calculator/).
1. Hereafter managers should review the Performance factor ranges recommendations:
Developing in role = 0-3% increase
Performing in role = 3-7% increase
Exceeding in role = 6-10% increase
1. Is the recommended minimum increase from Total Rewards within the recommended range of the Performance factor?
  * Yes: The total recommended increase would be within the performance factor range.
E.g. The minimum increase to stay in range is 6% and the performance range is 3-7%. The PBP/Total Rewards team would recommend a 6-7% total increase.
  * No, it’s below: We would recommend a total increase within the Performance factor range.
E.g. The minimum increase to stay in range is 0% and the performance range is 3-7%. The PBP/Total Rewards team would recommend a 3-7% total increase.
  * No it’s above: We recommend a total increase which is aligned with the minimum recommended increase of Total Rewards.
E.g. The minimum increase to stay in range is 15% and the performance range is 3-7%. The PBP/Total Rewards team would recommend a 15% total increase.
1. Use placement within the compensation range and previous increases/start date to finalize the increase.
1. **Important note:** Any increase above 10% or out of band exceptions due to contract factor updates must come with additional justification entered into the notes section for planning to be complete. These justifications will be reviewed and approved by the department leader, division leader and compensation group. 
1. Also: Anyone on a formal Performance plan (PEP, PIP) will be excluded from the review. This must be communicated to Total Rewards and PBP by December 15th.

##### Examples

Person A has the following characteristics:
* Has been at GitLab for 6 months.
* Rated "Performing" during Performance Factor Review: 3-7%
* Location Factor: 5% increase
* Benchmark: 0% increase

As Person A's manager, since they haven't been here for very long, I may have been considering giving them a 3-4% increase based on their performance factor. Taking their location factor increase into account, I may increase this to 4-5%.

Person B has the following characteristics:
* Has been at GitLab over 2 years.
* Rated "Exceeding" during Performance Factor Review: 6-10%
* Location Factor: 5%
* Benchmark: 5%

As Person B's manager, Person B is one of my top performers and I want to compensate them adequately for this. Based on the increases for their location factor and benchmark and their high performance, I may recommend a 12% increase and include a justification for indirect managers to review and approve my recommendation for an increase above 10%.

### Iterations to Compaas

We have now completed two compensation reviewed utilizing Compaas. Based on some awesome feedback from our GitLab team during [Annual Compensation Review](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/80) and [Catch-up Compensation Review](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/173), we are working to have the following iterations to our process in Compaas. There are certain items that we may not be able to have implemented since any change to Compaas updates their tool for all clients. Where that is the case we will outline that it is a limitation for future reference.

The following iterations are expected to be live for FY22 Annual Compensation Review:
* Implement Proxy Access for People Business Partners. People Business Partner currently have access to a report, but the goal is to have this access expanded to admin access for their parts of the organization.
* Fields for recommended performance increase, percent change due to benchmark increases, percent change due to location factor increases.
* Approver and slate owner navigation so you don't have to toggle back and forth, but instead everything is in once place. Update: Still on separate slates, but you can see everyone by navigating to "All Employees" tab.
* Push notification so approvers know when a slate is waiting on their action.
* Bulk approvals so approvers don't have to go in and approve slates individually.
* Okta integration. Tentatively will be updated for FY22 Annual Compensation Review.

The following iterations will not be ready to ship in time for FY22 Annual Compensation Review, but we will continue to review having implemented in Compaas:
* Managers and team members to be able to log in and see where they fall in compensation for Compaas Analytics
* Ability to review who has/has not logged in with which ID and which permissions
* Proxy approval where People Business Partners can approve on behalf of their part of the organization.
* Mobile responsive

### Communicating Compensation Increases

All increases for Annual Compensation Review will be finalized by Feb 1st. FY 2022 compensation will be uploaded into BambooHR no later than Feb 4th for payroll to make changes in all subsequent systems. The Total Rewards team will turn off the ability to see compensation in BambooHR using Employee or Contractor Self Service from Jan 25th until Feb 12th for Employees and until Feb 5th for Contractors. Managers will have the ability to communicate any increases to their team before end of day on the 5th for Contractors and end of day on the 12th for Employees.

For FY22, each team member with a compensation change will have a [Total Rewards Statment](/handbook/total-rewards/compensation/compensation-review-cycle/#total-rewards-statement) generated. Managers are able to utilize this letter when having conversations with your team members about their increase and overall Total Rewards Compensation package at GitLab. More information on these letters can be found in the handbook section below. 

Communication Guidelines can be found in the [Leadership Toolkit](/handbook/leadership/compensation-review-conversations/).

If your direct report has any questions on the calculation of their increase please feel free to have them reach out to the Total Rewards team.

#### Total Rewards Statement

Each team member who receives an increase during the Annual Compensation Review cycle will receive a Total Rewards Statement detailing their Total Rewards package (cash, equity, benefits). This letter will be available for managers to review in BambooHR no later than Feb 4th. Team members who are Contractors will be able to review their letter on Feb 6th and team members who are Employees will be able to review their letter on Feb 13th. Managers should share the letter in BambooHR once they have reviewed this with the team member. Please reach out to Total Rewards if the letter hasn't been shared by Feb 6th (Contractors) or Feb 13th (Employees) and we'll share this with you. Instructions for downloading and reviewing the letter are as follows:
1. Log into [BambooHR](https://gitlab.bamboohr.com/).
1. If you are a manager, navigate to the profile of one of your direct reports first. Select the Documents tab and expand the `Annual Compensation Review Total Rewards Statements` folder. You can either select the linked document name to open a quick view of the Total Rewards Statement in BambooHR or select the download button that appears to the right of the letter. If you are a manager and are unable to see the Documents tab for your team member, please reach out to `total-rewards@ gitlab` and we will review your access level.
   * ![download change letter](/images/handbook/people-group/BHR_ACRLetter_Folder.png)
1. There are two different letter formats depending on whether you/your team member is paid a base salary only or receives a commission/bonus:
   * [Base + Bonus Letter Example](https://drive.google.com/file/d/141f-z4ID3kNHsPbRkBv5H38t9Ffy3lUr/view?usp=sharing)
   * [Base Only Letter Example](https://drive.google.com/file/d/1_ytqLF6MlpNWLTorSzXJRoaKiT5aBUoI/view?usp=sharing)
1. Each letter will have a Cash Compensation Section outlining the current (FY21) compensation and new (FY22) compensation in the team member's local currency.
   * The Base Only letter will list the current and new Base Salary as well as the percent increase (Total Target Cash Compensation % Change).
   * The Base + Bonus letter will list the current and new Base Salary, the current and new Target Bonus (Variable), the current and new Total Target Cash Compensation (Base + Bonus), and the Total Target Cash Compensation % Change. 
1. Both letters will also have a section for Equity Compensation outlining the number of unvested stock options as of January 27, 2021. The letter will also include a link to the [Compensation Calculator](https://comp-calculator.gitlab.net/) for the manager and team member to review the potential value of the unvested stock options. 
   * Please note that the current number of unvested stock options was pulled on January 27th. The total number of unvested options may have changed between this date and the time the letter is reviewed if more stock options were vested during this time period. Please see the handbook for more information on [vesting at GitLab](https://about.gitlab.com/handbook/stock-options/#vesting).
1. Both letters will also have a section for Benefits linking to the [Compensation Calculator](https://comp-calculator.gitlab.net/) for the team member to review the value of their benefits package. The compensation calculator outlines the following benefits: Remote Benefits, General Benefits, and Entity-Specific benefits.
1. To share the letter directly with the team member once their increase has been communicated, select the gear button to the right of the letter, select "Share with Employee", and, in the pop up prompt, select "Share" again.
   *  ![share letter](/images/handbook/people-group/Share_Letter.png)

## FY 2022 Annual Compensation Review Budget

For Fiscal Year 2022, the promotion and annual compensation review budget is separate and calculated based on the following conditions:

1. Promotion Budget
  * Assume 12% of the team is promoted with an average of a 10% increase to OTE.
1. Annual Compensation Review Budget
  * 6% of those who were not promoted. Therefore 6% of 88% of the population.  
  * The annual compensation review budget is held by each e-group leader. The leader can allocate budget however appropriate within their division, but cannot move funds to another division.
  * The budget for [grade 12](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) and above is managed by the Compensation Group internally to GitLab, and approved by the Compensation Committee at the Board level.
1. Relocation Budget
  * Relocations are not considered in the budget. Theoretically throughout the year, there would be a net zero from those moving to higher-cost and lower-cost regions.
1. Budget Rollovers: The budget from annual compensation review can rollover into the catch up compensation review or the promotion budget for that fiscal year. For example, if Engineering has 20,000 USD left after the FY22 annual compensation review process, they can use these funds for promotions in FY22. At the end of each fiscal year, the budgets will reset.
1. Budgets _cannot_ be transferred to a different division.

**Calculating Budget Actuals**
1. Promotion Budget
   * The Total Rewards team will calculate what the budget is for each Division to be managed by the executive of the group.
   * Take total OTE (TTC) as of October 31st of the current fiscal year x 12% x 10%. To get to each quarterly budget divide the annual amount by four so promotions budgets are the same throughout the year. We want to ensure predictability in financial models, so while the budget is set at the beginning of each fiscal year, we will also analyze spend per quarter.
   * Any additional increases over budget must be approved by the CPO and CFO and may be taken from the following quarter's pool.
   * Considerations when reviewing increases in BambooHR:
     * Any promotion with a percent increase of more than 10% to base salary must be approved by the CPO
     * Any promotion where the employee has had less than one year of tenure in the current level must be approved by the CPO
     * Any promotion to Director and above must be approved by the Compensation Group
   *  The Total Rewards team will track and report on budgets throughout the quarter while the data team works to automate this in Sisense. 
1. Annual Compensation Review Budget
  * Per Division take the total OTE (TTC) of the group as of October 31st x 88%. Review removing any overspend or allocate additional budget per division from the promotion budget.
  * Verify totals with the Manager, FP&A.

**Administering the Promotion Budget Quarterly**
1. The data team is working to make the promotion budgets available in Sisense.

## Targeted Mid-year Increase Review

Team members hired between November 1st and January 31st or team members who did not receive an increase during the most recent annual compensation review due to a recent promotion, compensation adjustment, or for performance may not have their compensation evaluated for more than a year. In order for GitLab to help Managers to be better equipped to retain team members and reward high performers who were not increased during the previous annual compensation review, Managers will be able to recommend team members for targeted mid-year increases.  

During the [annual compensation review](#annual-compensation-review), the budget for these team members is separated out to be used in August. If anyone would fall out of the compensation range, the team member would be adjusted immediately, but this would be deducted from the budget used in August.

This review will only take into account a performance alignment (if any). Team members should not expect an increase, but instead understand that their compensation is being reviewed to ensure alignment to market and performance.

### Manager Process

This section will be updated with more detailed steps in FY22.

In August, Managers can log into BambooHR and add a compensation adjustment for any eligible team member they would like to propose for an increase. Managers should complete and link the [Mid-year Increase Template](https://docs.google.com/document/d/1gNoCHIhRr0GKRL0L7Pq9UepJDrtIrezaAs1XfgLlZKA/edit?usp=sharing) in the comments of the request. All approved increases will be effective on September 1.

### Targeted Mid-year Increase Review Timeline

To be updated for FY22.
