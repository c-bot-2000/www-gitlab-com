---
layout: handbook-page-toc
title: GitLab Marketing Team READMEs
description: "Learn more about working with various members of the marketing team"
---
## Marketing Team READMEs

- [Darren M.'s README (Head of Remote)](/handbook/marketing/readmes/dmurph/)
- [William Chia's README (Sr. PMM)](/handbook/marketing/readmes/william-chia.html)
- [Parker Ennis README (Sr. PMM)](/handbook/marketing/readmes/parker-ennis.html)
- [Wil Spillane's README  (Head of Social)](https://about.gitlab.com/handbook/marketing/readmes/wspillane.html)