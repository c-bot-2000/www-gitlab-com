---
layout: handbook-page-toc
title: "Evangelist Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

At GitLab our mission is to change all creative work from read-only to read-write so that **everyone can contribute**. In order to fulfill this mission, we need to create both the tools and platform to enable this change and a community of contributors who share our mission. We are just getting started in building the GitLab community and we encourage everyone to contribute to its growth.

There are many ways to participate in the GitLab community today: [contributing to an open source project](/handbook/marketing/community-relations/code-contributor-program/), [contributing to our documentation](https://docs.gitlab.com/ee/development/documentation/), [hosting your open source project on GitLab](/solutions/open-source/), or teaching your colleagues and collaborators about the value of [Concurrent DevOps](/concurrent-devops/).

We are building an evangelist program to support people who share our mission and want to give tech talks, run local meetups, or create videos or blogs. We will be announcing more in Q1. For now, please email `evangelists@gitlab.com` if you have feedback on our vision, ideas for how we can build our community, or suggestions for a name for our evangelist program.

## How to see what we're working on

We use the `Evangelist Program`, `Heroes`, and `Meetups` labels to track issues. The [Evangelist Program issue board](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/951386?&label_name[]=Evangelist%20Program) provides an overview of these issues and their status.

Evangelist Program issues typically exist in the `Evangelist Program` subgroup but they can also exist in Field Marketing, Corporate Marketing, or other marketing subgroups. Our upcoming Meetups are tagged with the `Meetups` label and listed on the [Meetups board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/962542?&label_name%5B%5D=Meetups).

We utilize a process of having a epic for current OKRs and using labels and boards to track ongoing work related to our KPIs (for example: Meetups). As our OKRs and KPIs change, this page will be updated so you can see what we are working on and track our progress.

### Evangelist Program OKRs and KPIs

We are actively looking at ways to improve the data collection for our Evangelist Programs. If you have ideas for how we can improve, please reach out to [evangelists@gitlab.com](mailto:evangelists@gitlab.com).

#### [FY21-Q3 OKR: Scaling GitLab Heroes Impact](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/31)

- <span class="colour" style="color:rgb(0, 0, 0)">**Objective**: Increase and measure contributions from the GitLab Heroes</span>
    - <span class="colour" style="color:rgb(0, 0, 0)">**Key Result:** 30 MRs of recent contributions merged to the Recent Contributions page</span>
    - <span class="colour" style="color:rgb(0, 0, 0)"></span><span class="colour" style="color:rgb(0, 0, 0)">**Key Result:** Add KPI for Heroes contributions to</span>[<span class="colour" style="color:rgb(0, 0, 0)"> </span><span class="colour" style="color:rgb(17, 85, 204)"><u>GitLab Marketing KPI list</u></span>](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/#marketing-kpis)

#### Evangelist Program KPIs

Key Performance Indicators for the Evangelist Program can be found in the [Marketing section](/handbook/business-ops/data-team/kpi-index/index.html#marketing-kpis) of GitLab's KPI index.

#### How to update KPIs for the Marketing Key Meeting

1. Find the link to the current month's deck in the [#keymonthlymarketingmetrics](https://gitlab.slack.com/archives/CM5UC5XPT) Slack channel. It is typically shared on the second Monday of the month.
1. Navigate to the slide containing the Meetups per month chart (slides 6-8 typically contain the Community Relations metrics).
1. Open the [chart](https://app.periscopedata.com/app/gitlab/431555/Marketing-Metrics?widget=7799761&udv=1021136) in Sisense.
1. Hover over the top right corner to reveal the hamburger menu, open the hamburger menu, and click `Download Image`.
1. Replace the existing `Meetups per month` chart in the Marketing Key Meeting deck with the newly downloaded image.

### Upcoming events

The [Community Events calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_90t5ue1q8kbjoq5b0r91nu7rvc%40group.calendar.google.com&ctz=America%2FNew_York) includes a list of our upcoming meetups, hackathons, office hours, and other community events. After opening the calendar, you can click on the `+` in the bottom right corner of the browser window to add `Community Events` to your list of calendars in Google Calendar.

## Meetups

### COVID-19 update

**IMPORTANT: Due to the COVID-19 pandemic, GitLab is not currently supporting in-person meetups in order to encourage responsible physical distancing within our community. We encourage organizers who wish to continue to bring their communities together to use remote meeting platforms like Zoom and Google Hangouts.**

GitLab supports team members and members of the wider GitLab community who want to organize or speak at meetups. Our goal in supporting these events to better engage with and increase connections among the GitLab community, increase awareness of GitLab, and better educate the technology community.

### Organize a meetup

- We love and support meetups. If you participate in local tech groups and are interested in having a GitLab speaker or GitLab as a sponsor, please submit an [issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=sponsorship-request). Please note, providing sufficient lead time (at least a month) allows us to better support your event so feel free to reach out as soon as possible.
- When a meetup issue is submitted by a community member, the issue should be assigned to the Evangelist Program manager, labeled with the [`Meetups`](https://gitlab.com/groups/gitlab-com/marketing/-/issues?label_name%5B%5D=Meetups) label, and be given a due date on the date of the event. Meetup issues created by a GitLab team member will have those attributes added via the template.
- If you are interested in creating your own GitLab meetup or if you already have an existing meetup on meetup.com that you would like to link to GitLab's meetup.com Pro account, please email `evangelists@gitlab.com`. You can find the list of GitLab meetups on the [meetup.com page](https://www.meetup.com/pro/gitlab).
    - Connecting your group to GitLab's Meetup Pro instance will also allow you to utilize easy-to-use, custom templates for your Meetup events. When scheduling a new event on Meetup, you can choose a template by clicking on `Start from a template` in the right sidebar and choosing a template that best fits your event.
- When you are getting started, we recommend scheduling at least 2 meetups. Publish your first meeting with a date and topic, and then a second meeting with a date and topic. The second meeting can have a flexible topic based on how the first meeting goes. The point of setting two meet-ups is to help build momentum in the new group.
- Often, we try to have GitLab employees attend the first couple of meetups. Once the group has hosted a couple of successful events, it becomes easier for the group to sustain itself. It is much harder to start new meetups versus maintaining existing ones. So we make an effort to support and keep new meetups going.
- Additional information on organizing your own meetup events can be found on our [Meetup checklist](/community/meetups/checklist/).

#### Meetup Promotion

GitLab provides support to meetup organizers to drive attendance at meetup events. In order to help support our organizers, the Evangelist Program Manager takes the following steps when an event is scheduled:

- Add meetup event to the [GitLab Events](/events/) page on our website. GitLab team members can follow [these steps to add an event to the events page](/handbook/marketing/corporate-marketing/#how-to-add-events-to-the-aboutgitlabcomevents-page).
- Add meetup event to the [Community Events calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_90t5ue1q8kbjoq5b0r91nu7rvc%40group.calendar.google.com&ctz=America%2FNew_York).
- The Evangelist Program Manager should add to the issue description the SDR and SAL assigned to an event's region to ensure they are informed about the event. The SDR and SAL for a region can be found on the [Territories](/handbook/sales/territories/#territories) page of the handbook. The SDR and SAL for the event should then communicate the event to nearby accounts to help increase attendance for the meetup organizer.

Organizers are encouraged to take the following steps to drive attendance to their events:

- Use your personal social media to promote the event within your network and invite your members to do the same.
- Post about the event, including a link to the registration page, in the [News section](https://forum.gitlab.com/c/announcements/5) on forum.gitlab.com.
- Send at least two reminders in the weeks and days leading up to your meetup reminding people to RSVP.
- Reach out to other related meetups (ex: DevOps, Cloud, Kubernetes) in your area and invite them to your event to help grow your community.

#### Meetup Expenses

**IMPORTANT: Due to the COVID-19 pandemic, GitLab is not currently supporting in-person meetups in order to encourage responsible physical distancing within our community. In order to reinforce this policy, we are suspending reimbursement for meetup expenses for in-person events at this time. We encourage organizers who wish to continue to bring their communities together to use remote meeting platforms like Zoom and Google Hangouts.**

<details>
<summary markdown="span">This initiative is temporarily suspended due to COVID-19</summary>

- ~~GitLab can help cover the cost of food & beverage (and other expenses required for a successful event) for the meetup event. A general guideline is $US 5/person for a maximum of $US 500 per each meetup.~~
- In order to ensure we can support meetup groups around the globe and to encourage our organizers to invest in producing high-quality events for our community, GitLab will only provide financial support and swag for two meetup events per month for each meetup group.
- When making purchases for a meetup, please follow GitLab's company guideline to ["spend company money like it is your own money"](/handbook/spending-company-money/). Keeping the cost of individual meetups low is important as it will allow us to support a larger number of groups and events.
- GitLab uses Tipalti to process invoices for meetups. To be reimbursed for your expenses, please send an email to [evangelists@gitlab.com](mailto:evangelists@gitlab.com) and [ap@gitlab.com](mailto:ap@gitlab.com) to request access to Tipalti.
- Once you have access to the Tipalti portal, you will upload your invoice and receipts as a single document on the Tipalti portal or email the invoice and receipts as a single document to [gitlab@supplierinvoices.com](mailto:gitlab@supplierinvoices.com). We suggest organizers use our [Meetup Invoice template](https://docs.google.com/spreadsheets/d/1D3lpPrwfz1zQp8LsiWUq64aBNYHnlKbhjyPf6sQ8NsM/edit?usp=sharing).
- To create a single document containing your invoice and expenses, please follow these steps:
    - Make a copy of the [Meetup Invoice template](https://docs.google.com/spreadsheets/d/1D3lpPrwfz1zQp8LsiWUq64aBNYHnlKbhjyPf6sQ8NsM/edit?usp=sharing).
    - Enter all your expenses into the template.
    - Download the sheet as a PDF.
    - Scan your receipts and add them to the PDF ([instructions for Mac](https://support.apple.com/en-us/HT202945)).
    - Upload the single document containing the receipts and invoice to Tipalti or send it to [gitlab@supplierinvoices.com](mailto:gitlab@supplierinvoices.com).
- Submission of an invoice will trigger a workflow that generates an email to the Evangelist Program Manager. Upon receipt of that email, will review the bill and take appropriate action:
    - If the report is complete and the expense is approved, the Evanglist Program Manager should click the `Approve bill` button in the mail.
    - If the bill's `Class` does not include the appropriate finance tag (for example: `Q4_FY2020_Meetups`) or there are other issues with the bill, the Evangelist Program Manager should click `Send back to AP`.
    - If the bill is not legitimate, the Evangelist Program should click `Dispute bill`.

For GitLab Team Members using Expensify for meetup-related expenses:

- If an expense is submitted via Expensify, the campaign finance tag (for example: `Q4_FY2020_Meetups`) should be selected as a `Classification` when creating the expense.

</details>

#### Meetup Swag

GitLab Meetup Starter Kits containing t-shirts, stickers, and info cards are available in Printfection. Please note we need at least 4-6 weeks to ship any meetup swag and that all shipping is significantly delayed due to COVID restrictions. 

For GitLab team members, please follow these steps to provide meetup organizers with a link to order a GitLab Meetup Starter Kit:

- Navigate to the [GitLab Meetup Starter Kit](https://app.printfection.com/account/campaign/overview.php?storeid=289967) campaign in Printfection.
- Note: To access the page, you will need a Printfection login. Don't have one? Please email merch@gitlab.com to request access and not this is for sending Heroes swag.  
- On the page, click the green button labeled `+ GET NEW LINK`.
- Copy the link generated in the popup window and share it with the Meetup organizer. Then click the button labeled `MARK LINK AS SENT` inside the popup.

For Meetup organizers:

- If you require swag for a meetup and have not received a link, please comment on the issue for your meetup and request a link.

### Speak at a meetup

Meetups help us raise awareness of GitLab and build communities in new places. We love to track them to know where the community is growing. If you are speaking at a meetup as a representative of GitLab or you are giving a talk about GitLab, please let us know! Here's how and why we do this:

- Speakers should use the [Meetup Speaker template](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetup-speaker) to create a new issue with the details for your meetup talk.
- Opening an issue helps us support you. A few ways we can help:
    - Marketing: We'll share the event on our blog, via social, and with GitLab community members in the area to raise awareness of the event.
    - Sponsorship: GitLab will cover the cost of food and beverages for meetup organizers.
    - Swag: Our swag is pretty popular so we'll send you plenty of stickers and _maybe_ some other fun stuff to give away.
    - Speaker prep: If you need help with your deck or would like someone to offer feedback on a dry run of your talk, the [Evangelist Program Manager](mailto:evangelists@gitlab.com) is happy to help.
- We track meetups via issues on the [Meetups issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/962542?label_name%5B%5D=Meetups). Take a look and see what we have coming up!

### GitLab Virtual Meetup

- GitLab's Evangelist Program Manager organizes the [GitLab Virtual Meetup](https://www.meetup.com/gitlab-virtual-meetups/) group. This group brings together our global community for virtual meetup events.
- GitLab virtual meetups typically feature one speaker followed by randomized breakout groups using the Zoom breakout room feature. The normal duration of these events is one hour in total.
- For GitLab Virtual Meetups, the Evangelist Program Manager will upload the recordings to GitLab Unfiltered.
- If you would like to present at a GitLab virtual meetup, please create an issue using the [Meetup Speaker](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/-/issues/new?issuable_template=meetup-speaker) template.
- Additional information on organizing your own virtual meetup events can be found on our [Meetup checklist](/community/meetups/checklist/#planning-a-virtual-meetup).

## GitLab Heroes

[GitLab Heroes](/community/heroes/) engages, supports, connects, and recognizes members of the wider GitLab community who make outstanding contributions to GitLab and our community around the globe. Examples of activities that may make a member of the wider GitLab community eligible for the Heroes program include:

- Organizing meetups
- Recording demos for YouTube
- Giving talks at conferences and events
- Writing technical blog posts
- Contributing to our open source project

Heroes are eligible for rewards to help enable and encourage contributions. These rewards include:

- Invitations to special events including GitLab Commit
- Support for travel to speak about GitLab at events
- GitLab Ultimate licenses
- Special Heroes swag so people know how awesome you are
- Access to GitLab's product, engineering, and developer evangelism teams to help with reviews of talks and blog posts

We have three levels of Heroes: Contributor, Hero, Superhero. A community member's contributions will determine at which level they enter the program and the benefits for Heroes increase as they progress through the levels. More detail can be found in the [Hero's Journey](/community/heroes/#heroes-journey) section of the Hero's page.

Community members who are interested in applying for the Heroes program should apply through the [application form](/community/heroes/#apply) on the Heroes page.

The Heroes program is managed by the Evangelist Program Manager with support from the GitLab's Developer Evangelism team. The Evangelist Program Manager leads the review of applications along with the Developer Evangelism team. The Evangelist Program Manager also serves as the main point of contact with the Heroes community, manages Heroes related marketing pages and events, and is responsible for the adminstration of the program including metrics and KPI tracking.

Select Heroes may be asked to join the GitLab Developer Evangelism Community. At that point, those community members will be supported by and engage with the Developer Evangelism team.

Please email us at [evangelists@gitlab.com](mailto:evangelists@gitlab.com) if you have questions about the GitLab Heroes program.

### GitLab Heroes Application Process

We use GitLab to manage our Heroes application process. To apply for GitLab Heroes, community members must follow these steps:

1. Click the "Become a Hero" button or the "Apply to become a GitLab Hero" button on the [GitLab Heroes page](https://about.gitlab.com/community/heroes/#apply)
1. Clicking the button will open an email message, in which, the applicant will be prompted to send an email which creates an issue in the [`GitLab Heroes Application`](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/gitlab-heroes-applications) project using GitLab's [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html#overview) feature. _Note: only GitLab team members can view this project as the issues may contain applicants' email addresses._ 
1. Using Service Desk, an auto-reply will send an application template to the applicant.
1. Once the applicant replies with their completed the application, the Evangelist Program Manager should apply the `heroes-application` template to their Service Desk issue.

After completing the above steps, the application is ready for review. The review committee consists of the Evangelist Program Manager as the lead with the Code Contributor Program Manager supporting. The composition of the review committtee is subject to change. Any changes will be reflected here.

Generally, applicants will receive a response from GitLab within two weeks of submitting an application. Following the review process, the Evangelist Program Manager will inform folks who are selected to be GitLab Heroes of your status including the level (`Contributor`, `Hero`, `Superhero`) at which time you will enter the program. If you have any questions about the status of your application, please reach out to [evangelists@gitlab.com](mailto:evangelists@gitlab.com) for support.

We conduct semi-annual reviews of the current GitLab Heroes members to ensure they are remaining active in the community in order to maintain their GitLab Heroes status. When a current GitLab Hero is inactive, the Evangelist Program Manager will contact them to identify the reasons and work with them to resolve any blockers or issues. When a GitLab Hero no longer plans to remain active in the GitLab community, the Evangelist Program Manager will remove them from the active membership of the program.

### Adding yourself to the Heroes page

Upon acceptance, Heroes are asked to submit a Merge Request to add themselves to the [GitLab Heroes members page](/community/heroes/members).

To add yourself to the Heroes page, you will need:

- Your personal Twitter / GitLab handles
- A picture of yourself for the Heroes Members page

> **Picture Requirements**
> 
> 
> - Crop image to a perfect square.
> - Keep maximum dimension under 400 by 400 pixels.
> - Use the JPEG (`.jpg`) or PNG (`.png`) format.
> - Test image in color and black-and-white.
> - Name file `yournameinlowercase` and add the appropriate file extension.

Once you have the above items, follow these steps to add yourself to the Heroes page:

1. Go to the [Heroes file in the GitLab.com / www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/heroes.yml) project.
1. On the file page, click on the button labeled `Web IDE` near the middle of the page.
1. When prompted, click `Fork` to create a Fork of the repo which will allow you to make changes and submit a Merge Request.
1. You should see the `heroes.yml` file open in your browser once the fork has been created. Add the following fields to the end of the file and enter your information into each of the blank fields:

``` yaml
- type: person
  name:
  locality:
  country:
  role: GitLab Hero
  twitter:
  bio:
  gitlab:
  picture:
```

1. After updating `heroes.yml`, use the file browser on the left side of the screen to navigate to `source/images/heroes`.
1. Click the `⋁` icon next to the `heroes` directory, select upload file, and upload the photo of yourself. Be sure to follow the picture requirements listed above and confirm that the file name matches your `picture` entry in `heroes.yml`.
1. Once you have finished this, click the `Commit` button in the bottom left. It should say something like `2 unstaged and 0 staged changes`. This will bring up a sidebar with an `Unstaged` and `Staged` area.
1. Check the files to ensure your updates are what you expect. If they are, click the check mark next to the filename to "stage" these changes.
1. Once you have verified all of the edits, enter a short commit message including what you've changed. Choose `Create a new branch`. Name the branch in the format of `YOURINITIALS-heroes-page` or similar. Tick the `Start a new merge request` checkbox. Then click `Commit` once more.
1. Click on the Activity link in the header to go to your Activity page. Once there, click on the blue `Create merge request` button at the top of the page.
1. Fill out the merge request details. Please ensure you tick the box to `Allow commits from members who can merge to target branch` as detailed on the [Allow collaboration on merge requests across forks](https://docs.gitlab.com/ee/user/project/merge_requests/allow_collaboration.html#enabling-commit-edits-from-upstream-members) page in our docs.
1. Mention `@johncoghlan` in a comment in the merge request so our team can review and merge.

### Adding members to the GitLab Heroes project

Upon accepting new members to the GitLab Heroes program, newly accepted members should be invited to the [GitLab Heroes project](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes) by the Evangelist Program manager.

To add a new member to the project:

1. Navigate to [GitLab Heroes project](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes)
1. Go to `Settings > Members` in the left sidebar.
1. On the `Invite Member`, input all new members using their GitLab username or email address associated with their application.
1. Set `Choose a role permission` to `Developer`.
1. Set `Access expiration date` one year from the date of acceptance.

### Communication with Heroes

Communication with Heroes applicants and members of the GitLab Heroes program is conducted through email updates, generally from the Evangelist program, and via the [GitLab Heroes project](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes). Please communicate using MRs and Issues in the GitLab Heroes project whenever possible to provide transparency to the wider GitLab community and allow for easier collaboration.

When communicating with GitLab Heroes, you may wish to use the **GitLab Heroes Email Templates doc** in Google Drive for acceptances, declines, follow-ups for clarification on contributions, and regular program updates. Access to these templates is limited to GitLab employees due to the sensitivity of some of the content in the templates (such as codes for Heroes swag).

#### Requests for Heroes

Please follow the process detailed in the GitLab Heroes project to [request support from Heroes](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes#requesting-support-from-heroes).

### GitLab Heroes Licenses

GitLab team members can issue a Ultimate license to GitLab Heroes by following the steps below. These licenses are only open to GitLab Heroes at the Hero or Superhero level. Licenses should be issued for 6 months and can be extended as long as the GitLab Hero maintains their Hero or Superhero status in the program.

#### Self-Managed

1. If you need access to license.gitlab.com, you will need to get permission to login to dev.gitlab.org by creating an [access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues).
1. Log in to [license.gitlab.com](https://license.gitlab.com/) with your GitLab credentials.
1. Choose the manual entry option.
1. Select the Ultimate license (unless requested otherwise).
1. Set the term for 6 months.
1. Set the appropriate number of seats to 1.
1. Indicate that the license is for a GitLab Hero and that use of the license is limited to personal use only in the free form text box at the bottom of the page.

#### SaaS

1. Request that the GitLab Hero sign-up for a free trial on GitLab.com via our [trial page](/free-trial/).

1. [Create an issue](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/new?issuable_template=GitLab.com%20Trial%20Extension) under the [internal-requests project](https://gitlab.com/gitlab-com/support/internal-requests) and choose the `GitLab.com Trial Extension` template.
1. Fill in the relevant information including an end date six months after the start date and that plan should be upgraded to Ultimate.
1. Assign to the Director, Community Relations for approval. Include a brief note about the motivation for this change.
1. Once approved by Director, Community Relations, a member of the Support Engineering team will pick up the issue and complete the request.

### GitLab Heroes Swag

GitLab Heroes swag is available only for GitLab Heroes.

Gitlab Heroes swag fulfillment is managed by Printfection.

GitLab team members who wish to provide Heroes with swag, which is typically done upon acceptance to the program, please follow this process:

- Identify the level (Contributor, Hero, or Superhero) for the GitLab Hero to whom you will be sending swag.
- Navigate to the [Community: GitLab Heroes Contributor Swag](https://app.printfection.com/account/campaign/overview.php?storeid=290667) or [Community: GitLab Hero & Superhero Swag](https://app.printfection.com/account/campaign/overview.php?storeid=290668) campaign in Printfection.
- Note: To access the page, you will need to be logged in to Printfection using the credentials in 1Password.
- On the page, click the green button labeled `+ GET NEW LINK`.
- Copy the link generated in the popup window and share it with the GitLab Hero. Then click the button labeled `MARK LINK AS SENT` inside the popup.

To replish Heroes swag, please follow the necessary steps in the [Merchandise workflow](/handbook/marketing/corporate-marketing/merchandise-handling/#ordering-new-swag) pages of the Handbook.

### GitLab Heroes Project

The [GitLab Heroes project](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes) is used for discussion via Issues, as a repo for materials that GitLab Heroes create, and a home for other resources for members of the program. Everyone in the program will have Developer access. GitLab Heroes and our community are encouraged to contribute to the project.

## Community events

We'd love to support you if you are organizing or speaking at a community-driven event, be it GitLab-centric or around a topic where GitLab content is relevant (e.g. DevOps meetup). Depending on the number and type of attendees at an event, it may be owned by [Corporate Marketing](/handbook/marketing/#new-ideas-for-marketing-campaigns-or-events), [Field Marketing](/handbook/marketing/revenue-marketing/field-marketing/), or Community Relations. Our events decision tree is a guide to help you find the right team to handle an event request.

Events Decision Tree:
![event decision tree](/images/handbook/marketing/event-decision-tree.png)

### Submit an event request to our team

To submit a community event for support or sponsorship:

1. Review our events decision tree to ensure you are directing your event inquiry to the appropriate team.
1. Submit an issue using the [sponsorship-request](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=sponsorship-request) template.
1. For Service Desk or other auto-generated issues that contain sponsorship requests, we will retroactively apply the 'sponsorship-request' to the issue. The process for updating an issue with no template to the 'sponsorship-request' template is: copy text from original issue, assign 'sponsorship-request' template to issue, paste text from original issue into the appropriate field at bottom of template, update remaining fields.
1. GitLab XDRs: for _contact requests_ received in Salesforce via the [Sales webform](/sales/) requesting event sponsorship, please change ownership to `GitLab Evangelist` in SFDC & be sure to "check the box" to send a notification.
1. GitLab's Evangelist Program Manager will review the request and follow up with the event contact.

### How we assess requests

We ask the following questions when assessing an event:

- Will there be a GitLab speaker? We do not require a speaker slot in return for sponsorship but we do prioritize events where the audience will be hearing about GitLab - either from a GitLab team member or a member of the wider GitLab community.
- What type of people will be attending the event? We prioritize events attended by diverse groups of developers with an interest in DevSecOps, Cloud Native, Kubernetes, Serverless, Multi-cloud, CI/CD, Open Source, and other related topics.
- Will we be able to interact with attendees? We prioritize events that provide opportunities for meetings, workshops, booth or stands to help people find us, and other interaction with attendees.
- Where will the event be held? We aim to have a presence at events around the globe with a particular focus on areas with large GitLab communities and large populations of developers.
- Is the event important for industry and/or open source visibility? We prioritize events that influence trends and attract leaders within the developer and open source communities. We also prioritize events organized by our strategic partners.
- What is the size of the opportunity for the event? We prioritize events based on audience size, the number of interactions we have with attendees, and potential for future contributions to GitLab from attendees.

Each question is graded on a scale of 0-2. We then tally the scores and assign the event to a sponsorship tier.

- Events scoring below 5 are not eligible for sponsorship.
- Events scoring 6-8 are eligible for GitLab swag for attendees and other in-kind sponsorship.
- Events scoring 9 or above are eligible for financial support.

We ask these questions and use this scorecard to ensure that we're prioritizing GitLab's and our community's best interests when we sponsor events.

If you have questions, you can always reach us by sending an e-mail to `evangelists@gitlab.com`.

#### Student-run hackathons

Student hackathons are the events most frequently submitting requests for support from GitLab. Hackathon organizers who wish to leverage GitLab's DevOps platform for their events are encouraged to use free trials for their events. This can be done by directing hackathon participants to apply for a [free trial of GitLab](/free-trial/) for use during the hackathon which will allow them to use all of GitLab's features. In some cases, if your event meets [the criteria](/handbook/marketing/community-relations/evangelist-program/#how-we-assess-requests) above at a score of 9 or higher, we may also send stickers for participants or swags as prizes. Given the volume of requests we receive, providing financial support for these events is not feasible.

## Find a tech speaker

We'd love to support you if you are organizing an event, be it GitLab-centric or around a topic where GitLab content is relevant (e.g. DevOps meetup, hackathon, etc.). There are a few ways you can get in touch with speakers from the GitLab team and the wider community to participate and do a talk at your event:

- Review our list of [GitLab Heroes members](/community/heroes/members/) to see if there is a GitLab Hero near you. If you'd like to request support from a GitLab Hero, please follow the process detailed in the GitLab Heroes project to [request support from Heroes](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes#requesting-support-from-heroes).
- Visit our list of active speakers on our [Speakers Bureau](/speakers/) page. Once you find a speaker in your region, contact them directly.
- If you are unable to find a speaker in your region, you can complete our [speaker request form](https://docs.google.com/forms/d/e/1FAIpQLSc6jQWbh-63myQu7EBuZZ0KY2J_EKSAZPH6OP2TURNBmfMjtg/viewform).

For GitLab team members, you can check the #cfp channel on Slack where many of our active tech speakers will see your speaker request. Most speakers will also be able to do talks remotely if the event is virtual or if travel is a challenge.

If you have questions, you can always reach us by sending an e-mail to `evangelists@gitlab.com`.

## Community nominations

GitLab team members and members of the wider GitLab community are encouraged to nominate community members to participate in our programs. This is a valuable way to contribute to the growth and sucess of the GitLab community.

To nominate a community member who may be interested in becoming a code contributor or meetup organizer, who actively shares their knowledge of GitLab in talks or blog posts, or who meets [the criteria for the GitLab Heroes program](/community/heroes/#heros-journey), please contact the Community Relations team by emailing evangelists@gitlab.com.

## Community content

GitLab actively supports content contributors. Our community team tracks GitLab content and our evangelist program manager and editorial team regularly reviews the content. If you would like to submit your content for review, please create an [issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=community-content) on our evangelist program project.

We make an effort to amplify and support content contributions that generate value for our community. Criteria we consider include: how well a post addresses an issue in the Community Writers issue tracker, how well a post aligns with our strategy and values, and how well a post is written.

### Media Releases

The Evangelist Program Manager must collect a signed `Media Consent and Release Form` before their content is published by GitLab. GitLab team members can request a copy of the form to send to members of the wider GitLab community via the [#legal](https://gitlab.slack.com/archives/C78E74A6L) channel in Slack.

## Evangelist Program Office Hours

Our Evangelist Program Manager hosts office hours via Zoom every Friday at 10:00am ET excluding holidays. They want to answer your meetup, events, and public speaking questions and hear your feedback on our programs! You can see the meeting information and join the call via the [Community Events](https://calendar.google.com/calendar/embed?src=gitlab.com_90t5ue1q8kbjoq5b0r91nu7rvc%40group.calendar.google.com&ctz=America%2FNew_York) calendar.

## Helpful Resources

- [Community Events Calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_90t5ue1q8kbjoq5b0r91nu7rvc%40group.calendar.google.com&ctz=America%2FNew_York)
- [Meetups Checklist](/community/meetups/checklist/)
- [Swag](/handbook/marketing/corporate-marketing/merchandise-handling/#ordering-new-swag)
- [Speakers Bureau](/handbook/marketing/community-relations/developer-evangelism/speakers-bureau/)

### Resources for speakers

For resources for GitLab team members who are planning on attending events or speaking at conferences, see [Speaker Resources](/handbook/marketing/corporate-marketing/speaking-resources/). 
