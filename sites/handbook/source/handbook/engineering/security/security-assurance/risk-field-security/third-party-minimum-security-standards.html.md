---
layout: markdown_page
title: "Third Party Minimum Security Standards"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Third Party Minimum Security Standards

## Scope
In order to maintain the confidentiality, availability and integrity of GitLab [classified data](/handbook/engineering/security/data-classification-standard.html), this minimum security standards guide was developed. This guide is applicable to any Third Party with an **inherent risk score of Moderate or High** as determined by the first phase of the Third Party Risk Management procedure.

## Roles and Responsibilities

| Role | Responsibilities |
| ------ | ------ |
| **Business Owner (requester)** | Participate in the initial Inherent Risk determination process|
| | Provide Minimum Standards guide to the Third Party |
|  | Act as liaison with Third Party and Risk and Field Security team, as needed, to complete assessment |
| | At the end of the engagement, ensure all data is destroyed or returned |
| | Participate in periodic access reviews, as applicable |
| **Risk and Field Security Team** | Maintain Third Party Minimum Security Standards |
| | Review provided evidence as part of the Third Party Risk Management activities|
| | Communicate observations and advise in remediation |
| | Conduct additional risk-based reviews, as applicable |

## Standards

The following standards will be reviewed during the Third Party Risk Management procedure.

### Third Parties Providing Applications (Free or Paid)

- Third Party must have dedicated Security and Risk Management team(s) and documented procedures to support Security and Risk Management Activities
- Third Party must provide a copy of an annual independent audit of physical security controls for any in-scope facilities
- Third Party must provide a copy of an annual independent audit (example: SOC2 report or similar) and, if applicable, bridge letter
- Third Party must provide a copy of an annual Penetration Test report and status of any open Critical or High vulnerabilities
- Third Party access controls must meet or exceed GitLab's [account and password standards]and GitLab's [least privilege policy](/handbook/engineering/security/access-management-policy.html#principle-of-least-privilege)
- Application must meet or exceed GitLab's [application authentication standards](https://about.gitlab.com/handbook/security/#application-authentication-requirements)
- Application must meet or exceed GitLab's [systems monitoring controls](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html#systems-monitoring) and have the ability to integrate with GitLab's SIEM
- Third Party endpoints must meet or exceed GitLab's [endpoint security standards](/handbook/security/#laptop-or-desktop-system-configuration)
- Third Party endpoints must meet or exceed GitLab's [minimum encryption standards](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html#data-management)
- Third Party must provide data flow diagram, types of data, and storage locations of any GitLab data and, if applicable, any in-scope infrastructure. If the application is hosted on AWS, S3 bucket configurations must be allowed.
- Third Party must provide an attestation of immediate data destruction upon request or when the specific need for that data no longer exists (whichever comes first)
- Third Party must maintain data and infrastructure redundancy to support high availability
- Third Party must provide the last 12 months of uptime data

Third Parties who can not meet these standards will be asked to provide evidence of compensating controls. A Third Party Risk Exception will be created which must be approved by the business owner and security leadership.

### Third Parties Providing Services

**Professional Services Organizations**

- Third Party must provide information on their security and risk management practices to include, at a minimum, information about pre-employment screening, security awareness training, and privacy practices.
- Third Party access controls must meet or exceed GitLab's [account and password standards](GitLab's [least privilege policy](/handbook/engineering/security/access-management-policy.html#principle-of-least-privilege)
- Third Party endpoints must meet or exceed GitLab's [minimum encryption standards](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html#data-management) and GitLab's [Security Standards](/handbook/security/#laptop-or-desktop-system-configuration).
- Third Party must provide data flow diagram, types of data, and storage locations of any GitLab data.
- Third Party must provide an attestation of immediate data destruction upon request or when the specific need for that data no longer exists (whichever comes first)

**Individual Contractors**

If the Third Party will need access to GitLab systems that house classified data, including but not limited to Slack and Google Workspace, the below standards apply.

- Third Party must undergo an onboarding process that, at a minimum, includes completing a Non-Disclosure Agreement, background check, security awareness training, and review and acknowledgement of the [code of conduct](/handbook/people-group/code-of-conduct/). Please reference the [procurement contractor process](/handbook/finance/procurement/vendor-contract-professional-services/#-step-7--create-professional-services-access-request-optional) and orientation process for [external consultants](/handbook/people-group/general-onboarding/consultants/).
- Third Party must complete GitLab's [security contractor assessment](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/third-party-vendor-security-management/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).
- Third Party endpoints must meet or exceed GitLab's [Security standards](/handbook/security/#laptop-or-desktop-system-configuration)
- Third Party endpoints must meet or exceed GitLab's [minimum encryption standards](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html#data-management)
- Third Party must provide an attestation of immediate data destruction upon request or when the specific need for that data no longer exists (whichever comes first)

`Coming Soon`: List of compensating controls

### Third Parties Providing Physical Services (Field Marketing)

- Third Party must gain consent for any use of personal information. As an example, when sending gifts to individuals, consider if the individual is knowingly providing their address for the shipment.
- Third Party must utilize a secured method for receiving personal data such as sharing through a document controlled by GitLab where access can be revoked once completed.
- Third Party endpoints must meet or exceed GitLab's [Security standards](/handbook/security/#laptop-or-desktop-system-configuration)
- Third Party endpoints must meet or exceed GitLab's [minimum encryption standards](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html#data-management)
- Third Party must provide an attestation of immediate data destruction upon request or when the specific need for that data no longer exists (whichever comes first)

### Alliances

`Coming Soon`

## Mergers and Acquisitions

`Coming Soon`

## Guidance to sharing data externally

Before sending data **outside** of GitLab should review the following guidance:

<p>
<details>
<Summary>Data Classification</summary>
<br>
Team members should review the [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html). For data that is classified as Yellow or above:

- The Third Party must first establish a non-disclosure agreement or similar confidentiality contractual clauses. See the guide and template for [GitLab confidentiality agreements](https://about.gitlab.com/handbook/finance/procurement/#contract-templates).
- Use the principle of "need-to-know" and obtain approval by the appropriate data owner. Good questions to ask yourself are:
  - What specific data does the third party need?
  - What can be omitted?
  - In what manner will they use the data?
  - Will they download it and share it with a fourth party?
  - Where will the data be stored?
  - How will they destroy it when they are done?
  - How will access be logged and monitored?
  - How will access be revoked?
  - How will access be reviewed to ensure it is still appropriate?
</details>

<details>
<Summary>Consent for Use</summary>
<br>
For data that is classified as Yellow or above must have documented consent prior to sharing with Third Parties. Examples include:

- Customer Data (anything uploaded or created within GitLab by customers)
- Customer user analytics that is not anonymized
- Sending gifts to event attendees to a home address
- Team member personal information gathered during surveys
- Pictures and other identifying information of individuals

</details>

<details>
<Summary>Methods of Sharing Data</summary>

For data that is classified as Yellow or above, the following guidelines should be used:

GitLab’s Tools | Instructions and Controls
|--| --|
| GitLab | - Create a new private project dedicated to the engagement with external users. <br> - Do not create an issue in an existing private project as the Guest level member will not be able to see the issue if private. <br> - Add the external user with Reporter permissions. <br> - Open an issue to share the non-public data in the description of the issue and mark it confidential. <br> - Do not attach documents with the data as the attachment can be accessed unauthenticated with the direct link. <br> - Include the disclaimer below in the description along with the data. <br> - Remove the user from the project once done and delete the issue.
| Google Workspace | - Create a dedicated folder for External Sharing and a subfolder for the external party. <br> - Upload the data to a document or sheet and verify the considerations in Step 2 before sending the invite to the external users. <br> - Include the disclaimer below in the document. <br> - Share the document to the intended party with the appropriate access level. <br> - Grant Editor access only if they need to collaborate in order to limit their ability to change permissions or invite others to the sheet. <br> - Viewer or Commenter access will allow the party to download, print or copy so this is the preference if they do not need to edit the document.
|Company email | - Consult the Security Assurance team to validate any additional steps that are needed for the type of data shared. <br> - Encrypt the document or sheet and password protect. An easy way to accomplish this is by creating a file and encrypting it with the Mac built-in [Disk Utility capability](https://www.macupdate.com/blog/post/66-how-do-you-encrypt-files-folders-on-mac).  <br> - Choose a random password generator with at least 60 characters. 1Password provides a [generator tool](https://1password.com/password-generator/) where you can adjust the length. <br> - Add the disclaimer below in the document or create a separate sheet in the workbook. <br> - When sending the encrypted file, Gmail may prompt you to store it in G-Drive if the size is over 25MB. You should allow this even if your recipient doesn't have a Google account; the direct link shouldn’t require it. <br> - Send the password in a separate email to the recipient outside a different channel than the encrypted document sent. For example, if the document was sent via email the password can be shared by a text message, phone call or a different email account. <br> - Ask the recipient to confirm deletion of the emails once successfully opened, and the document when no longer needed.|

</details>

<details>
<Summary>Additional Tips</summary>
<br>

For data that is classified as Yellow or above:

- Do not store any local copies of data. If you have to download them, ensure you delete them as soon as possible.
- Do not use email to send data. If you receive an email containing these types of data, delete it and request it be sent using a secured method (see above).
- Create a mutual plan with timelines to return and/or dispose of the data once the Third Party no longer needs it. It might be helpful to schedule regular check-ins and identify deliverables that demonstrate expected progress.
- When the engagement is final, remove the user(s) from the document or sheet. Also, confirm with the external party that they have deleted any locally saved or shredded printed data.
- Utilize the below disclaimer below:

**Disclaimer**

>THIS DOCUMENT IS DISCLOSED ONLY TO THE RECIPIENT TO WHOM THIS DOCUMENT IS ADDRESSED AND IS PURSUANT TO A RELATIONSHIP OF CONFIDENTIALITY UNDER WHICH THE RECIPIENT HAS OBLIGATIONS OF CONFIDENTIALITY. THIS DOCUMENT MAY CONTAIN CONFIDENTIAL INFORMATION, PROPRIETARY INFORMATION AND/OR TRADE SECRETS (COLLECTIVELY “CONFIDENTIAL INFORMATION”) BELONGING TO GITLAB INC. AND/OR ITS AFFILIATES AND/OR SUBSIDIARIES. THE CONFIDENTIAL INFORMATION IS TO BE USED BY THE RECIPIENT ONLY FOR THE PURPOSE FOR WHICH THIS DOCUMENT IS SUPPLIED. THE CONTENTS OF THIS DOCUMENT ARE PROVIDED IN COMMERCIAL CONFIDENCE, SOLELY FOR THE PURPOSE, THIS DOCUMENT IS SUPPLIED TO THE RECIPIENT, BY GITLAB. ANY INFORMATION THAT GITLAB CONSIDERS TO BE A CONFIDENTIAL INFORMATION WILL NOT BE SUBJECT TO DISCLOSURE UNDER ANY PUBLIC RECORDS ACT. ANY DATA LOCALLY STORED MUST BE RETURNED AND/OR DISPOSED UPON COMPLETION OR NO LONGER NEEDED. PLEASE CONFIRM IN THIS DOCUMENT ONCE THE DISPOSAL IS DONE.

</details>

