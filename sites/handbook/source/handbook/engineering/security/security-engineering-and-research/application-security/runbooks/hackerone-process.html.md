---
layout: handbook-page-toc
title: "HackerOne Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### HackerOne Process

GitLab utilizes [HackerOne](https://hackerone.com/gitlab) for its bug bounty program. Security researchers can report vulnerabilities in GitLab applications or the GitLab infrastructure via the HackerOne website. Team members authorized to respond to HackerOne reports use procedures outlined here.
The `#hackerone-feed` Slack channel receives notifications of report status changes and comments via HackerOne's Slack integration.

For information or questions about the GitLab HackerOne program, please contact

- [James Ritchey](https://gitlab.com/jritchey) - Primary Program Manager, or
- [Ethan Strike](https://gitlab.com/estrike) - Secondary Program Manager

#### Working the Queue

- When beginning work on a report, the security team member should assign the
report to themselves immediately.
- It is OK to take a report that you will not work on immediately, especially
if it is a duplicate or related to another report you are familiar with, just
be sure to get it reassigned if you won't be able to meet the estimated triage time.
- When starting a triage work cycle, team members should prioritize as follows:
  1. Identify, triage, and [escalate any severity::1/priority::1](./handling-s1p1.html) issues first.
  1. Close duplicate and invalid reports.
  1. Triage further using Sort by "Oldest" reports.

  If a reporter has questions about the order of report responses, `06 - Duplicates/Invalid Reports Triaged First` common response may be used.
- Conduct initial communication with the reporter and investigation, using the
following guidelines as necessary:
    - [Request clarification](#if-a-report-is-unclear)
    - Attempt to verify the report and triage the vulnerability.
- Potential, non-bounty outcomes:
    - Report is out-of-scope. If actionable, issues may still be created, but
    further process may not be followed see [violating the rules](#if-a-report-violates-the-rules-of-engagement).
    - Report is a `~feature` as defined above and would not need to be
    made confidential or scheduled for remediation. An issue can be created, or
    requested that the reporter creates one if desired, but the report can be
    closed as "Informative".
    - Report is otherwise [Informative, Not Applicable, or Spam](#closing-reports-as-informative-not-applicable-or-spam).
    For example, we've gotten a number of reports for [inline HTML in markdown](https://docs.gitlab.com/ee/user/markdown.html#inline-html)
    that do not exceed the capabilities of markdown itself.
    - Report is a duplicate. Verify that the issue was not previously reported in HackerOne, or
    that an issue does not already exist in GitLab. If it is a duplicate:
        - Change the state of the report to "Duplicate".
        - If the issue was previously reported in H1, include the report id, as it can
        impact the reporter's reputation
        - Fill in the `01 - Duplicate` common response. Include the link to the GitLab issue.
        - The team member may use their discretion if the reporter asks to be added as a contributor to the original H1 report;
        however, the default is to not add because the corresponding GitLab issue will
        be made public 30 days after the patch is released. If it is decided to add the
        duplicate reporter, ensure that the report does not have reporter sensitive information before allowing access.
        `05 - Duplicate Follow Up, No Adding Contributors/Future Public Release` common response
        can be used when denying the request to add as a contributor.
        - If the new report makes us realize we have mishandled a previous report (e.g. closed as informative but it was a valid bug) we reopen the original and award the new report a bounty to thank the reporter for putting the mishandled issue back on our radar
          - The bounty we award as a thank you should be $100 for low severity reports and equivalent to the initial bounty we pay on triage for the higher severities
          - If we realize our mistake late in the process and we have already rewarded the new report, the bounty should be raised to the amounts above or left as is if it's already higher
          - The author of the duplicate report should also be thanked in the CVE credits and blog post
- If the report is valid, in-scope, original, and requires action, security-related documentation change, or further investigation by
the responsible engineering team:
    - Verify and/or set the appropriate Severity in H1
    - Verify and/or set the appropriate Weakness in H1
    - [Calculate the CVSS score](./cvss-calculation.html) and post the resulting vector string (e.g.: `AV:N/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:L`) as an internal comment on the report, this will be used later when requesting a CVE ID
      - We do not use the HackerOne field for CVSS because our bounties are based on our own assesment of severity and business impact rather than the CVSS score and sometimes those will differ
    - If the report is [permissions related](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html#permissions), check for similar issues in the API, GraphQL, and Elasticsearch, as appropriate. Also check with alternate authentication mechanisms like Deploy Tokens, Deploy Keys, Trigger Tokens, etc.
    - Add your initial [suggested bounty](https://docs.hackerone.com/programs/bounties.html#suggesting-bounties) in H1
        - If the severity is Medium or higher, award a bounty of $1000 when the report is triaged. See [GitLab's H1 Policy](https://hackerone.com/gitlab), under `Rewards`
        - If the report is a security-related documentation change, the bounty will be $100
    - Import the report into a GitLab issue using `/h1import <report> [project]` in Slack
        - Verify the Severity/Priority assigned by `h1import` ([Severity and Priority](/handbook/engineering/security/#severity-and-priority-labels-on-security-issues)
        - On the imported issue:
            - Assign the appropriate [Due Date](../#due-date-on-security-issues)
            - Have a proper [`How to reproduce`](../#reproducibility-on-security-issues) section
        - If the report is a security-related documentation change, add the `~documentation` label
    - @-mention product manager of appropriate teams for scheduling and/or the engineering managers if additional engineering feedback is required to complete the triage, based on the [product categories page](/handbook/product/categories/)
        - add labels (`/label ~` command) corresponding to the [DevOps stage](/handbook/product/categories/#devops-stages) and source group (consult the [Hierarchy](/handbook/product/categories/#hierarchy) for an overview on categories forming the hierarchy)
        - As applicable, notify relevant team members via the issue, chat, and email, depending on the chosen security level.
    - Change the state of the report to "Triaged" in HackerOne and include a link to the issue as the reference.
        - Fill in the comment using the `00 - Triaged` Common response as a template.
        - In the comment, include link to the confidential issue
    - If full impact is needed to be assessed against GitLab infrastructure, instead of testing in https://gitlab.com, use https://staging.gitlab.com/help to sign in with your GitLab email account
        - If multiple users are needed, use credentials for users gitlab-qa-user* stored in 1password Team Vault to access the staging environment
- Discussion and remediation of vulnerabilities can sometimes take longer than we would prefer. Even so, frequent communication with reporters is far better than leaving reporters in the dark, even if our progress is slow. Therefore:
    - Once the corresponding GitLab issue has been assigned a milestone, an
    automated process will add a comment to the H1 report with the planned
    fix date. If the issue has not been assigned to a milestone within **1 week**
    of the product manager being @ mentioned on the issue, follow up with
    the product management team.
    - In the case where fixes are not as clear or discussion is still
    on-going as to whether a patch will be created at all, reporters should
    be notified of updates at least **monthly**.
    - Awards may be paid in advance of a confirmed fix by the following review
      process to insure 1) the report is not duplicated by other active reports and
      2) consistency in award amounts:
      - Post a note on the current [~"bug bounty council"](https://gitlab.com/gitlab-com/gl-security/security-department-meta/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Bug%20Bounty%20Council) issue, following the
        format in the template, which includes a brief description, [CVSSv3 vector string](./cvss-calculation.html) and suggested
        bounty amount.
        - Wait for at least two thumbs up from other members of the team.
        - The `04 - Bounty Award / Reviewed and Awarded Prior to Fix` can be used
        as the template for the bounty award if approved.
    - In any case, no report should go "stale" where updates are not provided within the last month.

#### Application Security Engineer Procedures for severity::1/priority::1 Issues

Please see [Handling severity::1/priority::1 Issues](./handling-s1p1.html)

#### If a Report is Unclear

If a report is unclear, or the reviewer has any questions about the validity of the finding or how it can be exploited, now is the time to ask. Move the report to the "Needs More Info" state until the researcher has provided all the information necessary to determine the validity and impact of the finding. Use your best judgement to determine whether it makes sense to open a confidential issue anyway, noting in it that you are seeking more information from the reporter. When in doubt, err on the side of opening the issue.

One the report has been clarified, follow the "regular flow" described above.

#### If a Report Violates the Rules of Engagement

If a report violates the rules of GitLab's bug bounty program use good judgement in deciding how to proceed. For instance, if a researcher has tested a vulnerability against GitLab production systems (a violation), but the vulnerability has not placed GitLab user data at risk, notify them that they have violated the terms of the bounty program but you are still taking the report seriously and will treat it normally. If the researcher has acted in a dangerous or malicious way, inform them that they have violated the terms of the bug bounty program and will not receive credit. Then continue with the "regular flow" as you normally would.

#### Closing reports as Informative, Not Applicable, or Spam

If the report does not pose a security risk to GitLab or GitLab users it can be closed without opening an issue on GitLab.com.

When this happens inform the researcher why it is not a vulnerability. It is up to the discretion of the Security Engineer whether to close the report as "Informative", "Not Applicable", or "Spam". HackerOne offers the following guidelines on each of these statuses:

- Informative: Report contained useful information but did not warrant an immediate action.
- Not Applicable: Report was invalid, ineligible, or irrelevant.
- Spam: Report is not a legitimate attempt to describe a security issue.

We mostly use the "Informative" status for reports with little to no security impact to GitLab and "Not Applicable" for reports that show a poor understanding of security concepts or reports that are clearly out of scope. "Spam" results in the maximum penalty to the researcher's reputation and should only be used in obvious cases of abuse.

#### Breakdown of Effective Communication

Sometimes there will be a breakdown in effective communication with a reporter. While this could happen for multiple reasons, it is important that further communication follows [GitLab's Guidelines for Effective and Responsible Communication](/handbook/communication/#effective--responsible-communication-guidelines). If communication with a reporter has gotten to this point, the following steps should be taken to help meet this goal.

- Take a step back from the situation. Do not respond immediately.
- Seek advice from the H1 triage engineer or, if you are more comfortable, a manager on how best to move forward.
- Consider writing the response multiple times, and have the response reviewed by the person you consulted.
- Once diffused, if the general situation is not currently documented, open an MR to the handbook on how it can be handled in the future.

#### Reports potentially affecting third parties

When GitLab receives reports, via HackerOne or other means, which might affect third parties the reporter will be encouraged to report the vulnerabilities upstream. On a case-by-case basis, e.g. for urgent or critical issues, GitLab might proactively report security issues upstream while being transparent to the reporter and making sure the original reporter will be credited. GitLab team members however will not attempt to re-apply unique techniques observed from bug bounty related submissions towards unrelated third parties which might be affected.

#### Awarding Ultimate Licenses

GitLab reporters with 3 or more valid reports are eligible for a 1-year Ultimate license for up to 5 users. As per the [H1 policy](https://gitlab.com/gitlab-com/gl-security/hackerone/configuration/-/blob/master/program-policy.md#gitlab-ultimate-license), reporters will request the license through a comment on one of their issues. When a reporter has requested a license, the following steps should be taken:

1. Validate that the three reports were valid. That means they are `Triaged` or `Resolved`.
1. Validate that the three reports have not been used to obtain a previous license.
1. If the reports are not valid, respond to the reporter on H1 explaining the reason the license is not being issued.
1. If the reports are valid, create the license on `https://license.gitlab.com`,
    - For `Name` use the reporters fullname if available, otherwise their H1 handle
    - For `Company` use `H1 Reporter Award`
    - For `Email` use the reporter's `[username]@wearehackerone.com` email address
    - `User Count` is up to 5
    - `GitLab Plan` is `Ultimate`
    - The license should start the day of issue and expire in 1 year
1. Enter the associated license information in the [H1 License Award sheet](https://docs.google.com/spreadsheets/d/1qJZ9jfIvQuSU5u4odj4Db_CRKJ_GHegtSZQvJx36FUE/edit)
1. Reply to the report on H1 use the `20 - Ultimate License Creation` template.

The license will be sent to the reporter by LicenseDot. If the reporter claims that the license has not arrived, the app can be used to resend the license.
When that happens, the creation of a new license should be avoided.
