---
layout: handbook-page-toc
title: Trust & Safety Team 
description: "GitLab.com Trust & Safety Team Overview" 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

The Trust & Safety team are the guardians of the anti-abuse world, who develop the tools and manage the workflows to mitigate [abusive activity](#what-is-abuse) on GitLab.com.

## Our Vision

Making the internet safer by reducing malicious activity originating from GitLab.com.
 
## Our Mission

The Trust & Safety team investigates and mitigates the malicious use of GitLab.com and it’s associated features and tools with the goal of making the internet a safer place. In order to achieve this we must ensure that we are good internet citizens. 
 
### Initiatives for this specialty include:

* Detection and mitigation of [abusive activity](#what-is-abuse) on GitLab.com.
* [DMCA Notice and Counter-Notices](#dmca-requests) processing.
* Escalating potential abuse vectors to stakeholders.
* Research and prevention trending abuse methodologies on GitLab.com.
 
***Code of Conduct Violations*** are handled by the [Community Advocates](/handbook/marketing/community-relations/community-advocacy/) in the [Community Relations Team](/handbook/marketing/community-relations/). For more information on reporting these violations please see the [GitLab Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/) page.

## Team Members

The following people are permanent members of the Trust & Safety Team

<table>
<thead>
<tr>
<th>Person</th>
<th>Role</th>
</tr>
</thead>
<tbody>
<tr>
<td>Roger Ostrander</td>
<td><a href="/job-families/engineering/trust-and-safety">Security Engineer, Trust & Safety</a></td>
</tr>
<tr>
<td>Shawn Sichak</td>
<td><a href="/job-families/engineering/trust-and-safety">Security Engineer, Trust & Safety</a></td>
</tr>
<tr>
<td>Westley van den Berg</td>
<td><a href="/job-families/engineering/trust-and-safety">Security Analyst, Trust & Safety</a></td>
</tr>
<tr>
<td>Charl de Wit</td>
<td><a href="/job-families/engineering/trust-and-safety">Security Manager, Trust & Safety</a></td>
</tr>
</tbody>
</table>
 
## Contact Us

If you would like to report any [abuse](#what-is-abuse) originating from GitLab.com, please see our [**How to Report Abuse**](#how-to-report-abuse) section below.

GitLab team members can tag (`@gitlab-com/gl-security/security-operations/trust-and-safety`) us on any issue they require our input on, or create an issue in our [Operations Issue tracker](https://gitlab.com/gitlab-com/gl-security/security-operations/trust-and-safety/operations/-/issues). If it's a time sensitive issue, please reach out on Slack in the `#abuse` channel or by using `@trust-and-safety`. 

For any abuse prevention feature requests and suggestions for Gitlab.com, CE and EE, please create a **Feature proposal** Issue from the provided templates in the [GitLab Project](https://gitlab.com/gitlab-org/gitlab/-/issues) and add the ~"Abuse Prevention" label. Feel free to us for additional input `@gitlab-com/gl-security/security-operations/trust-and-safety` or if you have any questions.

If you are running your own GitLab instance and are looking for some pro-tips on dealing with abuse, please see our [Trust & Safety DIY page](/handbook/engineering/security/security-operations/trustandsafety/diy.html).

For Open Source Program Partners, Premium and Ultimate customers, you can reach us at `abuse@gitlab.com` if you would like to discuss, and potentially improve, your current spam prevention and mitigation strategies. Please include any relevant information as to the abuse issue you are currently experiencing to help us provide the most relevant information. 

## Dashboards

[**Trust & Safety Mitigation Data**](https://app.periscopedata.com/app/gitlab/642878/WIP:-Abuse-Mitigation-Dashboard) *(GitLab internal)*

Total accounts mitigated per week
  - Defined as all accounts mitigated for abusive activity or content in a given week (weeks run from Mon to Sun).

Automatic vs Manual
  - Defined as the volume of accounts detected and mitigated via automation vs manual detection (via abuse reports/investigations/monitoring) and mitigation. 

Average Time to Mitigation
  - Time to Mitigation is defined as the date/time between the creation of an abusive account and the date/time of taking administrative action on the account.  

Accounts mitigated per category
  - This is a breakdown of the total accounts mitigated per abuse category.

[**Cost of Abuse**](https://app.periscopedata.com/app/gitlab/761157/Cost-of-Abuse) *(GitLab internal)*
 
# Trust & Safety Resources
 
## What is Abuse?
Abuse is the intentional misuse of GitLab products/services to cause harm or for personal gain. (Abuse is covered under Section 3 of the GitLab Website [Terms of Use](/terms/#gitlab_com))

### Examples of common forms of Abuse include, but are not limited to:
 
**1. Malware:** Defined as software that is designed and distributed with the intention of causing damage to a computer, server, client, or computer network.

Making use of GitLab.com services to deliver malicious executables or as attack infrastructure is prohibited under the [GitLab Website Terms of Use](/terms/#gitlab_com) (**Section 3, “Acceptable Use of Your Account and the Website”**).
We do however understand that making such technical details available for research purposes can benefit the wider community and as such it will be allowed if the content meets the following criteria:
  - The Group and Project descriptions must clearly describe the purpose and author of the content.
  - Further details about specific project content that can be independently verified by the **GitLab Security** department must be present in the project `README.md` file; for example, links to supporting materials such as a blog post describing the project.
  - All malicious binaries/files are stored in password protected archive files, with the passwords clearly documented; for example, placed in the repository’s `README.md`.
     * Example: https://github.com/ytisf/theZoo
     * `git-lfs` is available for use for binary files on GitLab.com.
  - Non-profit open source projects may meet the requirements to qualify for our [GitLab for Open Source](/solutions/open-source/join/) program.

**2. Commercial Spam:** An account that's been created for the purpose of advertising a product or service.

**3. Malicious Spam:** An account that’s been created for the purpose of distribution of fraudulent, illegal, pirated or deceptive content.

**4. CI Abuse:** Making use of CI Runners for any other purpose than what it is intended for. Examples include, but are not limited to:
  - Cryptocurrency Mining
  - Network Abuse (Denial of Service, Scraping, etc.)


**5. Prohibited Content:** Distributing harmful or offensive content that is defamatory, obscene, abusive, an invasion of privacy (Personally Identifiable Information/PII) or harassing.
  - This includes distributing PII obtained from a data breach.
  - For any reports of Child Sexual Abuse Material (CSAM) please notify [INHOPE](https://www.inhope.org/EN) via the [`Report Content`](https://www.inhope.org/EN#hotlineReferral) button.

**6. GitLab Pages**: Pages Abuse: Include, but are not limited to:
  - Web Spam,
  - Phishing Pages
  - Pages that contain Obscene or Harmful content
 
## How to Report Abuse
You can report abuse on GitLab.com via the `Report Abuse` [button](https://docs.gitlab.com/ee/user/abuse_reports.html) while logged in.
	* Please ensure to include any relevant details pertaining to your report in the text field.
Alternatively you can eMail abuse report to `abuse@gitlab.com`
For DMCA Notices please email `dmca@gitlab.com`
 
## DMCA Requests
 
The Trust & Safety team are responsible for processing Digital Millennium Copyright Act (DMCA) notices. All DMCA requests need to be vetted by Legal first before we proceed with the take down of the reported content. 
 
### For DMCA requests the Trust & Safety Team will follow the below process
 
* Confirm that the content is still live. 
* Confirm that the DMCA notice meets the legal requirements in order to proceed.

* Remove access to the reported content, then notify the implicated user of the notice and request they take the necessary action.
* If no response has been received by the implicated user or a counter-notice was submitted, we will remove access to the content and disable the account.
* If a valid counter-notice is received we will return access to the content.
 
For more detailed information regarding how we process DMCA related notices, please see: [DMCA Removal Workflow](/handbook/engineering/security/security-operations/trustandsafety/dmca-removal-requests.html)
 
## Our Mission statement

Be good internet citizens.
 
### How do we achieve this? 

* Be honest and trustworthy
* Follow the rules and laws
* Be informed about the world around you
* Respect the property of others
* Be active in the community
* Be compassionate
* Be a good neighbour
* Protect the GitLab environment
* Respect the rights of others
* Take responsibility for your actions

