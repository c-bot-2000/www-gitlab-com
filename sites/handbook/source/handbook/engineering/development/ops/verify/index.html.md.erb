---
layout: handbook-page-toc
title: "Verify Stage"
description: "The verify development group handbook page."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

To enable software teams around the world to be confident in their delivery by providing a [comprehensive CI platform](/direction/ops/#verify) that enables them to [Do Powerful Things Easily](/direction/ops/#do-powerful-things-easily).

## Mission

As engineers in Verify we know our customers because we _are_ our customers, and we are constantly striving to make our platform better for everyone. We do this through [iteration](/handbook/values/#iteration), [dogfooding](/handbook/values/#dogfooding), and being involved in our open source community. We innovate, we collaborate, and we challenge assumptions to arrive at great results.

We take ownership of the things we build, with a focus on stability and availability. We do this by having a deep technical understanding of the operation and performance characteristics of our platform, and a proactive perspective to future growth.

## Who we are

The Verify stage is made up of 4 groups:

1. [Verify:Continuous Integration](/handbook/engineering/development/ops/verify/continuous-integration/)
1. [Verify:Pipeline Authoring](/handbook/engineering/development/ops/verify/pipeline-authoring/)
1. [Verify:Runner](/handbook/engineering/development/ops/verify/runner/)
1. [Verify:Testing](/handbook/engineering/development/ops/verify/testing/)

## UX Strategy

For of our UX Vision and Strategy, take a look at the [Verify UX Strategy](/handbook/engineering/ux/stage-group-ux-strategy/verify/) page.

### Jobs to be done (JTBD)

A [Job to be Done (JTBD)](/handbook/engineering/ux/jobs-to-be-done/) is a framework, or lens, for viewing products and solutions in terms of the jobs customers are trying to achieve.

* [Verify:Continuous Integration JTBD](/handbook/engineering/development/ops/verify/continuous-integration/jtbd/)
* [Verify:Pipeline Authoring JTBD](/handbook/engineering/development/ops/verify/pipeline-authoring/jtdb/) 
* [Verify:Runner JTBD](/handbook/engineering/development/ops/verify/runner/jtbd/)
* [Verify:Testing JTBD](/handbook/engineering/development/ops/verify/testing/JTBD/)

### Continuous Integration and Pipeline Authoring Collaboration

Pipeline Authoring and Continuous Integration are closely related but they also represent different stages in the cycle of a user's interaction with a pipeline. At a very high-level, this image illustrates the main focus of each group and how they can both support a better pipeline experience.

![Verify Groups](/images/handbook/engineering/verify/verify_groups_banner.jpg)

### Verify:Continuous Integration

<%= direct_team(manager_role: 'Backend Engineering Manager, Verify:Continuous Integration & Verify:Pipeline Authoring', role_regexp: /Continuous Integration/) %>

<%= direct_team(manager_role: 'Frontend Engineering Manager, Verify', role_regexp: /Continuous Integration/) %>

### Verify:Pipeline Authoring
<%= direct_team(manager_role: 'Backend Engineering Manager, Verify:Continuous Integration & Verify:Pipeline Authoring', role_regexp: /Pipeline Authoring/) %>

<%= direct_team(manager_role: 'Frontend Engineering Manager, Verify', role_regexp: /Pipeline Authoring/) %>

### Verify:Runner
<%= direct_team(manager_role: 'Backend Engineering Manager, Verify:Runner') %>

### Verify:Testing
<%= direct_team(manager_role: 'Backend Engineering Manager, Verify:Testing') %>

## How we work

Each group in Verify is encouraged to define their own process based on what works best for them. Individual group processes are described on the pages below:

* [Verify:Continuous Integration](/handbook/engineering/development/ops/verify/continuous-integration/)
* [Verify:Pipeline Authoring](/handbook/engineering/development/ops/verify/pipeline-authoring/)
* [Verify:Runner](/handbook/engineering/development/ops/verify/runner/)
* [Verify:Testing](/handbook/engineering/development/ops/verify/testing/)

### Decoupled project management

Many of the groups in CI/CD have separate Frontend and Backend Engineering Managers. Given each group's ability to define their own process, groups may also consider decoupling project management functions from people management functions.

For example, a group's managers could determine that one of them will be the DRI for a given project, or the entire group. That individual would then be responsible for planning for the whole team, instead of splitting the planning responsiblity between two managers which requires extra coordination.

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%=
role_regexp = /[,&] Verify/
direct_manager_role = 'Senior Manager, Engineering, Verify'
other_manager_roles = [
  'Frontend Engineering Manager, Verify',
  'Backend Engineering Manager, Verify:Continuous Integration & Verify:Pipeline Authoring',
  'Backend Engineering Manager, Verify:Runner',
  'Backend Engineering Manager, Verify:Testing'

]
stable_counterparts(role_regexp: role_regexp, direct_manager_role: direct_manager_role, other_manager_roles: other_manager_roles)
%>

## Verify Engineering Update Newsletter

Every two weeks the Verify Engineering Update Newsletter is set out to an opt-in subscriber list. The purpose of the email is to share recent highlights from the Verify stage so folks will have a better idea of what is happening on other teams, and provide new opportunities for learning and collaboration.

Everyone is welcome to sign up or view previous issue on the [newsletter page](https://www.getrevue.co/profile/verify-engineering-update).

Each issue of the newsletter is planned using individual issues linked in the [newsletter epic](https://gitlab.com/groups/gitlab-com/-/epics/1148). Content is generally contributed by managers, but everyone is encouraged to contribute topics for the newsletter.

## Verify Technical Discussions

Verify Technical Discussions is a Zoom meeting hosted bi-weekly by Verify Team. Everyone is invited, however paricipation of Verify stage members is especially encouraged.

During the meeting we discus a variety of technical aspects related to the Verify stage roadmap.

Everyone can add their points to the [agenda document](https://docs.google.com/document/d/1SEydi30hYjqZ5ellZgwcijUesRnWjQVd___h3J9SSKY).

Below you can find a table with links to recordings of Verify Technical Discussions and Technical Deep Dives.

| Date       | Title                                                          | Recording                                 |
|------------|----------------------------------------------------------------|-------------------------------------------|
| 2021-01-21 | Technical Discussions - Pipeline Editor and database storage   | [Recording](https://youtu.be/KsOR3lIz_4w) |
| 2021-01-07 | Technical Discussions - Next iteration of CI/CD Pipeline DAG   | [Recording](https://youtu.be/CvYEqSwd-UE) |
| 2020-12-10 | Technical Deep Dive - Observability at GitLab with demos       | [Recording](https://youtu.be/DVNyH3zQWBo) |
| 2020-11-19 | Technical Deep Dive - Cloud Native Build Logs feature overview | [Recording](https://youtu.be/Fq1ecmb_zk0) |
| 2020-05-08 | Technical Deep Dive - Using Prometheus with GitLab Compose Kit | [Recording](https://youtu.be/y9NW7A_XJrU) |

## Weekly Triage Reports

The Product Manager, Engineering Manager(s), and Designer for each group are responsible for triaging and scheduling [feature proposals and bugs as part of the Weekly Triage Report](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#group-level-bugs-features-and-ux-debt). Product Managers schedule issues by assigning them to a Milestone or the Backlog.  For bug triage, Engineering Managers and Product Managers work together to ensure that the correct `severity` and `priority` labels have been applied.  Since [Product Managers are the DRIs for prioritization](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#priority), they will validate and/or apply the initial `priority` label to bugs.  Engineering Managers are responsible for adding or updating the `severity` labels for bugs listed in the triage report, and to help Product Management with understanding the criticality and technical feasibility of the bug, as needed.

While SLOs for resolving bugs are tied to severities, it is up to Product Management to set priorities for the group with an appropriate target resolution time. For example, criteria such as the volume of `severity::2` level bugs may make it appropriate for the Product Manager to adjust the priority accordingly to reflect the expected time to resolution.

## Common Links

### Slack Channels

* Verify [#s_verify](https://gitlab.slack.com/archives/C0SFP840G)
  * Verify:Continuous Integration [#g_ci](https://gitlab.slack.com/archives/CPCJ8CCCX)
  * Verify:Pipeline Authoring [#g_pipeline-authoring](https://gitlab.slack.com/archives/C019R5JD44E)
  * Verify:Runner [#g_runner](https://gitlab.slack.com/archives/CBQ76ND6W)
  * Verify:Testing [#g_testing](https://gitlab.slack.com/archives/CPANF553J)
* Verify Engineering Managers [#s_verify-em](https://gitlab.slack.com/archives/C014UTH4W02)
* Verify Frontend Engineering [#s_verify_fe](https://gitlab.slack.com/archives/CUYH1MP0Q)
* CI/CD UX [#ux_ci-cd](http://gitlab.slack.com/archives/CL9STLJ06)
