---
layout: handbook-page-toc
title: Zendesk Support Instances
category: Zendesk
description: The Global GitLab Support Zendesk Instance is the instance where we receive and work on most of our customers' tickets.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Zendesk Support Instances

## GitLab Support Zendesk Instance: [gitlab.zendesk.com](https://gitlab.zendesk.com)

The Global GitLab Support Zendesk Instance is the instance where we receive and work on most of our customers' tickets. This instance is accessible to both, GitLab.com and Self-managed users. 

GitLab.com and Self-Managed tickets are worked on by our Support Engineers. All of our Support Managers and Support Engineers have access to this instance.

Tickets are accessed by different addresses for customers and for Support Agents, where `nnnnnn` is the ticket number:

* Customers: `https://support.gitlab.com/hc/requests/nnnnnn`
* Agents: `https://gitlab.zendesk.com/agent/tickets/nnnnnn`

## GitLab US Federal Support Zendesk Instance: [gitlab-federal-support.zendesk.com](https://gitlab-federal-support.zendesk.com)

The US Federal Instance is only accessible to our US Federal customers who require their tickets to be worked on exclusively by US Citizens. The US Federal Zendesk instance is only available to our Premium and Ultimate Customers and currently offers support during US Business Hours (0600 to 1800 Pacific Time). Only our US Citizens Support Engineers have access to this instance because we don't accept tickets from GitLab.com customers. If a user who is not a Federal Customer submits a ticket to our Federal Instance, our engineers will let them know that their ticket must be submitted to our Main GitLab Support Zendesk Instance (gitlab.zendesk.com).

Our customers in the US Federal Zendesk Instance will receive the Tiered Support response times outlined in our [Support Page](/support/#priority-support) but the response times will be calculated on Business Hours instead of Calendar Hours from Monday to Friday.

The customer facing URL for the Federal Zendesk Instance is [federal-support.gitlab.com](https://federal-support.gitlab.com) which is a CNAME for [gitlab-federal-support.zendesk.com](https://gitlab-federal-support.zendesk.com).

#### Emergency Requests

Our US Federal Customers will be provided with an email address they can email to in case of emergency. The emergency email triggers an emergency page that our engineers will respond to within the time outlined in our Support Page. 

#### Working Tickets

Please see [Working with federal tickets](/handbook/support/workflows/federal_tickets.html) for information about working tickets on this instance.
