#--------------------------------------
# Monorepo-related configuration
#--------------------------------------

monorepo_root = File.expand_path('../..', __dir__)

# hack around relative requires elsewhere in the shared code by adding the monorepo root to the load path
$LOAD_PATH.unshift(monorepo_root)

set :data_dir, "#{monorepo_root}/data"
set :helpers_dir, "../../helpers" # This has to be relative, because Middleman's ExternalHelpers#after_configuration uses File.join(app.root, ...)

require_relative '../../extensions/monorepo.rb'
activate :monorepo do |monorepo|
  monorepo.site = 'handbook'
end

#--------------------------------------
# End of Monorepo-related configuration
#--------------------------------------

require 'extensions/only_debugged_resources'
require 'extensions/partial_build_handbook'
require 'extensions/proxy_server_information'
require "lib/homepage"
require 'lib/mermaid'
require 'lib/plantuml'
require 'lib/code_owners'
require 'generators/prodops_direction'

#----------------------------------------------------------
# Global config (not specific to development or build mode)
#----------------------------------------------------------

# Settings
set :haml, { format: :xhtml }
set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Paths with custom per-page overrides
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# Don't render or include the following into the sitemap
ignore '**/.gitkeep'

# Extensions
activate :syntax, line_numbers: false
if ENV['MIDDLEMAN_DEBUG_RESOURCE_REGEX']
  ::Middleman::Extensions.register(:only_debugged_resources, OnlyDebuggedResources)
  activate :only_debugged_resources
end
activate :partial_build_handbook # Handle splitting of files across CI jobs

# Hiring chart pages
Gitlab::Homepage::Team.new.unique_departments.merge!(company: 'Company').each do |slug, name|
  proxy "/handbook/hiring/charts/#{slug}/index.html", "/handbook/hiring/charts/template.html", locals: { department: name }, ignore: true
end

# Compensation Roadmaps
data.compensation_roadmaps.each do |compensation_roadmap|
  proxy "/handbook/engineering/compensation-roadmaps/#{compensation_roadmap.slug}/index.html", "/handbook/engineering/compensation-roadmaps/template.html", locals: {
    compensation_roadmap: compensation_roadmap
  }, ignore: true
end

# GitLab Projects
proxy '/handbook/engineering/projects/index.html',
      '/handbook/engineering/projects/template.html',
      locals: { team: Gitlab::Homepage::Team.new },
      ignore: true

# Product Operations page item generators
if ENV['PRIVATE_TOKEN']
  prodops_direction = Generators::ProdOpsDirection.new

  # Set second argument to 'true' to include Epics, third to 'true' to include Issue, forth to 'true' to include Merge Requests
  # Each string in 'prod_ops_label_sets' array generates one list of Issues/MRs/Epics from current and recent milestones
  pdf_labels = ['Product Operations,product development flow,prodops:release']
  # pdf_releases = prodops_direction.generate_prod_ops_direction(true, pdf_labels) # /handbook/product-development-flow/releases/
  pdf_releases = prodops_direction.labeled_items_by_milestone(pdf_labels, false, false, true)

  proxy '/handbook/product-development-flow/releases/index.html', '/handbook/product-development-flow/releases/template.html', locals: {
    pdf_releases: pdf_releases
  }, ignore: true
end

#----------------------------------------------------------
# End global config (not specific to development or build mode)
#----------------------------------------------------------

# Development-specific config
configure :development do
  # There is no root index.html in handbook sub-site, so redirect root to handbook/index.html
  redirect "index.html", to: "/handbook/index.html"

  # Reload the browser automatically whenever files change
  activate :livereload unless ENV['ENABLE_LIVERELOAD'] != '1'

  # External Pipeline
  unless ENV['SKIP_EXTERNAL_PIPELINE']
    # NOTE: if we use the external pipeline, we set the Middleman port to 7654,
    # which the webpack devServer config relies on in webpack.config.js in the project root.
    # We set up webpack devServer on port 4567 to keep the local dev commands the same,
    # but it proxies to 7654 for everything other than the webpack assets.
    # See doc/webpack.md for more information.
    set(:port, 7654)
    # This Middleman extension logs a custom message about the Webpack proxy port
    activate :proxy_server_information
    # NOTE: This only applies to 'development' mode.  For local builds, use the `rake build:*` tasks
    activate :external_pipeline,
             name: :frontend,
             command: "cd #{monorepo_root} && yarn run start-rollup",
             source: "#{monorepo_root}/tmp/frontend",
             latency: 3
    # NOTE: This runs webpack and makes assets availble during local development
    activate :external_pipeline,
             name: :webpack,
             command: "cd #{monorepo_root} && yarn run start-webpack",
             source: "#{monorepo_root}/tmp/dist",
             latency: 3
  end

  activate :autoprefixer do |config|
    config.browsers = ['last 2 versions', 'Explorer >= 9']
  end
end

# Build-specific configuration
configure :build do
  Kramdown::Converter::PlantUmlHtmlWrapper.plantuml_setup

  # Mermaid diagrams don't render without line breaks
  activate :minify_html, preserve_line_breaks: true
end
