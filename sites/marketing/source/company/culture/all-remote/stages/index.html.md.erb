---
layout: handbook-page-toc
title: "The 10 stages of remote work"
canonical_path: "/company/culture/all-remote/stages/"
description: "On this page GitLab detail the 10 stages of remote work. This should be viewed as a sliding scale. Learn more!"
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

![GitLab collaboration illustration](/images/all-remote/gitlab-collaboration-illustration.jpg){: .medium.center}

On this page, we're detailing the 10 stages of remote work. This should be viewed as a sliding scale.

For companies transitioning into a remote environment, visit GitLab's guide to understanding the [phases of remote adaptation](/company/culture/all-remote/phases-of-remote-adaptation/).

For job seekers tracking companies transitioning to remote, bookmark the [Remote Work Policy Tracker](https://remote.lifeshack.io/).

## 1. No remote

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/_HhlqwJsNyM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, Darren (GitLab) and Anna-Karin (Because Mondays) discuss a number of challenges and solutions related to remote work, transitioning a company to remote, working asynchronously, and defaulting to documentation.*

Some enterprises do not allow for any remote work. This could be due to a leadership mandate, or could be attributed to the nature of the business. For example, medical care, broadcasting from live events, and certain forms of manufacturing do not lend themselves to being successfully completed while remote. 

It is worth noting, however, that even businesses which have historically been impossible to complete remotely are seeing new opportunity arise thanks to technological advancements. 

The da Vinci Surgical System, for instance, has been [used in telesurgery](http://www.bbc.com/future/story/20140516-i-operate-on-people-400km-away), and communications infrastructure is robust enough in some locales to support [remote broadcasting](https://ftw.usatoday.com/2015/06/espn-broadcasts-remote-integration-save-millions). 

Multinational corporations with multiple offices across the globe are inherently remote, even if they do not allow remote work. An employee in one office is inherently remote to another employee in another office, and a refusal to recognize this reality can create dysfunction when attempting to collaborate across offices. 

## 2. Remote time

Remote time may be referenced as remote tolerated. This stage of remote allows all approved employees in a company to work *some* (but not *all*) days from home, or a place of their choosing. 

This is commonly seen in agency and corporate environments where "remote Fridays" are sold as a perk to employment. In such scenarios, it is clear that leadership is not piloting remote work as a means to judge the feasability of all-remote, but rather compromising with employee demands for greater flexibility. 

Such employers are tolerant of some amount of working outside of the office, but still expect an individual to spend the bulk of their time in the office.

Some employers allow specific remote time, such as allowing an individual or team to work remotely a certain amount of days or hours per week. This can vary greatly amongst companies, with some being very rigid (e.g. the entire company may work remotely on Fridays, but there are no exceptions to the day) to very loose (e.g. a proven individual may work remotely up to three days per week of their choosing).

While this stage provides added flexibility, there is much ambiguity, and the looseness is generally left up to individual managers to decide. This also hampers mobility. Employees which must commute into the office a certain amount of days per week cannot reasonably explore relocation to areas which may be more amenable, and will likely not invest heavily in creating a focused, ergonomic [workspace](/company/culture/all-remote/workspace/) at home given the in-office requirement. 

## 3. Remote exceptions

In this stage, some employees may work remotely indefinitely due to their role, while others are required to work from a company office. This is frequently seen in companies which mandate that executive leadership work out of the office by default.

This arrangement may be amenable to those who are allowed to be full-time remote workers, but it can breed resentment in those who are forced into the office. Plus, exceptions who choose to relocate to an area far away from the company's headquarters are always at risk of being called back into the office. In such a scenario, they would be forced to either relocate back to a commutable distance from the headquarters, or resign and look for other employment. 

## 4. Remote allowed

In this stage, remote is allowed for anyone at the company (with very few edge cases, such as those who are employed to physically service a building or must work with equipment that cannot be transported to one's home). 

Job seekers should [inquire](/company/culture/all-remote/evaluate/) to what degree remote workers are supported in such environments. There is a marked difference between *allowing* remote and *supporting* remote. Some employers reason that remote employees are bypassing a commute, therefore no additional support is warranted. This may be referenced as the "Be grateful you don't have to commute" cloud that often hangs over the head of remoters. 

Other employers make concerted efforts to ensure that remoters feel included, valued,  and welcomed. 

Firms such as [Twitter](https://blog.twitter.com/en_us/topics/company/2020/keeping-our-employees-and-partners-safe-during-coronavirus.html) and [Shopify](https://www.shopify.com/blog/working-remotely-for-the-planet) are embracing this approach following the COVID-19 pandemic.

## 5. Hybrid-remote

Hybrid-remote is a tempting compromise that [masks many downsides](https://twitter.com/sytses/status/1264341436138270720). In a hybrid-remote organization, leaders are forced to manage two fundamentally distinct ways of working. There is administrative burden to manage a default-onsite experience and a default-offsite experience, and some employees may choose to oscillate between the two. 

There are many flavors of [hybrid-remote](/company/culture/all-remote/part-remote/). The key attribute of a hybrid-remote arrangement is that *some* employees — but not all — are allowed to work remotely 100% of the time. These organizations have at least one physical office where most of the company physically commutes to each working day. 

While such organizations work well for some employees, it's important to note that many hybrid-remote companies are not comfortable being publicly labeled as such. Their job vacanies are oftentimes not listed as remote-friendly. Should you apply for such a role, be prepared to lobby for assuming the role in a remote environment every step of the way.

## 6. Remote days

In an effort to ensure that there is only ever one playing field to manage, some companies may allow remote days where the entire company (executives included) work remotely at the same time. 

This approach may lead to stronger remote muscle being built, and [remote-first workflows](/company/culture/all-remote/how-to-work-remote-first/) used on remote days could become the default even on colocated days.

## 7. Remote-first

Remote-first organizations optimize their company for remote. They create documentation, policies, and [workflows](/company/culture/all-remote/how-to-work-remote-first/) that work when assuming that 100% of the organization is remote, even if some continue to occasionally visit a company-owned office.

These companies work to ensure that offices are simply [venues to work remotely from](/company/culture/all-remote/how-to-work-remote-first/#offices-are-simply-venues-to-work-remotely-from), and are not the epicenter of power or culture. 

[Quora shifting to remote-first](https://news.ycombinator.com/item?id=23646164) in June 2020 is an example of this, as is [Upwork's move](https://twitter.com/hydnbrwn/status/1263840533144727552) in May 2020. 

## 8. Remote only

With remote only there is no co-located work in a common office.
The difference with all remote is that the work is still biased towards one time zone.

Certain companies allow employees to work remotely, but maintain "core team hours." [InVision Studio](https://www.invisionapp.com/inside-design/studio-remote-design-team/), for example, has members spread across multiple countries and time zones, but aims to achieve "at least a 4-hour overlap with InVision’s core team hours, 10am–6pm Eastern Standard Time." 

This tends to attract employees who are in relative close proximity to one another, or at the very least, in a nearby time zone even if located in a different hemisphere. 

## 9. All-remote

All-remote means you work mostly asynchronous to allow you to work across time zones.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/jdN5mj5ieLk?start=2115" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

As part of a Harvard Business School case study [interview](https://youtu.be/jdN5mj5ieLk) (shown above), GitLab co-founder and CEO Sid Sijbrandij spoke with Professor Prithwiraj Choudhury on the various stages of remote work.

> We think this model will be called all-remote. 
>
> We think it's time for clear differentiation between companies which *allow* remote, and companies that will not allow you to come into an office.
>
> People will start understanding the difference. If there is no headquarters, you're not at a satellite office. — *GitLab co-founder and CEO Sid Sijbrandij*

GitLab is a [100% remote company](/company/culture/all-remote/), where each individual works remotely and there are no company-owned offices. With [team members](/company/team/) in over 65 countries, many time zones are accounted for. 

In such companies, there is no effort placed on trying to coordinate team members to a given time zone. Rather, a bias towards [asynchronous communication](/company/culture/all-remote/asynchronous/) encourages documentation, discourages synchronous meetings as a default for collaboration, and provides greater flexibility for each member to determine the working hours that best suites their [lifestyle](/company/culture/all-remote/people/). 

This goes beyond enabling a work from home arrangement; all-remote creates a work from *anywhere* arrangement. 

## 10. Strictly remote

Taken to its extreme, a strictly remote company would never meet in person and never permit synchronous meetings. This seems impractical, and we are not yet aware of a company that adheres to this. If you are aware of any such company, please create a merge request and assign to `@dmurph` to evaluate. 

## What "all-remote" does not mean

Let's address some of the common misconceptions about all-remote work.

First things first: An all-remote company means there is *no* office where multiple people are based. The only way to not have people in a satellite office is not to have a main office. It's not that we don't have a headquarters, it is that we have 1,100+ (and growing) headquarters!

The terms "remote" and "distributed" are often used interchangeably, but they're not quite the same. We prefer the term "remote" because "distributed" suggests multiple physical offices. "Remote" is also the [most common term](https://www.google.com/search?ei=4IBsXKnLDIGRggftuqfAAQ&q=distributed+companies&oq=distributed+companies&gs_l=psy-ab.12...0.0..5177...0.0..0.0.0.......0......gws-wiz.6xnu76aJWr4) to refer to the absence of a physical workspace, and being able to do your job from anywhere.

For employees, being part of an all-remote company does not mean working independently or being isolated, because it's not a substitute for human interaction.

Technology allows us to [stay closely in touch](/company/culture/all-remote/informal-communication/) with our teams, whether asychronously in text or in real time with high-fidelity conversations through video. Teams should collaborate closely, [communicate](/company/culture/all-remote/informal-communication/) often, [build relationships virtually](/blog/2019/07/31/pyb-all-remote-mark-frein/), and feel like valuable members of a larger team.

Working remotely also doesn't mean you're physically constrained to home. You're free to work [wherever you want](/company/culture/all-remote/people/#travelers). That could be at home with family, a coffee shop, a coworking space, or your local library while your little one is enjoying storytime. It could mean that you're [location independent](/company/culture/all-remote/people/#nomads), traveling around and working in a new place each week. You can have frequent video chats or virtual pairing sessions with coworkers throughout the day, and you can even meet up with other coworkers to work together in person if you're located near each other.

At the organizational level, "all-remote" does not mean simply offshoring work. Instead, it means you're able to [hire the best talent from all around the world](/company/culture/all-remote/hiring/). It's also not a management paradigm. You still have a hierarchical organization, but with a [focus on output instead of input](/company/culture/all-remote/management/).

All in all, remote is fundamentally about *freedom* and *individual choice*. At GitLab, we [value your results](/handbook/values/#results), regardless of where you get your work done.

## GitLab Knowledge Assessment: Stages of Remote Work 

Anyone can test their knowledge on the Stages of Remote Work by completing the [knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLSdVT7qsBLQmx5KE5D-oZ80Byv-H9scDirYWYZZsud46N7Kz1g/viewform). Earn at least an 80% or higher on the assessment to receive a passing score. Once the quiz has been passed, you will receive an email acknowledging the completion from GitLab. We are in the process of designing a GitLab Remote Certification and completion of the assessment will be one requirement in obtaining the [certification](/handbook/people-group/learning-and-development/certifications). If you have questions, please reach out to our [Learning & Development](/handbook/people-group/learning-and-development) team at `learning@gitlab.com`.

## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
