---
layout: job_family_page
title: Data Science
---

The Data Science program at GitLab focuses on developing model-based insights to help us understand our business, customers, and product better. Data Scientists work across the entire development lifecycle, from inception to final delivery. As a result of helping GitLab understand major trends across our business, Data Scientists make significant strategic contributions to new and existing business initiatives.

Data Scientists work with peers on the Data Team and functional teams to:
  * perform ad-hoc exploratory analysis
  * solve well-defined business problems
  * regularly measure and improve analytics initiatives
  * create and maintain production models and related applications

Example Data Science projects include:
  * account scoring
  * propensity to buy
  * customer segmentation
  * sentiment analysis
  * customer churn and uplift prediction
  * hypothesis testing and forecasting

Data Scientists are a part of the [Data Team](/handbook/business-ops/data-team/) and report to the [Director/ Sr. Director, Data & Analytics](/job-families/finance/dir-data-and-analytics/).

## Levels

Read more about [levels](/handbook/hiring/#definitions) at GitLab here.

### Data Scientist (Intermediate)

Data Scientists (Intermediate) share the same requirements and responsibilities outlined above.

#### Data Scientist (Intermediate) Job Grade

The Data Scientist (Intermediate)  is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Data Scientist (Intermediate) Responsibilities

* Communicate with business partners to understand their needs to help develop new strategic insights
* Define, collaborate, and communicate key influences, levers, and impacts to non-technical audiences
* Perform exploratory data analysis to understand ecosystems, behavioral trends, and long-term trends
* Build machine learning models (training, validation, and testing) with appropriate solutions for data reduction, sampling, feature selection, and feature engineering
* Design and evaluate experiments (including hypothesis testing) by creating key data sets 
* Apply data mining or NLP techniques to cleanse and prepare large data sets
* Help grow the Data Science function by defining and socializing best practices, particularly within a [DataOps](https://en.wikipedia.org/wiki/DataOps) and [MLOps](https://en.wikipedia.org/wiki/MLOps) data ecosystem
* Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale database environment. Maintain and advocate for these standards through code review
* Document every action in either issue/MR templates, the [handbook](/handbook/), or READMEs so your learnings turn into repeatable actions and then into automation following the GitLab tradition of [handbook first!](/handbook/handbook-usage/#why-handbook-first)

#### Data Scientist (Intermediate) Requirements

* Ability to use GitLab
* 4+ years professional experience in an analytics role
* 2+ years professional experience in a predictive analytics, data science, or similar role
* Developed 2 or more automated machine learning models for production use
* Developed and presented 4 or more predictive analytical projects
* Familiarity with the [CRISP-DM](https://en.wikipedia.org/wiki/Cross-industry_standard_process_for_data_mining) analytics development model
* Experience working with a variety of statistical and machine learning methods (time series analysis, regression, classification, clustering, survival analysis, etc) 
* Professional experience with python, including python data libraries (numpy, pandas, matplotlib, scikit-learn), or R 
* Deep understanding of SQL in data warehouses (we use Snowflake SQL) and in business intelligence tools (we use Sisense for Cloud Data Teams)
* Working knowledge of statistics
* Comfort working in a highly agile, [intensely iterative](/handbook/values/#iteration) environment
* Positive and solution-oriented mindset
* Effective communication skills: Regularly achieve consensus with peers, and clear status updates
* Experience owning a project from concept to production, including proposal, discussion, and final delivery
* [Self-motivated and self-managing](/handbook/values/#efficiency), with excellent organizational skills
* Share our values, and work in accordance with those values
* Ability to thrive in a fully remote organization
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)

#### Data Scientist (Intermediate) Performance Indicators

* Number of Models Operationalized
* Number of Strategic Insights Discovered ([Data Value Calculator](/handbook/business-ops/data-team/how-we-work/#data-team-value-calculator) score 2 or higher)
* Business Partner Customer Satisfaction Score ([CSAT](https://en.wikipedia.org/wiki/Customer_satisfaction))
* Merge Request Rate

## Career Ladder

The next step in the  Data Scientist job family is to move to Senior Data Scientist.

### Senior Data Scientist 

#### Job Grade

The Senior Data Scientist is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

The Senior Data Scientist has all of the responsibilities of an Intermediate Data Scientist, plus:

*  Improve predictive models with data from multiple models 
*  Automate feedback loops for algorithms/models in production  
*  Create repeatable processes and scalable data products
*  Influence functional teams and develop best practices across the organization
*  Review, scale, and enhance operationalized statistical models and algorithms 

#### Requirements

The Senior Data Scientist meets all of the requirements of an Intermediate Data Scientist, plus:

* 6+ years professional experience in an analytics role
* 4+ years professional experience in a predictive analytics, data science, or similar role
* Developed 4 or more automated machine learning models for production use
* Developed and presented 6 or more predictive analytical projects
* Developed communication skills with ability to explain statistic and mathematical concepts to non-experts
* Extensive knowledge, application, and experience in creating and implementing recommendation systems, machine learning, NLP, statistics, and deep learning
* Ability to quantify improvements from business efficiency or customer experience based on research outcomes 

#### Performance Indicators

* Number of Models Operationalized
* Number of Strategic Insights Discovered ([Data Value Calculator](/handbook/business-ops/data-team/how-we-work/#data-team-value-calculator) score 3 or higher)
* Business Partner Customer Satisfaction Score ([CSAT](https://en.wikipedia.org/wiki/Customer_satisfaction))
* Merge Request Rate

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

* Selected candidates will be invited to fill out a short questionnaire.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a first interview with our Senior Director, Data and Analytics
* Next, candidates will be invited to schedule a second interview with a member from our Data team
* Next, candidates will be invited to schedule a third interview with the business division DRI
* Next, candidates will be invited to schedule a fourth interview with a member from our Data team

Additional details about our process can be found on our [hiring page](/handbook/hiring).
