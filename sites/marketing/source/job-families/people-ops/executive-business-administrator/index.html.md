---
layout: job_family_page
title: "Executive Business Administration"
---

## About the role

Executive Business Administrator's at GitLab toggle seamlessly between various systems including Google Workspace, Slack, Expensify, Zoom and GitLab to accomplish an array of tasks, while staying focused on prioritization and escalating urgent issues. EBA's at GitLab are self-driven, collaborative, and agile team members who are experienced in managing multiple priorities, juggling various responsibilities, and anticipating executives’ needs. EBA's at GitLab pride themselves with being exceptionally organized, relentlessly resourceful, calm under pressure, strategic multi-takser, with a deep love of logistics and ability to thrive in a dynamic start-up environment.

## Executive Business Administrator (Intermediate)

### Job Grade

The Executive Business Administrator is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Support our Executive Leadership Team in EST - PST timezones
* Manage complex calendar including vetting, prioritizing and providing recommendations
* Total travel coordination including air and ground transportation, hotel reservations, security, visas and other travel documentation
* Maintain an efficient flow of information between all levels including internal and external contacts on a wide spectrum of plans and priorities
* Draft internal and external communications on behalf of executive
* File expense reports and track reimbursement status
* Manage projects, internal and external meetings and large-scale events from budget planning through logistical coordination
* May coordinate, track and schedule internal and external PR and communication events for manager and team
* Partner with PeopleOps, IT and Security to resolve any logistical issues
* Assist the full life cycle of recruiting including booking interviews, liaising between the hiring team and the candidate, and coordinating onboarding of new hires
* Maintains and tracks manager and teams project list and goals
* Attend team staff meetings, track agenda and assist Executive to keep team on course
* Schedules, live streams and moderates [Group Conversations](/handbook/people-group/group-conversations/)
* Provide general project support as needed including ad-hoc reporting
* Provide coverage for other E-Group EBA's

### Requirements

* Minimum of 3 years of executive administration supporting more than one executive
* Successful history of managing the calendars, expenses, and travel of multiple executives
* Experience predicting, prioritizing, and assisting an executive’s workload
* Extensive technical skills in Google Workspace, Zoom, Slack, and Expensify
* Must be proactive and able to deal with ambiguity, prioritize own work and resources, and juggle multiple tasks in a manner transparent to the team, and work independently to achieve results with a high degree of accuracy
* Exceptional communication and interpersonal skills and ability to interact autonomously with internal and external partners
* Maintain the confidentiality of highly sensitive material with tact and professionalism
* Demonstrated ability to adopt technical tools quickly (i.e. terminal, text editor)
* Possess an ability to multi-task and prioritize in a dynamic environment
* Superior attention to detail
* Event coordination and creative event planning experience
* Excellent written and verbal English communication skills
* Experience in a start-up environment preferred
* Experience working remotely preferred
* A desire to learn, have fun and work hard
* A passion for GitLab
* A sincere willingness to help out
* Able to work collaboratively with EBA's across the organization
* An orientation towards team success
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).
* Ability to use GitLab

### Performance Indicator
* [Leadership SAT Survey](https://about.gitlab.com/handbook/eba/#leadership-sat-survey)

## Senior Executive Business Administrator

### Job Grade

The Senior Executive Business Administrator is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Support C-Suite members of our Executive Leadership Group in PT, MT, CT, and ET time-zones
* Own and proactively manage a complex, changing, high-volume calendar across multiple time-zones. Ensure Exec is prepped for every meeting
* Make recommendations for the Executive in regard to their time management, prioritization, delegation and organization
* Seamlessly coordinate extensive domestic and international travel including air and ground transportation, hotel reservations, security, visas and other travel documentation. Accompanying the Executive when necessary
* Run cadence of weekly staff meetings and other important meetings, including planning agenda, organization, and follow-up on action items, while infusing our company values throughout
* Plan events such as team off-sites, team building activities and company kick-offs as needed
* Draft internal and external communications on behalf of Executive
* Assist the full life cycle of recruiting including booking interviews, liaising between the hiring team and the candidate, and coordinating onboarding of new hires
* Manage Executives email inbox including drafting internal and external communications on behalf of Executive
* Compile receipts to submit timely and accurate expense reports on a monthly basis
* Schedules, live streams and moderates [Group Conversations](/handbook/people-group/group-conversations/)
* Provide coverage for other Sr. E-Group EBA's
* Run and lead special projects upon request
* Must be able to work flexible hours to support international business meetings and some travel is required in most roles
* Other duties as assigned in support of the business (ad hoc tasks)

### Requirements

* Minimum 5 years supporting a C-Level Executive(s) as an executive business partner or Sr. EA at a quickly-scaling company
* Bachelor's’ Degree preferred. High school diploma or general education degree (GED) required
* Self-starter who can operate independently and move quickly from one task to another; creative problem solver, seeks "win-win" solutions; energized by challenges with superb attention to detail
* Extensive technical skills with Google Workspace, Zoom, Slack and Expensify among other tools
* Demonstrated Leadership mindset in prior roles—ability to influence culture/environment around them
* Proven leadership skills and demonstrates “one team” mindset; able to lead other EBA’s and partner well across GitLab
* Experience with event planning & coordination to include support for large meetings, off-sites and company events
* Detailed and goal-oriented planner; possesses the ability to appropriately prioritize business needs and handle multiple tasks in a fast-paced environment
* Approachable and effective communicator across various communication channels and with all levels of the organization
* Demonstrated ability to adopt technical tools quickly (i.e. terminal, text editor)
* Experience in a start-up environment preferred
* Experience working remotely preferred
* A passion for GitLab
* A sincere willingness to help out

### Performance Indicator
* [Leadership SAT Survey](https://about.gitlab.com/handbook/eba/#leadership-sat-survey)

## Staff Executive Business Administrator to the CEO

### Job Grade

The Staff Executive Business Administrator is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Strategically and methodically support our Chief Executive Officer as your primary executive, and the Chief Revenue Officer as your secondary executive,  in the Pacific time zone - serving as a partner in handling correspondence, including confidential and sensitive materials
* Proactively and without supervision, anticipate needs and follow through diligently, often using knowledge of the executive, their direct reports, external parties, and company values to make decisions that lead to more effective executive attention and communication
* Maintain the executives’ calendars in a thoughtful and purposeful manner, ensuring meetings contain the appropriate attendees, agenda items, and any changes are done with full consideration of a global team, spanning multiple time zones; ensure executive is prepped and briefed as necessary for each meeting
* Make recommendations for the executive in order to more effectively manage their time, prioritize, delegate and organize
* Seamlessly coordinate extensive travel including air and ground transportation, hotel reservations, security, visas, and any other travel documentation
* Maintain cadence of weekly meetings and other important meetings, including planning agenda, organization, and follow-up on action items, while infusing our company values throughout
* Draft internal and external communications on behalf of the executive keeping in mind our company values and safeguarding confidential information as well as the executive’s reputation
* Assist the full life cycle of recruiting activity for the executive,  including booking interviews, liaising between the hiring team and the candidate, and coordinating onboarding of newly hired executives
* Manage executive’s email inbox including drafting internal and external communications on behalf of the executive when required
* Compile receipts to submit timely and accurate expense reports on a monthly basis
* Document preparation for meetings such as creation of agendas for team meetings, presentations for group conversations, tracking key deliverables and action items, maintain operational metrics or KPIs for the department
* Liaise effectively with EBA counterparts and build strong and collaborative working relationships with a wide range of company stakeholders at all levels
* Orchestrate events such as team off-sites and team building activities
* Serve as a helpful knowledge base for executives and other team members (e.g., other EBAs), especially for processes and procedures, and provide assistance in a hands-on, kind, and proactive manner
* Work flexible hours to support global business meetings; some travel is required
* Continually improve processes and procedures to reduce work time and cost without compromising work quality;  take proactive measures to prevent yourself from burnout
* Schedules, live streams and moderates [Group Conversations](/handbook/people-group/group-conversations/)
* Other duties (and ad hoc tasks) as assigned in support of your executive and business

### Requirements

* Minimum 8+ years supporting a C-Level Executive(s) as an executive business partner at a quickly-scaling company, effectively representing the personal brand of the executive as well as the company’s culture and value
* Minimum 2 years experience managing a team
* High integrity and track record of maintaining confidentiality of all work product and sensitive information
* Strong attention to detail and proofreading ability
* Extensive technical skills with Google Workspace, Zoom, Slack and Expensify among other tools; demonstrated ability to adopt technical tools quickly (i.e. GitLab.com)
* Self-starter who can operate independently and move quickly from one task to another; creative problem solver, seeks "win-win" solutions; energized by challenges with superb attention to detail
* Excellent interpersonal skills and business acumen; ability to communicate with team members, partnering well across GitLab, and external parties at all levels
* Proven leadership skills and demonstrates “one team” mindset; able to be a role model for all other EBA’s
* Detailed and goal-oriented planner; possesses the ability to appropriately prioritize business needs and handle multiple tasks in a fast-paced environment
* Approachable and effective communicator across various communication channels and with all levels of the organization
* Experience with event planning & coordination to include support for large meetings, off-sites
* Experience in both start-up and medium size enterprise environments preferred
* Experience working remotely preferred
* A sincere willingness to help out
* A passion for GitLab and for contributing to an exceptional work culture

### Performance Indicator
* [Leadership SAT Survey](https://about.gitlab.com/handbook/eba/#leadership-sat-survey)

#### Hiring Process
* Qualified candidates will be invited to schedule an initial screening with a Global Recruiter
* Next, candidates will be invited to schedule a first interview with an Executive Business Administrator
* Next, candidates will be invited to schedule additional interviews with the Executive Business Administrator team
* Next or in parallel with EBA team interview, candidates will be invited to complete a 5 hour timed assessment
* Final candidates will then be invited to schedule interviews with members of the leadership team the role will be supporting
* Successful candidates will subsequently be made an offer via phone call or video chat

## Career Ladder

An Executive Business Administrators career path can take various routes depending on the long term career goals and aspirations of an EBA. One path that is routinely taken is that of a career EBA and is someone who is passionate about being in the EBA role for the long-term resulting in becoming a Senior, Staff, or Principal EBA at GitLab to a member of our E-Group. Another path that is open to an EBA is one where they decide to leverage their experience as an EBA to transition into a new role such as (but not limited to) a Chief of Staff, Project Manager, Recruiter, Corporate Events Manager, Business Analyst, Executive Communications, PR, Internal Communications, People Operations Specialist, People Business Partner, etc.

