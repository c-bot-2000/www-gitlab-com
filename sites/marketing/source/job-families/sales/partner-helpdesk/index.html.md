---
layout: job_family_page
title: "Partner Help Desk Specialist"
description: "The Partner Help Desk Specialist is responsible for supporting the GitLab partner development, sales enablement, pipeline development and sales transactions. "
---

The Partner Help Desk Specialist is responsible for supporting the GitLab partner development, sales enablement, pipeline development and sales transactions to achieve stated channel sales targets.  The Partner Help Desk Specialist will support Channel Sales Managers in addressing partner questions and system issues, and will proactively drive partner onboarding to speed each partner's time to their first deal.

## Levels

### Partner Help Desk Specialist (Intermediate)

The Partner Help Desk Specialist (Intermediate) reports to the [Senior Director, Channel Programs and Enablement](/job-families/sales/director-channel-programs-and-enablement/).

#### Partner Help Desk Specialist (Intermediate) Job Grade

The Partner Help Desk Specialist (Intermediate) is a [Grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Partner Help Desk Specialist (Intermediate) Responsibilities

* Support Channel Sales Manager in the Partner onboarding process; contracts, portal training, enablement, etc.
* Support your assigned channel to understand, navigate and meet milestones in the Gitlab Partner Program; including accreditations, badging, certifications.
* Support partner in all stages of the customer lifecycle; demand database management, deal registration, quote creation, order processing, add-ons & renewals etc.
* Execute sales and marketing enablement to channel & distribution partners.
* Support the execution of GTM, Sales, Marketing, Channel Services.

#### Partner Help Desk Specialist (Intermediate) Requirements

* Previous experience of supporting sales or channel sales ideally within the same product category and channel.
* Experience selling in the Software Development Tools and/or Application Lifecycle Management space via channel partnerships.
* Experience selling or supporting Open Source Solutions.
* 2+ years of experience with B2B sales.
* Interest in GitLab and open source software.
* Effective communicator with excellent interpersonal skills and relationship builder with partners and GitLab teams.
* Excellent sales support skills, proficiency with Salesforce a plus.
* Broad personal network within the industry.
* Driven, highly motivated and results driven.
* You share our [values](/handbook/values/), and work in accordance with those values.
* [Leadership at GitLab](/handbook/leadership/).
* Desire to learn the use of the GitLab platform.
* Ability to use GitLab.

### Senior Partner Help Desk Specialist

The Senior Partner Help Desk Specialist reports to [Senior Director, Channel Programs and Enablement](/job-families/sales/director-channel-programs-and-enablement/).

#### Senior Partner Help Desk Specialist Job Grade

The Senior Partner Help Desk Specialist is a [Grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Senior Partner Help Desk Specialist Responsibiliites

* Extends that of the Partner Help Desk Specialist (Intermediate) responsibilities

#### Senior Partner Help Desk Specialist Requirements

* Extends that of the Partner Help Desk Specialist (Intermediate) requirements
* 3+ years of experience with B2B sales.

## Performance Indicators
* 80% of Partners trained / certified
* Increase in number of Partner Initiated Opportunities
* Time to first partner deal - less than 3 months
* Number of partner cases resolved each month - 90% or more

## Career Ladder

The next step in the Partner Helpdesk Specialist job family is not yet defined at GitLab.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

- Selected candidates will be invited to schedule a 30 min. [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters
- Next, candidates will be invited to schedule a first interview with the Hiring Manager
- Next, candidates will be invited to interview with 1-4 Team Members

Additional details about our process can be found on our [hiring page](/handbook/hiring).
