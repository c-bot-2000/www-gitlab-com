---
title: "GitLab's Functional Group Updates - June 19th - June 29th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on from June 19th - June 29th"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/group-conversations/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Discussion Team

[Presentation slides](http://smcgivern.gitlab.io/discussion-updates/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8o7cVhDIm68" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Product Team

[Presentation slides](https://www.slideshare.net/JobvanderVoort/gitlab-product-update-june-20)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/i-PXdyjYuZc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Backend Team

[Presentation slides](https://docs.google.com/presentation/d/17dLtxUC3xA304gY-swmaKPJ4fUoGlr0Ts73nsWm8thY/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/_o80ilayDqA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### People Ops Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/1OAnjmDD31wI2ftEmZOLfSETDDSOZYmT4zat7CYK-qEM/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/g3TfCLBe0mc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### General

[Presentation slides](/company/okrs/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/6pSNdDJ7xPQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
